# pyannov

Determine the value of type annotation of python variables.

## python version
Tested in Python 3.6

In top folder and run this

> python3 pyannov/demo.py

## 权重设计

### 初始权重

### 依赖类型的权重区分

- 直接引用，别名 权重较高
- 操作符，函数调用等 权重较低

## 测试源代码来源

- test0.py dummy
- test1.py dummy+attribute
- test2.py dummy+if+unaryop
- test3.py dummy+forloop+augassign
- test4.py dummy+whileloop
- test5.py dummy+call
- test6.py dummy+tuple
- http_websocket.py from aiohttp
- kdtree.py from scipy

## 代码注入

源代码

```python
from pyannovmodule import annoted

# insert this code before accessing var1
pyannovmodule_var1(var1)

x = var1
```

类型标注 annoted.pyi

```python
def pyannovmodule_var1(var1: int) -> str
```

todo:如何做到自动化代码注入

## 实验设置

有关排序效果的实验（Rank实验）
1、从Top10中连续取R(k)的实验（标准实验）
2、从不同函数中取Top10的实验（标注推荐是否有局部性效果？）
3、从Top100中隔10个取10个的实验（放大数据集规模后标注推荐是否有效？）

有关标注数量的实验（Top实验）
1、在Top10中依次取1~10个的实验（标准实验）
2、从Top100中隔10个依次取1~10个的实验
3、从不同函数中依次取1~10个的实验（考察局部性效果）
4、从Top100中随机取10个的实验

分别做三组实验

#### 从不同函数中取Top10的实验（标注推荐是否有局部性效果？）

Rank实验和Top实验

#### 从Top100中隔10个取10个的实验（放大数据集规模后标注推荐是否有效？）

Rank实验和Top实验

#### 从Top100中随机取10个的实验

只有Top实验