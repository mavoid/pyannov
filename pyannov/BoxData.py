import json

class BoxData:
    def __init__(self, origin: list):
        length = len(origin)
        origin.sort(reverse=True)
        self._make(origin[int(length/2)], origin[:int(length/3)], 
            origin[:int(length/4)], origin[:int(length/6)], 
            origin[:10] if length>=10 else origin,
            origin[:20] if length>=20 else origin,
            origin[:30] if length>=30 else origin,
            origin[:40] if length>=40 else origin)
            

    def _make(self, middle, tops3, tops4, tops6, top10, top20, top30, top40):
        self.middle = middle
        self.tops3= tops3
        self.tops4 = tops4
        self.tops6 = tops6
        self.top10 = top10
        self.top20 = top20
        self.top30 = top30
        self.top40 = top40
    def getThreshold(self, option=''):
        if option == 'top10':
            return self.top10[-1]
        elif option == 'top20':
            return self.top20[-1]
        elif option == 'top40':
            return self.top40[-1]
        return self.top30[-1]

    def __str__(self):
        return 'middle:' + str(self.middle) + ',' + \
            'tops6:' + str(self.tops6)

class BoxEncoder(json.JSONEncoder):
    def default(self, b):
        if isinstance(b, BoxData):
            return b.__dict__