class Identifier:
    def __init__(self):
        self.functions = set()
        self.classes = set()
        self.modules = set()

    def add(self, functions, classes, modules):
        self.functions = self.functions.union(functions)
        self.classes = self.classes.union(classes)
        self.modules = self.modules.union(modules)
    