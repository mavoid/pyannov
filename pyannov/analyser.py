import log
import logging
import utils

from visitor import Visitor0

class Analyser():
    def __init__(self, outfile):
        self.log = logging.getLogger(self.__class__.__name__)
        self.outeps = outfile
    def analyse(self, asttree):
        v = Visitor0()
        v.visit(asttree)
        influence = v.influence
        table = v.table
        # save to file system
        # utils.saveGraph(influence, self.outeps)
        # self.log.info('analyse result influence: %s' % influence)
        # self.log.info('analyse result table: %s' % table)
        scr = self.score_bfs(influence, table)
        ss = self.sort(scr)
        # self.log.info(ss)
        return ss, len(influence), v

    # sort dict by value
    # https://stackoverflow.com/questions/20944483/python-3-sort-a-dict-by-its-values
    def sort(self, score: dict):
        s = [(k, score[k]) for k in sorted(score, key=score.get, reverse=True)]
        return s

    """
    influence: key:right value: left
    table: key:left value:right
    using breadth first search
    """
    def score_bfs(self, influence, table):
        result = dict()
        for right in influence.keys():
            result[right] = self.bfs([right], influence, set())
        return result

    # loop free version
    def bfs(self, roots, tbl: dict, looked=set(), base = 1):
        res = 0
        leaves = self.getLeaves(roots, tbl, looked)
        res += base * len(leaves)
        looked.update(roots)
        if len(leaves) == 0:
            return 0
        else:
            res += self.bfs(leaves, tbl, looked = looked,base=base*0.6)
        
        # self.log.debug("roots: %s"%roots)
        # self.log.debug("leaves: %s"%leaves)

        return res

    def getLeaves(self, roots, tbl: dict, looked):
        leaves = set()
        for k in roots:
            if k in tbl:
                v = tbl[k]
                leaves.update(v)
                # remove looked
                leaves = leaves -looked
        return leaves


