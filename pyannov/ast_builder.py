import log
import logging
import astor

import utils

####
# create the ast tree for analysis afterwards, the simplifying procedure also place here if there need simplifying 
####
class AstBuilder():
    def __init__(self, filename):
        self.log = logging.getLogger(self.__class__.__name__)
        self.file = filename
        self.tree = None
    def __build(self):
        with open(self.file, 'r') as sourcefile:
            try:
                atree = astor.parse_file(sourcefile.name)
                self.tree = atree
                self.tree.name = utils.get_canonical_name(self.file)
                return self.tree
            except SyntaxError as e:
                self.log.error(e.msg)
                self.log.error('Syntex error because of python2 code in file: %s' % sourcefile.name)
            
    def getAst(self):
        if self.tree is not None:
            return self.tree
        else:
            # self.log.debug('ast tree is None, build it first.')
            return self.__build()
