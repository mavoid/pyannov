import platform

proot = '/some/where/not/exist/' 
ejectdir = '/some/where/not/exist/'
outdir = '/some/where/not/exist/'

pyimodule = 'annotations'
annotationPrefix = 'annot'

if platform.system() == 'Darwin':
    # proot = '/Users/mhy/Repos/pyannov/test/'
    proot = '/Volumes/maconly/Repos/master-thesis/script/source/'
    # proot = '/Volumes/maconly/source/'
    ejectdir = '/Volumes/maconly/Repos/pyannov/resources/ejected/'
    outdir = '/Volumes/maconly/Repos/pyannov/resources/pyi/'
elif platform.system() == 'Linux':
    # fedora 29 on thinkpad
    proot = '/home/mhy/Desktop/master-thesis/script/source/'