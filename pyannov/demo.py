#! /Users/mhy/anaconda3/bin/python3

import os
import time
import json
import log
import logging
from analyser import Analyser
from ast_builder import AstBuilder
import astpretty
import utils
import config
from ejected import Ejector

from Identifier import Identifier

####
#   main function here. build ast first then do analysis.
####


class Demo():
    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.identifier = None

    def start1(self, projdir: str, outdir: str):
        scoreSum = []
        totalInfluence = 0
        utils.mkdir(outdir)
        for root, dirs, files in os.walk(projdir):
            for fname in files:
                if fname.endswith('.py'):
                    fullname = os.path.join(root, fname)
                    # self.log.debug('file name: %s' % fullname)
                    scoreSingle, influence = self.start0(fullname, utils.renamefile(
                        fullname, projdir, outdir, 'gv'))
                    scoreSum += scoreSingle
                    totalInfluence += influence
        return scoreSum, totalInfluence

    def start0(self, filename, outfile='test/outDefault'):
        # self.log.debug('file: '+filename)
        builder = AstBuilder(filename)
        tree = builder.getAst()
        if tree is None:
            return [],0
        # self.log.debug('ast: '+ astpretty.pformat(tree))
        analyser = Analyser(outfile)
        score, influence, v0 = analyser.analyse(tree)
        if not self.identifier is None: 
            self.identifier.add(v0.functions, v0.classes, v0.modules)
        # self.write2file(score, outfile)
        self.log.info("the scored variables: %s" % score)
        return score, influence

    def write2file(self, scorelist: list, outfile):
        utils.mkdir(outfile)
        with open(outfile, 'w+') as file:
            jsonstr = json.dumps(scorelist)
            file.write(jsonstr)


def clean(d):
    import shutil
    try:
        shutil.rmtree(d)
    except FileNotFoundError as ff:
        print(ff)


if __name__ == "__main__":
    dummy = 'test/test0.py'
    attr = 'test/test1.py'
    ifs = 'test/test2.py'
    fors = 'test/test3.py'
    whiles = 'test/test4.py'
    calls = 'test/test5.py'
    tuples = 'test/test6.py'
    real1 = 'test/http_websocket.py'
    real2 = 'test/kdtree.py'
    ch4paper = 'test/ch4.py'
    demo = Demo()
    # single .py file
    # demo.start0(dummy)
    # demo.start0(attr)
    # demo.start0(ifs)
    # demo.start0(fors)
    # demo.start0(whiles)
    # demo.start0(calls)
    # demo.start0(tuples)
    # demo.start0(real1)
    # demo.start0(real2)
    # demo.start0(ch4paper)

    roots = config.proot
    subdirs = utils.get_immediate_subdirectories(roots)
    ejectRoot = config.ejectdir
    outRoot = config.outdir
    clean(ejectRoot)
    clean(outRoot)

    performance = dict()  # in seconds
    influenceSum = dict()
    print('all projects: %s' % subdirs)
    for proj in subdirs:
        # todo: debug
        # if proj != 'dash':
        #     continue
        start = time.perf_counter()
        projdir = roots + proj + '/'
        pyidir = outRoot+proj + '/custom112233/'
        utils.mkdir(pyidir)
        ejectdir = ejectRoot + proj + '/'
        # whole project
        demo.identifier = Identifier()
        allScores, totalInfluence = demo.start1(projdir, pyidir)

        # just for compute scores
        end = time.perf_counter()

        boxdata = utils.genBoxplot(allScores, pyidir)
        ejector = Ejector(projdir, ejectdir, pyidir, allScores, boxdata, demo.identifier)
        ejector.eject()
        # save .pyi template to file
        # utils.annoted2file(allScores, threshold, outdir, projdir)

        performance[proj] = end - start
        influenceSum[proj] = totalInfluence

    pfile = outRoot+'/perf.csv'
    utils.savePerformance(pfile, performance, influenceSum)
