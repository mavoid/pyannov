import os
import astor
import log
import logging
import shutil
from BoxData import BoxData
from visitor1 import Visitor1
from Identifier import Identifier
import config
import utils

class Ejector():
    def __init__(self, projdir, ejecteddir, pyidir, scores: [(str, int)], 
            boxdata: BoxData, identifier: Identifier):
        self.log = logging.getLogger(self.__class__.__name__)
        self.projdir = projdir
        self.ejectedDir = ejecteddir
        self.pyidir = pyidir
        self.scores = scores
        self.boxdata = boxdata

        # for diversity (chapter 4)
        self.funcIdentifiers = identifier.functions
        self.classIdentifiers = identifier.classes
        self.moduleIdentifiers = identifier.modules
        self.__init__diversity()

    def _getModuleName(self, canonical: str):
        # last one is variable name
        array = canonical.split('.')
        module, length = self._moduleNameRecursive(array[:-1])
        if not module is None:
            middle = array[length:-1]
            return module, middle, array[-1]
        return None,None,None
        

    def _moduleNameRecursive(self, array: list):
        # stop condition
        if len(array) == 0:
            return None,None
        filename = '/' + os.path.join(*array)+'.py'
        if os.path.isfile(filename):
            return filename,len(array)
        else:
            return self._moduleNameRecursive(array[:-1])

    def _filterByInterval(self, count):
        if count % 10 == 1:
            return False
        return True

    def __init__diversity(self):
        self.limit = 1
        self.funcChosen = dict()
        self.classChosen = dict()
        self.moduleChosen = dict()
    def _filterByDiversity(self, canonical: str):
        # varible inside of a func
        for fid in self.funcIdentifiers:
            if canonical.startswith(fid):
                if fid in self.funcChosen and self.funcChosen[fid] >= self.limit: 
                    return True
                else:
                    # added to chosen
                    if fid in self.funcChosen:
                        self.funcChosen[fid] += 1
                    else:
                        self.funcChosen[fid] = 1
                    return False
                
        for clsid in self.classIdentifiers:
            if canonical.startswith(clsid):
                if clsid in self.classChosen and self.classChosen[clsid] >= self.limit: 
                    return True
                else:
                    # added to chosen
                    if clsid in self.classChosen:
                        self.classChosen[clsid] += 1
                    else:
                        self.classChosen[clsid] = 1
                    return False
        for mid in self.moduleIdentifiers:
            if canonical.startswith(mid):
                if mid in self.moduleChosen and self.moduleChosen[mid] >= self.limit: 
                    return True
                else:
                    # added to chosen
                    if mid in self.moduleChosen:
                        self.moduleChosen[mid] += 1
                    else:
                        self.moduleChosen[mid] = 1
                    return False

        return False

    def _needed(self):
        res = dict()
        allmodules = set()
        threshold = self.boxdata.getThreshold(option='top40')
        count = 0
        for name,score in self.scores:
            count += 1
            module, middle,vname = self._getModuleName(name)
            if score >= threshold:
                # if self._filterByDiversity(name):
                #     continue
                if self._filterByInterval(count):
                    continue
                if module is None:
                    self.log.error('file not found: %s'% module)
                else:
                    if module in res:
                        res[module].append((middle, vname))
                    else:
                        res[module] = [(middle, vname)]
            # not a bug
            if not module is None:
                allmodules.add(module)
        return res, allmodules

    # create unique function name for a variable
    def _variableName(self, module, middle, vname):
        module = utils.removePrefix(module, self.projdir)[:-3] + '/'
        if len(middle) > 0:
            module = os.path.join(module, *middle) + '/'
        fullname = module.replace('/','_').replace('-','_') + vname
        return config.annotationPrefix + '_' + fullname
    def append2pyi(self, module, infos):
        # this dir must matchs in pysonar configuration
        pyifile = os.path.join(self.pyidir,config.pyimodule+'.pyi')
        with open(pyifile, 'a+') as f:
            for info in infos:
                middle, vname = info
                fullname = self._variableName(module, middle, vname)
                pycode = self.genPythonSnippet(fullname)
                f.write(pycode)

    def genPythonSnippet(self, name):
        # default to assignment expression
        return name +' = ... # type: Todo\n'

    def src2file(self, src, module):
        dst = self.ejectedDir + utils.removePrefix(module, self.projdir)
        utils.mkdir(dst)
        with open(dst, 'w+') as f:
            f.write(src)
    def eject(self):
        # notneeded is obsolate
        needed, allmodules = self._needed()

        # copy all source first
        for module in allmodules:
            if module is None:
                self.log.warn('module: %s is None' % module)
            else:
                dst = self.ejectedDir + utils.removePrefix(module, self.projdir)
                utils.mkdir(dst)
                # by coping it
                shutil.copyfile(module, dst)

        for module in needed:
            # eject pyi template to file
            infos = needed[module]
            self.append2pyi(module, infos)

            # eject py code to source base of project
            astree = astor.parse_file(module)
            varset = utils.varsSet(infos)
            # todo: module info to Visitor1
            visitor = Visitor1(varset, utils.getModuleShort(module, self.projdir))
            modified = visitor.visit(astree)
            ## remains unhandled variables
            remains = visitor.vars
            src = astor.to_source(modified)
            self.src2file(src, module)
            # self.log.debug('src from modified ast: \n%s'%src)


        
