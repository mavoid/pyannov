import ast
import os
import json
import numpy as np
from graphviz import Digraph
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from BoxData import BoxData,BoxEncoder

def get_canonical_name(filename):
    if filename.endswith('.py'):
        name = filename[:-3]
        return name.replace('/', '.')
    else:
        return filename.replace('/', '.')

def namejoin(name0, name1):
    if len(name0) == 0:
        return name1
    return name0 + '.' + name1

def getfullname(pname, node):
    if isinstance(node, ast.Name):
        return namejoin(pname, node.id)
    # todo: handle attribute access

def add2table(key, val, table: dict):
    if key in table:
        table[key].append(val)
    else:
        table[key] = [val]

def saveGraph(table: dict, epsfile):
    dot = Digraph(engine='dot', format='eps', graph_attr={
                  'overlap': "false"})
    dot.edge_attr['len'] = '2.8'
    for key in table:
        for v in table[key]:
            dot.edge(key, v)
    dot.render(filename=epsfile)

# make dir recursively for dir or file
def mkdir(path: str):
    dirs = os.path.dirname(path)
    if not os.path.exists(dirs):
        os.makedirs(dirs)

def renamefile(fname, oldpre, newpre, suffix):
    clean = removePrefix(fname, oldpre)
    return newpre + clean + '.' + suffix

def removePrefix(t: str, prefix: str):
    if t.startswith(prefix):
        return t[len(prefix):]
    else:
        return t

def boxdata2file(data, outdir):
    with open(os.path.join(outdir, 'box.json'), 'w+') as f:
        f.write(json.dumps(data, cls=BoxEncoder))
def annoted2file(scores, threshold, outdir, projdir):
    annovmodule = os.path.join(outdir, 'pyannovmodule/')
    mkdir(annovmodule)
    with open(os.path.join(annovmodule,'annoted.pyi'), 'w+') as f:
        for vname,score in scores:
            if score >= threshold:
                f.write(removePrefix(vname, projdir.replace('/','.')) + '\n')

def genBoxplot(scores, outdir):
    nums = getNumberFromScore(scores)
    boxdata = BoxData(nums)
    boxdata2file(boxdata, outdir)
    nparray = np.asarray(nums, dtype = np.float)
    fig, ax = plt.subplots()
    ax.set_title(outdir.split('/')[-2][:-3])
    ax.boxplot(nparray)
    plt.savefig(os.path.join(outdir, 'box.png'))
    #plt.show()
    return boxdata

# [('key',2.0),('key2',1.0)]
def getNumberFromScore(scores):
    return [y for x,y in scores]

# https://stackoverflow.com/questions/800197/how-to-get-all-of-the-immediate-subdirectories-in-python
def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def varsSet(lst: [([str], str)]) -> set:
    res = set()
    for (middle,vname) in lst:
        fullname = ''
        for mid in middle:
            fullname += mid + '.'
        fullname += vname
        res.add(fullname)
    return res

# for visitor1
def uniFuncName(module, vname):
    if module == '':
        res = module
    else:
        res = module + '_' 
    if vname.startswith('.'):
        vname = vname[1:]
    vs = vname.replace('.', '_')
    return res + vs

def getModuleShort(module, prefix):
    # ends with .py
    module = removePrefix(module, prefix)
    return module.replace('/', '_').replace('-', '_')[:-3]

def savePerformance(pfile, perf, influenceCount):
    if not os.path.isfile(pfile):
        mkdir(pfile) 

    with open(pfile, 'w+') as ff:
        for key in ['dash', 'requests','flask', 'numpy', 'scrapy', 'sanic', 'aiohttp', 'pytorch', 'scipy', 'keras']:
            ff.write(key + ','+str(perf[key])+','+str(influenceCount[key])+'\n')
        



