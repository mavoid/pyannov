import astpretty
import ast

import log
import logging
import utils

###
# pname represent the parent node's name
# pname to make sure unique name of a variable name in global
###

class Visitor0(ast.NodeVisitor):
    def __init__(self):
        self.influence = dict() ## right names as keys
        self.table = dict()     ## left names as keys
        self.functions = set() ## for diversity (chaper 4)
        self.modules = set()
        self.classes = set()
        self.log = logging.getLogger(name= __name__)

    def _add2table(self, key, value, tbl, node):
        utils.add2table(key, value, tbl) 

    def generic_visit(self, node):
        pass
        # self.log.info('generic_visit() node type: ' + type(node).__name__)

    def myvisit(self, node, pname):
        node.pname = pname
        return self.visit(node)

    def visit_Module(self, node):
        # self.log.info('node: \n%s' % (astpretty.pformat(node)))
        modulename = node.name
        if node.name.endswith('dash.dash'):
            print('')
        self.modules.add(modulename)
        body = node.body
        for some in body:
            some.pname = modulename
            self.visit(some)

    def visit_FunctionDef(self, node):
        # self.log.info("node: \n%s" % (astpretty.pformat(node)))
        funname = utils.namejoin(node.pname, node.name)

        self.functions.add(funname)
        ## 过程内数据流分析
        for s in node.body:
            s.pname = funname
            self.visit(s)

    # def visit_ClassDef(self, node):
    #     # self.log.info("node: \n%s" % (astpretty.pformat(node)))
    #     classname = utils.namejoin(node.pname, node.name)

    #     self.classes.add(classname)
    #     ## 过程内数据流分析
    #     for s in node.body:
    #         s.pname = classname
    #         self.visit(s)
    
    def visit_Lambda(self, node):
        body = self.myvisit(node.body, node.pname)
        return body

    def visit_Starred(self, node):
        value = self.myvisit(node.value, node.pname)
        return value

    def visit_Call(self, node):
        res = []

        # handle function name
        node.func.pname = node.pname
        fnames = self.visit(node.func)
        res += fnames
        # handle args
        for a in node.args:
            a.pname = node.pname
            anames = self.visit(a)
            for aname in anames:
                res.append(aname)
        return res
    
    def visit_If(self, node):
        # ignore test for simplicity
        body = node.body
        for e in body:
            e.pname = node.pname
            self.visit(e)
        orelse = node.orelse
        for e in orelse:
            e.pname = node.pname
            self.visit(e)

    def visit_IfExp(self, node):
        body = self.myvisit(node.body, node.pname)
        orelse = self.myvisit(node.orelse, node.pname)
        test = self.myvisit(node.test, node.pname)
        return body + orelse + test

    def visit_For(self, node):
        # todo: more general dependence
        # for ele in mylist:
        # ele depends mylist also
        body = node.body
        for e in body:
            e.pname = node.pname
            self.visit(e)
        for e in node.orelse:
            e.pname = node.pname
            self.visit(e)
    def visit_YieldFrom(self, node):
        value = self.myvisit(node.value, node.pname)
        return value

    def visit_Yield(self, node):
        value = self.myvisit(node.value, node.pname)
        return value

    def visit_While(self, node):
        # ignore test
        for e in node.body:
            e.pname = node.pname
            self.visit(e)
        for e in node.orelse:
            e.pname = node.pname
            self.visit(e)

    def visit_Assign(self, node):
        targets = node.targets
        node.value.pname = node.pname
        rightnames = self.visit(node.value)
        for t in targets:
            if isinstance(t, ast.Name):
                leftname = utils.getfullname(node.pname, t)
                if leftname in self.table.keys():
                    self.table[leftname] += rightnames
                    for right in rightnames:
                        self._add2table(right, leftname, self.influence, node.value)
                else:
                    self.table[leftname] = rightnames
                    for right in rightnames:
                        self._add2table(right, leftname, self.influence, node)
            elif isinstance(t, ast.Attribute):
                t.pname = node.pname
                tp = self.visit(t)
                for leftname in tp:
                    self.table[leftname] = rightnames
                    for right in rightnames:
                        self._add2table(right, leftname, self.influence, node)

    def visit_AugAssign(self, node):
        node.targets = [node.target]
        self.visit_Assign(node)

    def visit_Name(self, node):
        return [utils.getfullname(node.pname, node)]

    # node: None
    def visit_NameConstant(self, node):
        return []

    def visit_Compare(self, node):
        node.left.pname = node.pname
        left = self.visit(node.left)
        rights = self.visitlist(node.comparators, node.pname)
        return left + rights

    def visit_BoolOp(self, node):
        values = self.visitlist(node.values, node.pname)
        return values
    
    def visit_BinOp(self, node):
        # return all dependent names
        pname = node.pname
        node.left.pname = pname
        node.right.pname = pname
        lefts = self.visit(node.left)
        rights = self.visit(node.right)
        return lefts + rights

    def visit_UnaryOp(self, node):
        node.operand.pname = node.pname
        return self.visit(node.operand)

    def visit_Num(self, node):
        return []
    def visit_Ellipsis(self, node):
        return []

    def visit_Tuple(self, node):
        res = []
        for ele in node.elts:
            ele.pname = node.pname
            enames = self.visit(ele)
            for ename in enames:
                res.append(ename)
        return res

    def visit_Dict(self, node):
        keys = self.visitlist(node.keys, node.pname)
        values = self.visitlist(node.values, node.pname)
        return keys + values

    def visit_Set(self, node):
        elts = self.visitlist(node.elts, node.pname)
        return elts

    def visitlist(self, lst, pname):
        res = []
        for e in lst:
            e.pname = pname
            res += self.visit(e)
        return res
    # node: [a,4,'c']
    def visit_List(self, node):
        varibles = []
        for ele in node.elts:
            ele.pname = node.pname
            varibles += self.visit(ele)
        return varibles
    
    def visit_ListComp(self, node):
        node.elt.pname = node.pname
        elt = self.visit(node.elt)
        gens = []
        for g in node.generators:
            g.pname = node.pname
            gens += self.visit(g)
        return elt + gens

    def visit_DictComp(self, node):
        value = self.myvisit(node.value, node.pname)
        key = self.myvisit(node.key, node.pname)
        generators = self.visitlist(node.generators, node.pname)
        return value + key + generators

    # node: a ^ b for a in range(256)
    def visit_GeneratorExp(self, node):
        node.elt.pname = node.pname
        elt = self.visit(node.elt)
        gens = []
        for g in node.generators:
            g.pname = node.pname
            gens += self.visit(g)
        return elt + gens

    def visit_Str(self, node):
        return []

    # node: lst[index]
    def visit_Subscript(self, node):
        node.slice.pname = node.pname
        inds = self.visit(node.slice)
        node.value.pname = node.pname
        vals = self.visit(node.value)
        # add to table
        for v in vals:
            for i in inds:
                self._add2table(v, i, self.influence, node)
                self._add2table(i, v, self.table, node)
        return inds + vals
    def visit_Index(self, node):
        node.value.pname = node.pname
        v = self.visit(node.value)
        return v

    # node: lst[lower:upper:step]
    def visit_Slice(self, node):
        res = []
        if node.lower:
            node.lower.pname = node.pname
            lowers = self.visit(node.lower)
            res += lowers
        if node.upper:
            node.upper.pname = node.pname
            uppers = self.visit(node.upper)
            res += uppers
        if node.step:
            node.step.pname = node.pname
            steps = self.visit(node.step)
            res += steps
        return res

    def visit_ExtSlice(self, node):
        dims = self.visitlist(node.dims, node.pname)
        return dims
        
    def visit_SetComp(self, node):
        # handle generators only
        res = []
        for g in node.generators:
            g.pname = node.pname
            res += self.visit(g)
        return res

    def visit_comprehension(self, node):
        res = []
        # handle iter only
        node.iter.pname = node.pname
        res += self.visit(node.iter)
        return res
    
    def visit_Bytes(self, node):
        return []

    def visit_Attribute(self, node: ast.Attribute):
        res = []
        # self.log.info('visit attribute : %s'%astpretty.pformat(node))
        node.value.pname = node.pname
        values = self.visit(node.value)
        attr = node.attr
        for v in values:
            fullname = utils.namejoin(v, attr)
            res.append(fullname)
        return res
        



