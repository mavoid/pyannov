import ast
import log
import logging
import config
import utils

# visitor for modified ast node
class Visitor1(ast.NodeTransformer):
    def __init__(self, infos: {str}, module):
        self.log = logging.getLogger(self.__class__.__name__)
        self.vars = infos
        self.module = module
        self.pyiModule = config.pyimodule

    def generic_visit(self, node):
        node.pname = None
        return node

    def nodeid(self, node):
        localname = node.name
        if localname is None:
            return None
        elif node.pname is None:
            return localname
        else:
            return node.pname + '.'+localname
    
    def isDefinedName(self, name, defined):
        for df in defined:
            if name.startswith(df):
                return True
        return False

    def findvars(self, prefix, exo: [str] = []):
        # find variables from self.vars by function name
        res = []
        for v in self.vars:
            if v.startswith(prefix):
                if not self.isDefinedName(v, exo):
                    res.append(v)
                # remove from self.vars 
                # self.vars.remove(v)
        return res

    def visit_Module(self, node):
        body = []
        funOrClsNames = []
        for e in node.body:
            e.pname = None
            if isinstance(e, (ast.ClassDef, ast.FunctionDef)):
                funOrClsNames.append(e.name)
            body.append(self.visit(e))
        vs = self.findvars('', funOrClsNames)
        node.body = body
        self.put2Module(vs, node)
        # put import in the beginning
        node.body.insert(0, ast.Import(names=[ast.alias(name=self.pyiModule,asname=None)]))
        return node

    def visit_ClassDef(self, node):
        clsname = self.nodeid(node)
        vs = self.findvars(clsname)
        newbody = []
        for exp in node.body:
            exp.pname = clsname
            newbody.append(self.visit(exp))
        node.body = newbody
        self.put2Def(vs, node)
        return node

    def visit_FunctionDef(self, node):
        fname = self.nodeid(node)
        vs = self.findvars(fname)
        # add ejected code to beginning of function body
        self.put2Def(vs, node)

        return node
        # return ast.copy_location(Subscript(
        #     value=Name(id='data', ctx=Load()),
        #     slice=Index(value=Str(s=node.id)),
        #     ctx=node.ctx
        # ), node)

    def put2Module(self, varlist, moduleNode):
        for vname in varlist:
            moduleNode.body.insert(0, self.ejectedCode(vname, vname))

    def put2Def(self, varlist, funcnode):
        fname = funcnode.name
        for vname in varlist:
            funcnode.body.insert(0, self.ejectedCode(vname, utils.removePrefix(vname, fname+'.')))

    def ejectedCode(self, vname, srcid):
        # this name must be same as in pyi files
        callname = config.annotationPrefix + '_' + utils.uniFuncName(self.module, vname)
        return ast.Assign(targets=[ast.Name(id=srcid)],
                value=self.buidAttribute(self.pyiModule, callname), 
                    keywords=[],
                    args=[])

    def buidAttribute(self, v, attr):
        return ast.Attribute(value=ast.Name(id=v), attr=ast.Name(id=attr))

    

