import annotations
CIMultiDictProxy = annotations.annot_aiohttp_typedefs_CIMultiDictProxy
str = annotations.annot_aiohttp_typedefs_str
Iterable = annotations.annot_aiohttp_typedefs_Iterable
MultiDictProxy = annotations.annot_aiohttp_typedefs_MultiDictProxy
Callable = annotations.annot_aiohttp_typedefs_Callable
Union = annotations.annot_aiohttp_typedefs_Union
CIMultiDict = annotations.annot_aiohttp_typedefs_CIMultiDict
MultiDict = annotations.annot_aiohttp_typedefs_MultiDict
Mapping = annotations.annot_aiohttp_typedefs_Mapping
Tuple = annotations.annot_aiohttp_typedefs_Tuple
import json
import os
import pathlib
import sys
from typing import TYPE_CHECKING, Any, Callable, Iterable, Mapping, Tuple, Union
from multidict import CIMultiDict, CIMultiDictProxy, MultiDict, MultiDictProxy
from yarl import URL
DEFAULT_JSON_ENCODER = json.dumps
DEFAULT_JSON_DECODER = json.loads
if TYPE_CHECKING:
    _CIMultiDict = CIMultiDict[str]
    _CIMultiDictProxy = CIMultiDictProxy[str]
    _MultiDict = MultiDict[str]
    _MultiDictProxy = MultiDictProxy[str]
    from http.cookies import BaseCookie
else:
    _CIMultiDict = CIMultiDict
    _CIMultiDictProxy = CIMultiDictProxy
    _MultiDict = MultiDict
    _MultiDictProxy = MultiDictProxy
Byteish = Union[bytes, bytearray, memoryview]
JSONEncoder = Callable[[Any], str]
JSONDecoder = Callable[[str], Any]
LooseHeaders = Union[Mapping[str, str], _CIMultiDict, _CIMultiDictProxy]
RawHeaders = Tuple[Tuple[bytes, bytes], ...]
StrOrURL = Union[str, URL]
LooseCookies = Union[Iterable[Tuple[str, 'BaseCookie[str]']], Mapping[str,
    'BaseCookie[str]'], 'BaseCookie[str]']
if sys.version_info >= (3, 6):
    PathLike = Union[str, 'os.PathLike[str]']
else:
    PathLike = Union[str, pathlib.PurePath]
