import annotations
author = annotations.annot_docs_conf_author
master_doc = annotations.annot_docs_conf_master_doc
import os
import sys
from recommonmark.parser import CommonMarkParser
from recommonmark.transform import AutoStructify
root_directory = os.path.dirname(os.getcwd())
sys.path.insert(0, root_directory)
import sanic
extensions = ['sphinx.ext.autodoc', 'sphinxcontrib.asyncio']
templates_path = ['_templates']
source_parsers = {'.md': CommonMarkParser}
source_suffix = ['.rst', '.md']
master_doc = 'index'
project = 'Sanic'
copyright = '2018, Sanic contributors'
author = 'Sanic contributors'
version = sanic.__version__
release = sanic.__version__
language = 'en'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'modules.rst']
pygments_style = 'sphinx'
todo_include_todos = False
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
htmlhelp_basename = 'Sanicdoc'
latex_elements = {}
latex_documents = [(master_doc, 'Sanic.tex', 'Sanic Documentation',
    'Sanic contributors', 'manual')]
man_pages = [(master_doc, 'sanic', 'Sanic Documentation', [author], 1)]
texinfo_documents = [(master_doc, 'Sanic', 'Sanic Documentation', author,
    'Sanic', 'One line description of project.', 'Miscellaneous')]
epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright
epub_exclude_files = ['search.html']
suppress_warnings = ['image.nonlocal_uri']


def setup(app):
    app.add_config_value('recommonmark_config', {'enable_eval_rst': True,
        'enable_auto_doc_ref': True}, True)
    app.add_transform(AutoStructify)
