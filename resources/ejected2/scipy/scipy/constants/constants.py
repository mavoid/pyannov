import annotations
_cd = annotations.annot_scipy_constants_constants__cd
"""
Collection of physical constants and conversion factors.

Most constants are in SI units, so you can do
print '10 mile per minute is', 10*mile/minute, 'm/s or', 10*mile/(minute*knot), 'knots'

The list is not meant to be comprehensive, but just a convenient list for everyday use.
"""
from __future__ import division, print_function, absolute_import
"""
BasSw 2006
physical constants: imported from CODATA
unit conversion: see e.g. NIST special publication 811
Use at own risk: double-check values before calculating your Mars orbit-insertion burn.
Some constants exist in a few variants, which are marked with suffixes.
The ones without any suffix should be the most common one.
"""
import math as _math
from .codata import value as _cd
import numpy as _np
pi = _math.pi
golden = golden_ratio = (1 + _math.sqrt(5)) / 2
yotta = 1e+24
zetta = 1e+21
exa = 1e+18
peta = 1000000000000000.0
tera = 1000000000000.0
giga = 1000000000.0
mega = 1000000.0
kilo = 1000.0
hecto = 100.0
deka = 10.0
deci = 0.1
centi = 0.01
milli = 0.001
micro = 1e-06
nano = 1e-09
pico = 1e-12
femto = 1e-15
atto = 1e-18
zepto = 1e-21
kibi = 2 ** 10
mebi = 2 ** 20
gibi = 2 ** 30
tebi = 2 ** 40
pebi = 2 ** 50
exbi = 2 ** 60
zebi = 2 ** 70
yobi = 2 ** 80
c = speed_of_light = _cd('speed of light in vacuum')
mu_0 = 4e-07 * pi
epsilon_0 = 1 / (mu_0 * c * c)
h = Planck = _cd('Planck constant')
hbar = h / (2 * pi)
G = gravitational_constant = _cd('Newtonian constant of gravitation')
g = _cd('standard acceleration of gravity')
e = elementary_charge = _cd('elementary charge')
R = gas_constant = _cd('molar gas constant')
alpha = fine_structure = _cd('fine-structure constant')
N_A = Avogadro = _cd('Avogadro constant')
k = Boltzmann = _cd('Boltzmann constant')
sigma = Stefan_Boltzmann = _cd('Stefan-Boltzmann constant')
Wien = _cd('Wien wavelength displacement law constant')
Rydberg = _cd('Rydberg constant')
gram = 0.001
metric_ton = 1000.0
grain = 6.479891e-05
lb = pound = 7000 * grain
blob = slinch = pound * g / 0.0254
slug = blob / 12
oz = ounce = pound / 16
stone = 14 * pound
long_ton = 2240 * pound
short_ton = 2000 * pound
troy_ounce = 480 * grain
troy_pound = 12 * troy_ounce
carat = 0.0002
m_e = electron_mass = _cd('electron mass')
m_p = proton_mass = _cd('proton mass')
m_n = neutron_mass = _cd('neutron mass')
m_u = u = atomic_mass = _cd('atomic mass constant')
degree = pi / 180
arcmin = arcminute = degree / 60
arcsec = arcsecond = arcmin / 60
minute = 60.0
hour = 60 * minute
day = 24 * hour
week = 7 * day
year = 365 * day
Julian_year = 365.25 * day
inch = 0.0254
foot = 12 * inch
yard = 3 * foot
mile = 1760 * yard
mil = inch / 1000
pt = point = inch / 72
survey_foot = 1200.0 / 3937
survey_mile = 5280 * survey_foot
nautical_mile = 1852.0
fermi = 1e-15
angstrom = 1e-10
micron = 1e-06
au = astronomical_unit = 149597870700.0
light_year = Julian_year * c
parsec = au / arcsec
atm = atmosphere = _cd('standard atmosphere')
bar = 100000.0
torr = mmHg = atm / 760
psi = pound * g / (inch * inch)
hectare = 10000.0
acre = 43560 * foot ** 2
litre = liter = 0.001
gallon = gallon_US = 231 * inch ** 3
fluid_ounce = fluid_ounce_US = gallon_US / 128
bbl = barrel = 42 * gallon_US
gallon_imp = 0.00454609
fluid_ounce_imp = gallon_imp / 160
kmh = 1000.0 / hour
mph = mile / hour
mach = speed_of_sound = 340.5
knot = nautical_mile / hour
zero_Celsius = 273.15
degree_Fahrenheit = 1 / 1.8
eV = electron_volt = elementary_charge
calorie = calorie_th = 4.184
calorie_IT = 4.1868
erg = 1e-07
Btu_th = pound * degree_Fahrenheit * calorie_th / gram
Btu = Btu_IT = pound * degree_Fahrenheit * calorie_IT / gram
ton_TNT = 1000000000.0 * calorie_th
hp = horsepower = 550 * foot * pound * g
dyn = dyne = 1e-05
lbf = pound_force = pound * g
kgf = kilogram_force = g


def convert_temperature(val, old_scale, new_scale):
    """
    Convert from a temperature scale to another one among Celsius, Kelvin,
    Fahrenheit and Rankine scales.

    Parameters
    ----------
    val : array_like
        Value(s) of the temperature(s) to be converted expressed in the
        original scale.

    old_scale: str
        Specifies as a string the original scale from which the temperature
        value(s) will be converted. Supported scales are Celsius ('Celsius',
        'celsius', 'C' or 'c'), Kelvin ('Kelvin', 'kelvin', 'K', 'k'),
        Fahrenheit ('Fahrenheit', 'fahrenheit', 'F' or 'f') and Rankine
        ('Rankine', 'rankine', 'R', 'r').

    new_scale: str
        Specifies as a string the new scale to which the temperature
        value(s) will be converted. Supported scales are Celsius ('Celsius',
        'celsius', 'C' or 'c'), Kelvin ('Kelvin', 'kelvin', 'K', 'k'),
        Fahrenheit ('Fahrenheit', 'fahrenheit', 'F' or 'f') and Rankine
        ('Rankine', 'rankine', 'R', 'r').

    Returns
    -------
    res : float or array of floats
        Value(s) of the converted temperature(s) expressed in the new scale.

    Notes
    -----
    .. versionadded:: 0.18.0

    Examples
    --------
    >>> from scipy.constants import convert_temperature
    >>> convert_temperature(np.array([-40, 40.0]), 'Celsius', 'Kelvin')
    array([ 233.15,  313.15])

    """
    if old_scale.lower() in ['celsius', 'c']:
        tempo = _np.asanyarray(val) + zero_Celsius
    elif old_scale.lower() in ['kelvin', 'k']:
        tempo = _np.asanyarray(val)
    elif old_scale.lower() in ['fahrenheit', 'f']:
        tempo = (_np.asanyarray(val) - 32.0) * 5.0 / 9.0 + zero_Celsius
    elif old_scale.lower() in ['rankine', 'r']:
        tempo = _np.asanyarray(val) * 5.0 / 9.0
    else:
        raise NotImplementedError(
            '%s scale is unsupported: supported scales are Celsius, Kelvin, Fahrenheit and Rankine'
             % old_scale)
    if new_scale.lower() in ['celsius', 'c']:
        res = tempo - zero_Celsius
    elif new_scale.lower() in ['kelvin', 'k']:
        res = tempo
    elif new_scale.lower() in ['fahrenheit', 'f']:
        res = (tempo - zero_Celsius) * 9.0 / 5.0 + 32.0
    elif new_scale.lower() in ['rankine', 'r']:
        res = tempo * 9.0 / 5.0
    else:
        raise NotImplementedError(
            "'%s' scale is unsupported: supported scales are 'Celsius', 'Kelvin', 'Fahrenheit' and 'Rankine'"
             % new_scale)
    return res


def lambda2nu(lambda_):
    """
    Convert wavelength to optical frequency

    Parameters
    ----------
    lambda_ : array_like
        Wavelength(s) to be converted.

    Returns
    -------
    nu : float or array of floats
        Equivalent optical frequency.

    Notes
    -----
    Computes ``nu = c / lambda`` where c = 299792458.0, i.e., the
    (vacuum) speed of light in meters/second.

    Examples
    --------
    >>> from scipy.constants import lambda2nu, speed_of_light
    >>> lambda2nu(np.array((1, speed_of_light)))
    array([  2.99792458e+08,   1.00000000e+00])

    """
    return _np.asanyarray(c) / lambda_


def nu2lambda(nu):
    """
    Convert optical frequency to wavelength.

    Parameters
    ----------
    nu : array_like
        Optical frequency to be converted.

    Returns
    -------
    lambda : float or array of floats
        Equivalent wavelength(s).

    Notes
    -----
    Computes ``lambda = c / nu`` where c = 299792458.0, i.e., the
    (vacuum) speed of light in meters/second.

    Examples
    --------
    >>> from scipy.constants import nu2lambda, speed_of_light
    >>> nu2lambda(np.array((1, speed_of_light)))
    array([  2.99792458e+08,   1.00000000e+00])

    """
    return c / _np.asanyarray(nu)
