import annotations
List = annotations.annot_zerver_lib_types_List
Callable = annotations.annot_zerver_lib_types_Callable
Union = annotations.annot_zerver_lib_types_Union
Dict = annotations.annot_zerver_lib_types_Dict
Optional = annotations.annot_zerver_lib_types_Optional
Tuple = annotations.annot_zerver_lib_types_Tuple
from typing import TypeVar, Callable, Optional, List, Dict, Union, Tuple, Any
from django.http import HttpResponse
ViewFuncT = TypeVar('ViewFuncT', bound=Callable[..., HttpResponse])
Validator = Callable[[str, object], Optional[str]]
ExtendedValidator = Callable[[str, str, object], Optional[str]]
RealmUserValidator = Callable[[int, List[int], bool], Optional[str]]
ProfileDataElement = Dict[str, Union[int, float, Optional[str]]]
ProfileData = List[ProfileDataElement]
FieldElement = Tuple[int, str, Validator, Callable[[Any], Any], str]
ExtendedFieldElement = Tuple[int, str, ExtendedValidator, Callable[[Any],
    Any], str]
UserFieldElement = Tuple[int, str, RealmUserValidator, Callable[[Any], Any],
    str]
FieldTypeData = List[Union[FieldElement, ExtendedFieldElement,
    UserFieldElement]]
ProfileFieldData = Dict[str, Dict[str, str]]
