import annotations
os.path.join = annotations.annot_zproject_settings_os_path_join
from copy import deepcopy
import os
import platform
import time
import sys
from typing import Any, Optional
import configparser
from zerver.lib.db import TimeTrackingConnection
import zerver.lib.logging_util
DEPLOY_ROOT = os.path.join(os.path.realpath(os.path.dirname(__file__)), '..')
config_file = configparser.RawConfigParser()
config_file.read('/etc/zulip/zulip.conf')
PRODUCTION = config_file.has_option('machine', 'deploy_type')
DEVELOPMENT = not PRODUCTION
secrets_file = configparser.RawConfigParser()
if PRODUCTION:
    secrets_file.read('/etc/zulip/zulip-secrets.conf')
else:
    secrets_file.read(os.path.join(DEPLOY_ROOT, 'zproject/dev-secrets.conf'))


def get_secret(key: str, default_value: Optional[Any]=None,
    development_only: bool=False) ->Optional[Any]:
    get_secret = annotations.annot_zproject_settings_get_secret
    if development_only and PRODUCTION:
        return default_value
    if secrets_file.has_option('secrets', key):
        return secrets_file.get('secrets', key)
    return default_value


def get_config(section: str, key: str, default_value: Optional[Any]=None
    ) ->Optional[Any]:
    if config_file.has_option(section, key):
        return config_file.get(section, key)
    return default_value


SECRET_KEY = get_secret('secret_key')
SHARED_SECRET = get_secret('shared_secret')
AVATAR_SALT = get_secret('avatar_salt')
SERVER_GENERATION = int(time.time())
ZULIP_ORG_KEY = get_secret('zulip_org_key')
ZULIP_ORG_ID = get_secret('zulip_org_id')
if 'DEBUG' not in globals():
    DEBUG = DEVELOPMENT
if DEBUG:
    INTERNAL_IPS = '127.0.0.1',
if len(sys.argv) > 2 and sys.argv[0].endswith('manage.py') and sys.argv[1
    ] == 'process_queue':
    IS_WORKER = True
else:
    IS_WORKER = False
TEST_SUITE = False
TUTORIAL_ENABLED = True
CASPER_TESTS = False
os.environ['BOTO_CONFIG'] = '/etc/zulip/boto.cfg'
if PRODUCTION:
    from .prod_settings import *
else:
    from .dev_settings import *
DEFAULT_SETTINGS = {'ALLOWED_HOSTS': [], 'NOREPLY_EMAIL_ADDRESS': 
    'noreply@' + EXTERNAL_HOST.split(':')[0],
    'ADD_TOKENS_TO_NOREPLY_ADDRESS': True,
    'TOKENIZED_NOREPLY_EMAIL_ADDRESS': 'noreply-{token}@' + EXTERNAL_HOST.
    split(':')[0], 'PHYSICAL_ADDRESS': '', 'EMAIL_HOST': None,
    'GOOGLE_OAUTH2_CLIENT_ID': None, 'AUTH_LDAP_SERVER_URI': '',
    'LDAP_EMAIL_ATTR': None, 'AUTH_LDAP_CONNECTION_OPTIONS': {},
    'AUTH_LDAP_CACHE_TIMEOUT': 0, 'FAKE_LDAP_MODE': None,
    'FAKE_LDAP_NUM_USERS': 8, 'SOCIAL_AUTH_GITHUB_KEY': get_secret(
    'social_auth_github_key', development_only=True),
    'SOCIAL_AUTH_GITHUB_ORG_NAME': None, 'SOCIAL_AUTH_GITHUB_TEAM_ID': None,
    'SOCIAL_AUTH_SUBDOMAIN': None, 'SOCIAL_AUTH_AZUREAD_OAUTH2_SECRET':
    get_secret('azure_oauth2_secret'), 'EMAIL_GATEWAY_PATTERN': '',
    'EMAIL_GATEWAY_LOGIN': None, 'EMAIL_GATEWAY_IMAP_SERVER': None,
    'EMAIL_GATEWAY_IMAP_PORT': None, 'EMAIL_GATEWAY_IMAP_FOLDER': None,
    'EMAIL_GATEWAY_EXTRA_PATTERN_HACK': None, 'ERROR_REPORTING': True,
    'BROWSER_ERROR_REPORTING': False, 'LOGGING_SHOW_MODULE': False,
    'LOGGING_SHOW_PID': False, 'SLOW_QUERY_LOGS_STREAM': None,
    'DEFAULT_AVATAR_URI': '/static/images/default-avatar.png',
    'DEFAULT_LOGO_URI': '/static/images/logo/zulip-org-logo.png',
    'S3_AVATAR_BUCKET': '', 'S3_AUTH_UPLOADS_BUCKET': '', 'S3_REGION': '',
    'LOCAL_UPLOADS_DIR': None, 'MAX_FILE_UPLOAD_SIZE': 25,
    'JITSI_SERVER_URL': 'https://meet.jit.si/', 'ENABLE_FEEDBACK':
    PRODUCTION, 'FEEDBACK_EMAIL': None, 'USER_STATE_SIZE_LIMIT': 10000000,
    'BOT_CONFIG_SIZE_LIMIT': 10000, 'CAMO_URI': '', 'MEMCACHED_LOCATION':
    '127.0.0.1:11211', 'RABBITMQ_HOST': '127.0.0.1', 'RABBITMQ_USERNAME':
    'zulip', 'REDIS_HOST': '127.0.0.1', 'REDIS_PORT': 6379,
    'REMOTE_POSTGRES_HOST': '', 'REMOTE_POSTGRES_SSLMODE': '',
    'THUMBOR_URL': '', 'SENDFILE_BACKEND': None, 'PRIVACY_POLICY': None,
    'TERMS_OF_SERVICE': None, 'ENABLE_FILE_LINKS': False, 'ENABLE_GRAVATAR':
    True, 'INLINE_IMAGE_PREVIEW': True, 'INLINE_URL_EMBED_PREVIEW': False,
    'NAME_CHANGES_DISABLED': False, 'PASSWORD_MIN_LENGTH': 6,
    'PASSWORD_MIN_GUESSES': 10000, 'PUSH_NOTIFICATION_BOUNCER_URL': None,
    'PUSH_NOTIFICATION_REDACT_CONTENT': False, 'RATE_LIMITING': True,
    'SEND_LOGIN_EMAILS': True, 'EMBEDDED_BOTS_ENABLED': False,
    'SEND_REMOVE_PUSH_NOTIFICATIONS': False,
    'TWO_FACTOR_AUTHENTICATION_ENABLED': False, 'ALWAYS_SEND_ALL_HOTSPOTS':
    False, 'SEARCH_PILLS_ENABLED': False, 'ALLOW_SUB_MESSAGES': DEVELOPMENT,
    'DEVELOPMENT_LOG_EMAILS': DEVELOPMENT}
DEFAULT_SETTINGS.update({'ERROR_BOT': None, 'NAGIOS_STAGING_SEND_BOT': None,
    'NAGIOS_STAGING_RECEIVE_BOT': None, 'FEEDBACK_BOT':
    'feedback@zulip.com', 'FEEDBACK_BOT_NAME': 'Zulip Feedback Bot',
    'FEEDBACK_STREAM': None, 'SYSTEM_BOT_REALM': 'zulip',
    'EXTRA_INSTALLED_APPS': ['analytics'], 'GOOGLE_CLIENT_ID':
    '835904834568-77mtr5mtmpgspj9b051del9i9r5t4g4n.apps.googleusercontent.com',
    'EVENT_LOGS_ENABLED': False, 'EXTERNAL_URI_SCHEME': 'https://',
    'OPEN_REALM_CREATION': False, 'SYSTEM_ONLY_REALMS': {'zulip'},
    'REALM_HOSTS': {}, 'USING_PGROONGA': False, 'EMAIL_BACKEND': None,
    'WARN_NO_EMAIL': False, 'SAVE_FRONTEND_STACKTRACES': False,
    'DEBUG_ERROR_REPORTING': False, 'POST_MIGRATION_CACHE_FLUSHING': False,
    'APNS_CERT_FILE': None, 'APNS_SANDBOX': True,
    'DATA_UPLOAD_MAX_MEMORY_SIZE': 25 * 1024 * 1024, 'MAX_AVATAR_FILE_SIZE':
    5, 'MAX_ICON_FILE_SIZE': 5, 'MAX_LOGO_FILE_SIZE': 5,
    'MAX_EMOJI_FILE_SIZE': 5, 'INVITES_MIN_USER_AGE_DAYS': 3,
    'INVITES_DEFAULT_REALM_DAILY_MAX': 100, 'INVITES_NEW_REALM_LIMIT_DAYS':
    [(1, 100)], 'INVITES_NEW_REALM_DAYS': 7, 'REGISTER_LINK_DISABLED': None,
    'LOGIN_LINK_DISABLED': False, 'FIND_TEAM_LINK_DISABLED': True,
    'EMAIL_DELIVERER_DISABLED': False, 'ROOT_SUBDOMAIN_ALIASES': ['www',
    'auth'], 'ROOT_DOMAIN_LANDING_PAGE': False, 'PERSONAL_ZMIRROR_SERVER':
    None, 'CONFIRMATION_LINK_DEFAULT_VALIDITY_DAYS': 1,
    'INVITATION_LINK_VALIDITY_DAYS': 10,
    'REALM_CREATION_LINK_VALIDITY_DAYS': 7, 'USE_WEBSOCKETS': True,
    'TOS_VERSION': None, 'FIRST_TIME_TOS_TEMPLATE': None, 'STATSD_HOST': '',
    'JWT_AUTH_KEYS': {}, 'SERVER_EMAIL': None, 'ADMINS': '',
    'WELCOME_EMAIL_SENDER': None, 'SEND_MISSED_MESSAGE_EMAILS_AS_USER':
    False, 'SEND_DIGEST_EMAILS': False, 'CUSTOM_LOGO_URL': None,
    'INITIAL_PASSWORD_SALT': None, 'STAGING': False,
    'STAGING_ERROR_NOTIFICATIONS': False, 'OFFLINE_THRESHOLD_SECS': 5 * 60,
    'BILLING_ENABLED': False})
for setting_name, setting_val in DEFAULT_SETTINGS.items():
    if setting_name not in vars():
        vars()[setting_name] = setting_val
REQUIRED_SETTINGS = [('EXTERNAL_HOST', 'zulip.example.com'), (
    'ZULIP_ADMINISTRATOR', 'zulip-admin@example.com'), ('SECRET_KEY', ''),
    ('AUTHENTICATION_BACKENDS', ())]
if ADMINS == '':
    ADMINS = ('Zulip Administrator', ZULIP_ADMINISTRATOR),
MANAGERS = ADMINS
TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = True
DEPLOY_ROOT = os.path.join(os.path.realpath(os.path.dirname(__file__)), '..')
DEVELOPMENT_LOG_DIRECTORY = os.path.join(DEPLOY_ROOT, 'var', 'log')
USE_X_FORWARDED_HOST = True
ALLOWED_HOSTS += ['127.0.0.1', 'localhost']
ALLOWED_HOSTS += [EXTERNAL_HOST.split(':')[0], '.' + EXTERNAL_HOST.split(
    ':')[0]]
ALLOWED_HOSTS += REALM_HOSTS.values()
from django.template.loaders import app_directories


class TwoFactorLoader(app_directories.Loader):

    def get_dirs(self):
        dirs = super().get_dirs()
        return [d for d in dirs if 'two_factor' in d]


MIDDLEWARE = ('zerver.middleware.TagRequests',
    'zerver.middleware.SetRemoteAddrFromForwardedFor',
    'zerver.middleware.LogRequests', 'zerver.middleware.JsonErrorHandler',
    'zerver.middleware.RateLimitMiddleware',
    'zerver.middleware.FlushDisplayRecipientCache',
    'django.middleware.common.CommonMiddleware',
    'zerver.middleware.SessionHostDomainMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'two_factor.middleware.threadlocals.ThreadLocals',
    'zerver.middleware.FinalizeOpenGraphDescription')
ANONYMOUS_USER_ID = None
AUTH_USER_MODEL = 'zerver.UserProfile'
TEST_RUNNER = 'zerver.lib.test_runner.Runner'
ROOT_URLCONF = 'zproject.urls'
WSGI_APPLICATION = 'zproject.wsgi.application'
INSTALLED_APPS = ['django.contrib.auth', 'django.contrib.contenttypes',
    'django.contrib.sessions', 'django.contrib.staticfiles', 'confirmation',
    'pipeline', 'webpack_loader', 'zerver', 'social_django', 'django_otp',
    'django_otp.plugins.otp_static', 'django_otp.plugins.otp_totp',
    'two_factor']
if USING_PGROONGA:
    INSTALLED_APPS += ['pgroonga']
INSTALLED_APPS += EXTRA_INSTALLED_APPS
ZILENCER_ENABLED = 'zilencer' in INSTALLED_APPS
CORPORATE_ENABLED = 'corporate' in INSTALLED_APPS
TORNADO_PROCESSES = int(get_config('application_server',
    'tornado_processes', 1))
TORNADO_SERVER = 'http://127.0.0.1:9993'
RUNNING_INSIDE_TORNADO = False
AUTORELOAD = DEBUG
SILENCED_SYSTEM_CHECKS = ['auth.W004']
DATABASES = {'default': {'ENGINE': 'django.db.backends.postgresql', 'NAME':
    'zulip', 'USER': 'zulip', 'PASSWORD': '', 'HOST': '', 'SCHEMA': 'zulip',
    'CONN_MAX_AGE': 600, 'OPTIONS': {'connection_factory':
    TimeTrackingConnection}}}
if DEVELOPMENT:
    LOCAL_DATABASE_PASSWORD = get_secret('local_database_password')
    DATABASES['default'].update({'PASSWORD': LOCAL_DATABASE_PASSWORD,
        'HOST': 'localhost'})
elif REMOTE_POSTGRES_HOST != '':
    DATABASES['default'].update({'HOST': REMOTE_POSTGRES_HOST})
    if get_secret('postgres_password') is not None:
        DATABASES['default'].update({'PASSWORD': get_secret(
            'postgres_password')})
    if REMOTE_POSTGRES_SSLMODE != '':
        DATABASES['default']['OPTIONS']['sslmode'] = REMOTE_POSTGRES_SSLMODE
    else:
        DATABASES['default']['OPTIONS']['sslmode'] = 'verify-full'
USING_RABBITMQ = True
RABBITMQ_PASSWORD = get_secret('rabbitmq_password')
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
PYLIBMC_MIN_COMPRESS_LEN = 100 * 1024
PYLIBMC_COMPRESS_LEVEL = 1
CACHES = {'default': {'BACKEND': 'django_pylibmc.memcached.PyLibMCCache',
    'LOCATION': MEMCACHED_LOCATION, 'TIMEOUT': 3600, 'OPTIONS': {
    'verify_keys': True, 'tcp_nodelay': True, 'retry_timeout': 1}},
    'database': {'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
    'LOCATION': 'third_party_api_results', 'TIMEOUT': None, 'OPTIONS': {
    'MAX_ENTRIES': 100000000, 'CULL_FREQUENCY': 10}}}
RATE_LIMITING_RULES = [(60, 200)]
DEBUG_RATE_LIMITING = DEBUG
REDIS_PASSWORD = get_secret('redis_password')
if PRODUCTION:
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    domain = get_config('django', 'cookie_domain', None)
    if domain is not None:
        CSRF_COOKIE_DOMAIN = '.' + domain
CSRF_COOKIE_PATH = '/;HttpOnly'
CSRF_FAILURE_VIEW = 'zerver.middleware.csrf_failure'
if DEVELOPMENT:
    PASSWORD_HASHERS = ('django.contrib.auth.hashers.SHA1PasswordHasher',
        'django.contrib.auth.hashers.PBKDF2PasswordHasher')
    INITIAL_PASSWORD_SALT = get_secret('initial_password_salt')
else:
    PASSWORD_HASHERS = ('django.contrib.auth.hashers.Argon2PasswordHasher',
        'django.contrib.auth.hashers.PBKDF2PasswordHasher')
ROOT_DOMAIN_URI = EXTERNAL_URI_SCHEME + EXTERNAL_HOST
if 'NAGIOS_BOT_HOST' not in vars():
    NAGIOS_BOT_HOST = EXTERNAL_HOST
S3_KEY = get_secret('s3_key')
S3_SECRET_KEY = get_secret('s3_secret_key')
if LOCAL_UPLOADS_DIR is not None:
    if SENDFILE_BACKEND is None:
        SENDFILE_BACKEND = 'sendfile.backends.nginx'
    SENDFILE_ROOT = os.path.join(LOCAL_UPLOADS_DIR, 'files')
    SENDFILE_URL = '/serve_uploads'
ANDROID_GCM_API_KEY = get_secret('android_gcm_api_key')
GOOGLE_OAUTH2_CLIENT_SECRET = get_secret('google_oauth2_client_secret')
DROPBOX_APP_KEY = get_secret('dropbox_app_key')
MAILCHIMP_API_KEY = get_secret('mailchimp_api_key')
TWITTER_CONSUMER_KEY = get_secret('twitter_consumer_key')
TWITTER_CONSUMER_SECRET = get_secret('twitter_consumer_secret')
TWITTER_ACCESS_TOKEN_KEY = get_secret('twitter_access_token_key')
TWITTER_ACCESS_TOKEN_SECRET = get_secret('twitter_access_token_secret')
INTERNAL_BOTS = [{'var_name': 'NOTIFICATION_BOT', 'email_template':
    'notification-bot@%s', 'name': 'Notification Bot'}, {'var_name':
    'EMAIL_GATEWAY_BOT', 'email_template': 'emailgateway@%s', 'name':
    'Email Gateway'}, {'var_name': 'NAGIOS_SEND_BOT', 'email_template':
    'nagios-send-bot@%s', 'name': 'Nagios Send Bot'}, {'var_name':
    'NAGIOS_RECEIVE_BOT', 'email_template': 'nagios-receive-bot@%s', 'name':
    'Nagios Receive Bot'}, {'var_name': 'WELCOME_BOT', 'email_template':
    'welcome-bot@%s', 'name': 'Welcome Bot'}]
REALM_INTERNAL_BOTS = []
DISABLED_REALM_INTERNAL_BOTS = [{'var_name': 'REMINDER_BOT',
    'email_template': 'reminder-bot@%s', 'name': 'Reminder Bot'}]
if PRODUCTION:
    INTERNAL_BOTS += [{'var_name': 'NAGIOS_STAGING_SEND_BOT',
        'email_template': 'nagios-staging-send-bot@%s', 'name':
        'Nagios Staging Send Bot'}, {'var_name':
        'NAGIOS_STAGING_RECEIVE_BOT', 'email_template':
        'nagios-staging-receive-bot@%s', 'name': 'Nagios Staging Receive Bot'}]
INTERNAL_BOT_DOMAIN = 'zulip.com'
for bot in (INTERNAL_BOTS + REALM_INTERNAL_BOTS + DISABLED_REALM_INTERNAL_BOTS
    ):
    if vars().get(bot['var_name']) is None:
        bot_email = bot['email_template'] % (INTERNAL_BOT_DOMAIN,)
        vars()[bot['var_name']] = bot_email
if EMAIL_GATEWAY_PATTERN != '':
    EMAIL_GATEWAY_EXAMPLE = EMAIL_GATEWAY_PATTERN % ('support+abcdefg',)
else:
    EMAIL_GATEWAY_EXAMPLE = ''
if STATSD_HOST != '':
    INSTALLED_APPS += ['django_statsd']
    STATSD_PORT = 8125
    STATSD_CLIENT = 'django_statsd.clients.normal'
if CAMO_URI != '':
    CAMO_KEY = get_secret('camo_key')
STATIC_URL = '/static/'
PIPELINE_ENABLED = not DEBUG
if DEBUG:
    STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'pipeline.finders.PipelineFinder')
    if PIPELINE_ENABLED:
        STATIC_ROOT = os.path.abspath(os.path.join(DEPLOY_ROOT,
            'prod-static/serve'))
    else:
        STATIC_ROOT = os.path.abspath(os.path.join(DEPLOY_ROOT, 'static/'))
else:
    STATICFILES_STORAGE = 'zerver.lib.storage.ZulipStorage'
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'pipeline.finders.PipelineFinder')
    if PRODUCTION:
        STATIC_ROOT = '/home/zulip/prod-static'
    else:
        STATIC_ROOT = os.path.abspath(os.path.join(DEPLOY_ROOT,
            'prod-static/serve'))
LOCALE_PATHS = os.path.join(STATIC_ROOT, 'locale'),
FILE_UPLOAD_MAX_MEMORY_SIZE = 0
STATICFILES_DIRS = ['static/']
STATIC_HEADER_FILE = 'zerver/static_header.txt'
PIPELINE = {'PIPELINE_ENABLED': PIPELINE_ENABLED, 'CSS_COMPRESSOR':
    'pipeline.compressors.yui.YUICompressor', 'YUI_BINARY':
    '/usr/bin/env yui-compressor', 'STYLESHEETS': {}, 'JAVASCRIPT': {}}
JS_SPECS = {'sockjs': {'source_filenames': ['third/sockjs/sockjs-0.3.4.js'],
    'output_filename': 'min/sockjs-0.3.4.min.js'}, 'katex': {
    'source_filenames': ['node_modules/katex/dist/katex.js'],
    'output_filename': 'min/katex.js'}, 'zxcvbn': {'source_filenames': [
    'node_modules/zxcvbn/dist/zxcvbn.js'], 'output_filename': 'min/zxcvbn.js'}}
if DEVELOPMENT:
    WEBPACK_STATS_FILE = os.path.join('var', 'webpack-stats-dev.json')
else:
    WEBPACK_STATS_FILE = 'webpack-stats-production.json'
WEBPACK_LOADER = {'DEFAULT': {'CACHE': not DEBUG, 'BUNDLE_DIR_NAME':
    'webpack-bundles/', 'STATS_FILE': os.path.join(DEPLOY_ROOT,
    WEBPACK_STATS_FILE)}}
LOADERS = ['django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader']
if PRODUCTION:
    LOADERS = [('django.template.loaders.cached.Loader', LOADERS)]
base_template_engine_settings = {'BACKEND':
    'django.template.backends.jinja2.Jinja2', 'OPTIONS': {'environment':
    'zproject.jinja2.environment', 'extensions': ['jinja2.ext.i18n',
    'jinja2.ext.autoescape', 'pipeline.jinja2.PipelineExtension',
    'webpack_loader.contrib.jinja2ext.WebpackExtension'],
    'context_processors': [
    'zerver.context_processors.zulip_default_context',
    'django.template.context_processors.i18n']}}
default_template_engine_settings = deepcopy(base_template_engine_settings)
default_template_engine_settings.update({'NAME': 'Jinja2', 'DIRS': [os.path
    .join(DEPLOY_ROOT, 'templates'), os.path.join(DEPLOY_ROOT, 'zerver',
    'webhooks'), os.path.join(STATIC_ROOT, 'generated', 'bots')],
    'APP_DIRS': True})
non_html_template_engine_settings = deepcopy(base_template_engine_settings)
non_html_template_engine_settings.update({'NAME': 'Jinja2_plaintext',
    'DIRS': [os.path.join(DEPLOY_ROOT, 'templates')], 'APP_DIRS': False})
non_html_template_engine_settings['OPTIONS'].update({'autoescape': False,
    'trim_blocks': True, 'lstrip_blocks': True})
two_factor_template_options = deepcopy(default_template_engine_settings[
    'OPTIONS'])
del two_factor_template_options['environment']
del two_factor_template_options['extensions']
two_factor_template_options['loaders'] = ['zproject.settings.TwoFactorLoader']
two_factor_template_engine_settings = {'NAME': 'Two_Factor', 'BACKEND':
    'django.template.backends.django.DjangoTemplates', 'DIRS': [],
    'APP_DIRS': False, 'OPTIONS': two_factor_template_options}
TEMPLATES = [default_template_engine_settings,
    non_html_template_engine_settings, two_factor_template_engine_settings]
ZULIP_PATHS = [('SERVER_LOG_PATH', '/var/log/zulip/server.log'), (
    'ERROR_FILE_LOG_PATH', '/var/log/zulip/errors.log'), (
    'MANAGEMENT_LOG_PATH', '/var/log/zulip/manage.log'), ('WORKER_LOG_PATH',
    '/var/log/zulip/workers.log'), (
    'JSON_PERSISTENT_QUEUE_FILENAME_PATTERN',
    '/home/zulip/tornado/event_queues%s.json'), ('EMAIL_LOG_PATH',
    '/var/log/zulip/send_email.log'), ('EMAIL_MIRROR_LOG_PATH',
    '/var/log/zulip/email_mirror.log'), ('EMAIL_DELIVERER_LOG_PATH',
    '/var/log/zulip/email-deliverer.log'), ('EMAIL_CONTENT_LOG_PATH',
    '/var/log/zulip/email_content.log'), ('LDAP_SYNC_LOG_PATH',
    '/var/log/zulip/sync_ldap_user_data.log'), ('QUEUE_ERROR_DIR',
    '/var/log/zulip/queue_error'), ('DIGEST_LOG_PATH',
    '/var/log/zulip/digest.log'), ('ANALYTICS_LOG_PATH',
    '/var/log/zulip/analytics.log'), ('ANALYTICS_LOCK_DIR',
    '/home/zulip/deployments/analytics-lock-dir'), (
    'API_KEY_ONLY_WEBHOOK_LOG_PATH', '/var/log/zulip/webhooks_errors.log'),
    ('SOFT_DEACTIVATION_LOG_PATH', '/var/log/zulip/soft_deactivation.log'),
    ('TRACEMALLOC_DUMP_DIR', '/var/log/zulip/tracemalloc'), (
    'SCHEDULED_MESSAGE_DELIVERER_LOG_PATH',
    '/var/log/zulip/scheduled_message_deliverer.log')]
if EVENT_LOGS_ENABLED:
    ZULIP_PATHS.append(('EVENT_LOG_DIR', '/home/zulip/logs/event_log'))
else:
    EVENT_LOG_DIR = None
for var, path in ZULIP_PATHS:
    if DEVELOPMENT:
        if path.startswith('/var/log'):
            path = os.path.join(DEVELOPMENT_LOG_DIRECTORY, os.path.basename
                (path))
        else:
            path = os.path.join(os.path.join(DEPLOY_ROOT, 'var'), os.path.
                basename(path))
    vars()[var] = path
ZULIP_WORKER_TEST_FILE = '/tmp/zulip-worker-test-file'
if IS_WORKER:
    FILE_LOG_PATH = WORKER_LOG_PATH
else:
    FILE_LOG_PATH = SERVER_LOG_PATH
LOGGING_ENABLED = True
DEFAULT_ZULIP_HANDLERS = (['zulip_admins'] if ERROR_REPORTING else []) + [
    'console', 'file', 'errors_file']
LOGGING = {'version': 1, 'disable_existing_loggers': False, 'formatters': {
    'default': {'()': 'zerver.lib.logging_util.ZulipFormatter'}}, 'filters':
    {'ZulipLimiter': {'()': 'zerver.lib.logging_util.ZulipLimiter'},
    'EmailLimiter': {'()': 'zerver.lib.logging_util.EmailLimiter'},
    'require_debug_false': {'()': 'django.utils.log.RequireDebugFalse'},
    'require_debug_true': {'()': 'django.utils.log.RequireDebugTrue'},
    'nop': {'()': 'zerver.lib.logging_util.ReturnTrue'},
    'require_logging_enabled': {'()':
    'zerver.lib.logging_util.ReturnEnabled'}, 'require_really_deployed': {
    '()': 'zerver.lib.logging_util.RequireReallyDeployed'},
    'skip_200_and_304': {'()': 'django.utils.log.CallbackFilter',
    'callback': zerver.lib.logging_util.skip_200_and_304},
    'skip_boring_404s': {'()': 'django.utils.log.CallbackFilter',
    'callback': zerver.lib.logging_util.skip_boring_404s},
    'skip_site_packages_logs': {'()': 'django.utils.log.CallbackFilter',
    'callback': zerver.lib.logging_util.skip_site_packages_logs}},
    'handlers': {'zulip_admins': {'level': 'ERROR', 'class':
    'zerver.logging_handlers.AdminNotifyHandler', 'filters': [
    'ZulipLimiter', 'require_debug_false', 'require_really_deployed'] if 
    not DEBUG_ERROR_REPORTING else [], 'formatter': 'default'}, 'console':
    {'level': 'DEBUG', 'class': 'logging.StreamHandler', 'formatter':
    'default'}, 'file': {'level': 'DEBUG', 'class':
    'logging.handlers.WatchedFileHandler', 'formatter': 'default',
    'filename': FILE_LOG_PATH}, 'errors_file': {'level': 'WARNING', 'class':
    'logging.handlers.WatchedFileHandler', 'formatter': 'default',
    'filename': ERROR_FILE_LOG_PATH}}, 'loggers': {'': {'level': 'INFO',
    'filters': ['require_logging_enabled'], 'handlers':
    DEFAULT_ZULIP_HANDLERS}, 'django': {}, 'django.request': {'level':
    'WARNING', 'filters': ['skip_boring_404s']},
    'django.security.DisallowedHost': {'handlers': ['file'], 'propagate':
    False}, 'django.server': {'filters': ['skip_200_and_304'], 'handlers':
    ['console', 'file'], 'propagate': False}, 'django.template': {'level':
    'DEBUG', 'filters': ['require_debug_true', 'skip_site_packages_logs'],
    'handlers': ['console'], 'propagate': False}, 'pika.adapters': {'level':
    'WARNING', 'handlers': ['console', 'file', 'errors_file'], 'propagate':
    False}, 'pika.connection': {'handlers': ['console', 'file',
    'errors_file'], 'propagate': False}, 'requests': {'level': 'WARNING'},
    'tornado.general': {'handlers': ['console', 'file'], 'propagate': False
    }, 'zerver.lib.digest': {'level': 'DEBUG'},
    'zerver.management.commands.deliver_email': {'level': 'DEBUG'},
    'zerver.management.commands.enqueue_digest_emails': {'level': 'DEBUG'},
    'zerver.management.commands.deliver_scheduled_messages': {'level':
    'DEBUG'}, 'zulip.management': {'handlers': ['file', 'errors_file'],
    'propagate': False}, 'zulip.queue': {'level': 'WARNING'},
    'zulip.soft_deactivation': {'handlers': ['file', 'errors_file'],
    'propagate': False}, 'zulip.zerver.webhooks': {'level': 'DEBUG',
    'handlers': ['file', 'errors_file'], 'propagate': False}}}
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = '/'
POLL_TIMEOUT = 90 * 1000
ZULIP_IOS_APP_ID = 'org.zulip.Zulip'
USING_APACHE_SSO = ('zproject.backends.ZulipRemoteUserBackend' in
    AUTHENTICATION_BACKENDS)
if len(AUTHENTICATION_BACKENDS) == 1 and AUTHENTICATION_BACKENDS[0
    ] == 'zproject.backends.ZulipRemoteUserBackend':
    HOME_NOT_LOGGED_IN = '/accounts/login/sso/'
    ONLY_SSO = True
else:
    HOME_NOT_LOGGED_IN = '/login/'
    ONLY_SSO = False
AUTHENTICATION_BACKENDS += 'zproject.backends.ZulipDummyBackend',
if DEVELOPMENT:
    HOME_NOT_LOGGED_IN = '/devlogin/'
    LOGIN_URL = '/devlogin/'
POPULATE_PROFILE_VIA_LDAP = bool(AUTH_LDAP_SERVER_URI)
if POPULATE_PROFILE_VIA_LDAP and 'zproject.backends.ZulipLDAPAuthBackend' not in AUTHENTICATION_BACKENDS:
    AUTHENTICATION_BACKENDS += 'zproject.backends.ZulipLDAPUserPopulator',
else:
    POPULATE_PROFILE_VIA_LDAP = ('zproject.backends.ZulipLDAPAuthBackend' in
        AUTHENTICATION_BACKENDS or POPULATE_PROFILE_VIA_LDAP)
if POPULATE_PROFILE_VIA_LDAP:
    import ldap
    if (AUTH_LDAP_BIND_DN and ldap.OPT_REFERRALS not in
        AUTH_LDAP_CONNECTION_OPTIONS):
        AUTH_LDAP_CONNECTION_OPTIONS[ldap.OPT_REFERRALS] = 0
if REGISTER_LINK_DISABLED is None:
    if (len(AUTHENTICATION_BACKENDS) == 2 and 
        'zproject.backends.ZulipLDAPAuthBackend' in AUTHENTICATION_BACKENDS):
        REGISTER_LINK_DISABLED = True
    else:
        REGISTER_LINK_DISABLED = False
SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['subdomain', 'is_signup',
    'mobile_flow_otp']
SOCIAL_AUTH_LOGIN_ERROR_URL = '/login/'
SOCIAL_AUTH_GITHUB_SECRET = get_secret('social_auth_github_secret')
SOCIAL_AUTH_GITHUB_SCOPE = ['user:email']
SOCIAL_AUTH_GITHUB_ORG_KEY = SOCIAL_AUTH_GITHUB_KEY
SOCIAL_AUTH_GITHUB_ORG_SECRET = SOCIAL_AUTH_GITHUB_SECRET
SOCIAL_AUTH_GITHUB_TEAM_KEY = SOCIAL_AUTH_GITHUB_KEY
SOCIAL_AUTH_GITHUB_TEAM_SECRET = SOCIAL_AUTH_GITHUB_SECRET
SOCIAL_AUTH_PIPELINE = ['social_core.pipeline.social_auth.social_details',
    'zproject.backends.social_auth_associate_user',
    'zproject.backends.social_auth_finish']
DEFAULT_FROM_EMAIL = ZULIP_ADMINISTRATOR
if EMAIL_BACKEND is not None:
    pass
elif DEVELOPMENT:
    EMAIL_BACKEND = 'zproject.email_backends.EmailLogBackEnd'
elif not EMAIL_HOST:
    WARN_NO_EMAIL = True
    EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_PASSWORD = get_secret('email_password')
EMAIL_GATEWAY_PASSWORD = get_secret('email_gateway_password')
AUTH_LDAP_BIND_PASSWORD = get_secret('auth_ldap_bind_password', '')
if SERVER_EMAIL is None:
    SERVER_EMAIL = ZULIP_ADMINISTRATOR
if PRODUCTION:
    DEFAULT_EXCEPTION_REPORTER_FILTER = (
        'zerver.filters.ZulipExceptionReporterFilter')
PROFILE_ALL_REQUESTS = False
CROSS_REALM_BOT_EMAILS = {'feedback@zulip.com',
    'notification-bot@zulip.com', 'welcome-bot@zulip.com',
    'new-user-bot@zulip.com', 'emailgateway@zulip.com'}
CONTRIBUTORS_DATA = os.path.join(STATIC_ROOT,
    'generated/github-contributors.json')
THUMBOR_KEY = get_secret('thumbor_key')
