import annotations
from __future__ import division, print_function, absolute_import
from numpy import sqrt, inner, zeros, inf, finfo
from numpy.linalg import norm
from .utils import make_system
__all__ = ['minres']


def minres(A, b, x0=None, shift=0.0, tol=1e-05, maxiter=None, M=None,
    callback=None, show=False, check=False):
    r2 = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_r2
    v = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_v
    beta1 = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_beta1
    gamma = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_gamma
    sqrt = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_sqrt
    beta = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_beta
    alfa = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_alfa
    inner = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_inner
    y = annotations.annot_scipy_sparse_linalg_isolve_minres_minres_y
    """
    Use MINimum RESidual iteration to solve Ax=b

    MINRES minimizes norm(A*x - b) for a real symmetric matrix A.  Unlike
    the Conjugate Gradient method, A can be indefinite or singular.

    If shift != 0 then the method solves (A - shift*I)x = b

    Parameters
    ----------
    A : {sparse matrix, dense matrix, LinearOperator}
        The real symmetric N-by-N matrix of the linear system
    b : {array, matrix}
        Right hand side of the linear system. Has shape (N,) or (N,1).

    Returns
    -------
    x : {array, matrix}
        The converged solution.
    info : integer
        Provides convergence information:
            0  : successful exit
            >0 : convergence to tolerance not achieved, number of iterations
            <0 : illegal input or breakdown

    Other Parameters
    ----------------
    x0  : {array, matrix}
        Starting guess for the solution.
    tol : float
        Tolerance to achieve. The algorithm terminates when the relative
        residual is below `tol`.
    maxiter : integer
        Maximum number of iterations.  Iteration will stop after maxiter
        steps even if the specified tolerance has not been achieved.
    M : {sparse matrix, dense matrix, LinearOperator}
        Preconditioner for A.  The preconditioner should approximate the
        inverse of A.  Effective preconditioning dramatically improves the
        rate of convergence, which implies that fewer iterations are needed
        to reach a given error tolerance.
    callback : function
        User-supplied function to call after each iteration.  It is called
        as callback(xk), where xk is the current solution vector.

    References
    ----------
    Solution of sparse indefinite systems of linear equations,
        C. C. Paige and M. A. Saunders (1975),
        SIAM J. Numer. Anal. 12(4), pp. 617-629.
        https://web.stanford.edu/group/SOL/software/minres/

    This file is a translation of the following MATLAB implementation:
        https://web.stanford.edu/group/SOL/software/minres/minres-matlab.zip

    """
    A, M, x, b, postprocess = make_system(A, M, x0, b)
    matvec = A.matvec
    psolve = M.matvec
    first = 'Enter minres.   '
    last = 'Exit  minres.   '
    n = A.shape[0]
    if maxiter is None:
        maxiter = 5 * n
    msg = [' beta2 = 0.  If M = I, b and x are eigenvectors    ',
        ' beta1 = 0.  The exact solution is  x = 0          ',
        ' A solution to Ax = b was found, given rtol        ',
        ' A least-squares solution was found, given rtol    ',
        ' Reasonable accuracy achieved, given eps           ',
        ' x has converged to an eigenvector                 ',
        ' acond has exceeded 0.1/eps                        ',
        ' The iteration limit was reached                   ',
        ' A  does not define a symmetric matrix             ',
        ' M  does not define a symmetric matrix             ',
        ' M  does not define a pos-def preconditioner       ']
    if show:
        print(first + 'Solution of symmetric Ax = b')
        print(first + 'n      =  %3g     shift  =  %23.14e' % (n, shift))
        print(first + 'itnlim =  %3g     rtol   =  %11.2e' % (maxiter, tol))
        print()
    istop = 0
    itn = 0
    Anorm = 0
    Acond = 0
    rnorm = 0
    ynorm = 0
    xtype = x.dtype
    eps = finfo(xtype).eps
    x = zeros(n, dtype=xtype)
    y = b
    r1 = b
    y = psolve(b)
    beta1 = inner(b, y)
    if beta1 < 0:
        raise ValueError('indefinite preconditioner')
    elif beta1 == 0:
        return postprocess(x), 0
    beta1 = sqrt(beta1)
    if check:
        w = matvec(y)
        r2 = matvec(w)
        s = inner(w, w)
        t = inner(y, r2)
        z = abs(s - t)
        epsa = (s + eps) * eps ** (1.0 / 3.0)
        if z > epsa:
            raise ValueError('non-symmetric matrix')
        r2 = psolve(y)
        s = inner(y, y)
        t = inner(r1, r2)
        z = abs(s - t)
        epsa = (s + eps) * eps ** (1.0 / 3.0)
        if z > epsa:
            raise ValueError('non-symmetric preconditioner')
    oldb = 0
    beta = beta1
    dbar = 0
    epsln = 0
    qrnorm = beta1
    phibar = beta1
    rhs1 = beta1
    rhs2 = 0
    tnorm2 = 0
    gmax = 0
    gmin = finfo(xtype).max
    cs = -1
    sn = 0
    w = zeros(n, dtype=xtype)
    w2 = zeros(n, dtype=xtype)
    r2 = r1
    if show:
        print()
        print()
        print(
            '   Itn     x(1)     Compatible    LS       norm(A)  cond(A) gbar/|A|'
            )
    while itn < maxiter:
        itn += 1
        s = 1.0 / beta
        v = s * y
        y = matvec(v)
        y = y - shift * v
        if itn >= 2:
            y = y - beta / oldb * r1
        alfa = inner(v, y)
        y = y - alfa / beta * r2
        r1 = r2
        r2 = y
        y = psolve(r2)
        oldb = beta
        beta = inner(r2, y)
        if beta < 0:
            raise ValueError('non-symmetric matrix')
        beta = sqrt(beta)
        tnorm2 += alfa ** 2 + oldb ** 2 + beta ** 2
        if itn == 1:
            if beta / beta1 <= 10 * eps:
                istop = -1
        oldeps = epsln
        delta = cs * dbar + sn * alfa
        gbar = sn * dbar - cs * alfa
        epsln = sn * beta
        dbar = -cs * beta
        root = norm([gbar, dbar])
        Arnorm = phibar * root
        gamma = norm([gbar, beta])
        gamma = max(gamma, eps)
        cs = gbar / gamma
        sn = beta / gamma
        phi = cs * phibar
        phibar = sn * phibar
        denom = 1.0 / gamma
        w1 = w2
        w2 = w
        w = (v - oldeps * w1 - delta * w2) * denom
        x = x + phi * w
        gmax = max(gmax, gamma)
        gmin = min(gmin, gamma)
        z = rhs1 / gamma
        rhs1 = rhs2 - delta * z
        rhs2 = -epsln * z
        Anorm = sqrt(tnorm2)
        ynorm = norm(x)
        epsa = Anorm * eps
        epsx = Anorm * ynorm * eps
        epsr = Anorm * ynorm * tol
        diag = gbar
        if diag == 0:
            diag = epsa
        qrnorm = phibar
        rnorm = qrnorm
        if ynorm == 0 or Anorm == 0:
            test1 = inf
        else:
            test1 = rnorm / (Anorm * ynorm)
        if Anorm == 0:
            test2 = inf
        else:
            test2 = root / Anorm
        Acond = gmax / gmin
        if istop == 0:
            t1 = 1 + test1
            t2 = 1 + test2
            if t2 <= 1:
                istop = 2
            if t1 <= 1:
                istop = 1
            if itn >= maxiter:
                istop = 6
            if Acond >= 0.1 / eps:
                istop = 4
            if epsx >= beta1:
                istop = 3
            if test2 <= tol:
                istop = 2
            if test1 <= tol:
                istop = 1
        prnt = False
        if n <= 40:
            prnt = True
        if itn <= 10:
            prnt = True
        if itn >= maxiter - 10:
            prnt = True
        if itn % 10 == 0:
            prnt = True
        if qrnorm <= 10 * epsx:
            prnt = True
        if qrnorm <= 10 * epsr:
            prnt = True
        if Acond <= 0.01 / eps:
            prnt = True
        if istop != 0:
            prnt = True
        if show and prnt:
            str1 = '%6g %12.5e %10.3e' % (itn, x[0], test1)
            str2 = ' %10.3e' % (test2,)
            str3 = ' %8.1e %8.1e %8.1e' % (Anorm, Acond, gbar / Anorm)
            print(str1 + str2 + str3)
            if itn % 10 == 0:
                print()
        if callback is not None:
            callback(x)
        if istop != 0:
            break
    if show:
        print()
        print(last + ' istop   =  %3g               itn   =%5g' % (istop, itn))
        print(last + ' Anorm   =  %12.4e      Acond =  %12.4e' % (Anorm, Acond)
            )
        print(last + ' rnorm   =  %12.4e      ynorm =  %12.4e' % (rnorm, ynorm)
            )
        print(last + ' Arnorm  =  %12.4e' % (Arnorm,))
        print(last + msg[istop + 1])
    if istop == 6:
        info = maxiter
    else:
        info = 0
    return postprocess(x), info


if __name__ == '__main__':
    from scipy import ones, arange
    from scipy.linalg import norm
    from scipy.sparse import spdiags
    n = 10
    residuals = []

    def cb(x):
        residuals.append(norm(b - A * x))
    A = spdiags([arange(1, n + 1, dtype=float)], [0], n, n, format='csr')
    M = spdiags([1.0 / arange(1, n + 1, dtype=float)], [0], n, n, format='csr')
    A.psolve = M.matvec
    b = 0 * ones(A.shape[0])
    x = minres(A, b, tol=1e-12, maxiter=None, callback=cb)
