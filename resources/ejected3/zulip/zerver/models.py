import annotations
from typing import Any, DefaultDict, Dict, List, Set, Tuple, TypeVar, Union, Optional, Sequence, AbstractSet, Pattern, AnyStr, Callable, Iterable
from typing.re import Match
from django.db import models
from django.db.models.query import QuerySet, F
from django.db.models import Manager, CASCADE, Sum
from django.db.models.functions import Length
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, UserManager, PermissionsMixin
import django.contrib.auth
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator, MinLengthValidator, RegexValidator
from django.dispatch import receiver
from zerver.lib.cache import cache_with_key, flush_user_profile, flush_realm, user_profile_by_api_key_cache_key, active_non_guest_user_ids_cache_key, user_profile_by_id_cache_key, user_profile_by_email_cache_key, user_profile_cache_key, generic_bulk_cached_fetch, cache_set, flush_stream, display_recipient_cache_key, cache_delete, active_user_ids_cache_key, get_stream_cache_key, realm_user_dicts_cache_key, bot_dicts_in_realm_cache_key, realm_user_dict_fields, bot_dict_fields, flush_message, flush_submessage, bot_profile_cache_key
from zerver.lib.utils import make_safe_digest, generate_random_token
from django.db import transaction
from django.utils.timezone import now as timezone_now
from django.contrib.sessions.models import Session
from zerver.lib.timestamp import datetime_to_timestamp
from django.db.models.signals import pre_save, post_save, post_delete
from django.utils.translation import ugettext_lazy as _
from zerver.lib import cache
from zerver.lib.validator import check_int, check_float, check_short_string, check_long_string, validate_choice_field, check_date, check_url, check_list
from zerver.lib.name_restrictions import is_disposable_domain
from zerver.lib.types import Validator, ExtendedValidator, ProfileDataElement, ProfileData, FieldTypeData, FieldElement, RealmUserValidator
from django.utils.encoding import force_text
from bitfield import BitField
from bitfield.types import BitHandler
from collections import defaultdict, OrderedDict
from datetime import timedelta
import pylibmc
import re
import logging
import sre_constants
import time
import datetime
import sys
MAX_TOPIC_NAME_LENGTH = 60
MAX_MESSAGE_LENGTH = 10000
MAX_LANGUAGE_ID_LENGTH = 50
STREAM_NAMES = TypeVar('STREAM_NAMES', Sequence[str], AbstractSet[str])


def query_for_ids(query: QuerySet, user_ids: List[int], field: str) ->QuerySet:
    """
    This function optimizes searches of the form
    `user_profile_id in (1, 2, 3, 4)` by quickly
    building the where clauses.  Profiling shows significant
    speedups over the normal Django-based approach.

    Use this very carefully!  Also, the caller should
    guard against empty lists of user_ids.
    """
    assert user_ids
    value_list = ', '.join(str(int(user_id)) for user_id in user_ids)
    clause = '%s in (%s)' % (field, value_list)
    query = query.extra(where=[clause])
    return query


per_request_display_recipient_cache = {}


def get_display_recipient_by_id(recipient_id: int, recipient_type: int,
    recipient_type_id: Optional[int]) ->Union[str, List[Dict[str, Any]]]:
    """
    returns: an object describing the recipient (using a cache).
    If the type is a stream, the type_id must be an int; a string is returned.
    Otherwise, type_id may be None; an array of recipient dicts is returned.
    """
    if recipient_id not in per_request_display_recipient_cache:
        result = get_display_recipient_remote_cache(recipient_id,
            recipient_type, recipient_type_id)
        per_request_display_recipient_cache[recipient_id] = result
    return per_request_display_recipient_cache[recipient_id]


def get_display_recipient(recipient: 'Recipient') ->Union[str, List[Dict[
    str, Any]]]:
    return get_display_recipient_by_id(recipient.id, recipient.type,
        recipient.type_id)


def flush_per_request_caches() ->None:
    global per_request_display_recipient_cache
    per_request_display_recipient_cache = {}
    global per_request_realm_filters_cache
    per_request_realm_filters_cache = {}


DisplayRecipientCacheT = Union[str, List[Dict[str, Any]]]


@cache_with_key(lambda *args: display_recipient_cache_key(args[0]), timeout
    =3600 * 24 * 7)
def get_display_recipient_remote_cache(recipient_id: int, recipient_type:
    int, recipient_type_id: Optional[int]) ->DisplayRecipientCacheT:
    """
    returns: an appropriate object describing the recipient.  For a
    stream this will be the stream name as a string.  For a huddle or
    personal, it will be an array of dicts about each recipient.
    """
    if recipient_type == Recipient.STREAM:
        assert recipient_type_id is not None
        stream = Stream.objects.get(id=recipient_type_id)
        return stream.name
    user_profile_list = UserProfile.objects.filter(subscription__recipient_id
        =recipient_id).select_related().order_by('id')
    return [{'email': user_profile.email, 'full_name': user_profile.
        full_name, 'short_name': user_profile.short_name, 'id':
        user_profile.id, 'is_mirror_dummy': user_profile.is_mirror_dummy} for
        user_profile in user_profile_list]


def get_realm_emoji_cache_key(realm: 'Realm') ->str:
    return 'realm_emoji:%s' % (realm.id,)


def get_active_realm_emoji_cache_key(realm: 'Realm') ->str:
    return 'active_realm_emoji:%s' % (realm.id,)


class Realm(models.Model):
    models.BooleanField = (annotations.
        annot_zerver_models_Realm_models_BooleanField)
    MAX_REALM_NAME_LENGTH = 40
    MAX_REALM_SUBDOMAIN_LENGTH = 40
    MAX_VIDEO_CHAT_PROVIDER_LENGTH = 40
    MAX_GOOGLE_HANGOUTS_DOMAIN_LENGTH = 255
    INVITES_STANDARD_REALM_DAILY_MAX = 3000
    MESSAGE_VISIBILITY_LIMITED = 10000
    VIDEO_CHAT_PROVIDERS = ['Jitsi', 'Google Hangouts']
    AUTHENTICATION_FLAGS = ['Google', 'Email', 'GitHub', 'LDAP', 'Dev',
        'RemoteUser', 'AzureAD']
    SUBDOMAIN_FOR_ROOT_DOMAIN = ''
    name = models.CharField(max_length=MAX_REALM_NAME_LENGTH, null=True)
    description = models.TextField(default='')
    string_id = models.CharField(max_length=MAX_REALM_SUBDOMAIN_LENGTH,
        unique=True)
    date_created = models.DateTimeField(default=timezone_now)
    deactivated = models.BooleanField(default=False)
    emails_restricted_to_domains = models.BooleanField(default=False)
    invite_required = models.BooleanField(default=True)
    invite_by_admins_only = models.BooleanField(default=False)
    _max_invites = models.IntegerField(null=True, db_column='max_invites')
    disallow_disposable_email_addresses = models.BooleanField(default=True)
    authentication_methods = BitField(flags=AUTHENTICATION_FLAGS, default=2 **
        31 - 1)
    inline_image_preview = models.BooleanField(default=True)
    inline_url_embed_preview = models.BooleanField(default=True)
    digest_emails_enabled = models.BooleanField(default=True)
    send_welcome_emails = models.BooleanField(default=True)
    mandatory_topics = models.BooleanField(default=False)
    create_stream_by_admins_only = models.BooleanField(default=False)
    add_emoji_by_admins_only = models.BooleanField(default=False)
    name_changes_disabled = models.BooleanField(default=False)
    email_changes_disabled = models.BooleanField(default=False)
    EMAIL_ADDRESS_VISIBILITY_EVERYONE = 1
    EMAIL_ADDRESS_VISIBILITY_MEMBERS = 2
    EMAIL_ADDRESS_VISIBILITY_ADMINS = 3
    email_address_visibility = models.PositiveSmallIntegerField(default=
        EMAIL_ADDRESS_VISIBILITY_EVERYONE)
    EMAIL_ADDRESS_VISIBILITY_TYPES = [EMAIL_ADDRESS_VISIBILITY_EVERYONE,
        EMAIL_ADDRESS_VISIBILITY_ADMINS]
    waiting_period_threshold = models.PositiveIntegerField(default=0)
    allow_message_deleting = models.BooleanField(default=False)
    DEFAULT_MESSAGE_CONTENT_DELETE_LIMIT_SECONDS = 600
    message_content_delete_limit_seconds = models.IntegerField(default=
        DEFAULT_MESSAGE_CONTENT_DELETE_LIMIT_SECONDS)
    allow_message_editing = models.BooleanField(default=True)
    DEFAULT_MESSAGE_CONTENT_EDIT_LIMIT_SECONDS = 600
    message_content_edit_limit_seconds = models.IntegerField(default=
        DEFAULT_MESSAGE_CONTENT_EDIT_LIMIT_SECONDS)
    allow_edit_history = models.BooleanField(default=True)
    DEFAULT_COMMUNITY_TOPIC_EDITING_LIMIT_SECONDS = 86400
    allow_community_topic_editing = models.BooleanField(default=True)
    default_twenty_four_hour_time = models.BooleanField(default=False)
    default_language = models.CharField(default='en', max_length=
        MAX_LANGUAGE_ID_LENGTH)
    DEFAULT_NOTIFICATION_STREAM_NAME = 'announce'
    INITIAL_PRIVATE_STREAM_NAME = 'core team'
    notifications_stream = models.ForeignKey('Stream', related_name='+',
        null=True, blank=True, on_delete=CASCADE)
    signup_notifications_stream = models.ForeignKey('Stream', related_name=
        '+', null=True, blank=True, on_delete=CASCADE)
    message_retention_days = models.IntegerField(null=True)
    message_visibility_limit = models.IntegerField(null=True)
    first_visible_message_id = models.IntegerField(default=0)
    CORPORATE = 1
    COMMUNITY = 2
    org_type = models.PositiveSmallIntegerField(default=CORPORATE)
    SELF_HOSTED = 1
    LIMITED = 2
    STANDARD = 3
    STANDARD_FREE = 4
    plan_type = models.PositiveSmallIntegerField(default=SELF_HOSTED)
    BOT_CREATION_EVERYONE = 1
    BOT_CREATION_LIMIT_GENERIC_BOTS = 2
    BOT_CREATION_ADMINS_ONLY = 3
    bot_creation_policy = models.PositiveSmallIntegerField(default=
        BOT_CREATION_EVERYONE)
    upload_quota_gb = models.IntegerField(null=True)
    video_chat_provider = models.CharField(default='Jitsi', max_length=
        MAX_VIDEO_CHAT_PROVIDER_LENGTH)
    google_hangouts_domain = models.TextField(default='')
    property_types = dict(add_emoji_by_admins_only=bool, allow_edit_history
        =bool, allow_message_deleting=bool, bot_creation_policy=int,
        create_stream_by_admins_only=bool, default_language=str,
        default_twenty_four_hour_time=bool, description=str,
        disallow_disposable_email_addresses=bool, email_address_visibility=
        int, email_changes_disabled=bool, google_hangouts_domain=str,
        invite_required=bool, invite_by_admins_only=bool,
        inline_image_preview=bool, inline_url_embed_preview=bool,
        mandatory_topics=bool, message_retention_days=(int, type(None)),
        name=str, name_changes_disabled=bool, emails_restricted_to_domains=
        bool, send_welcome_emails=bool, video_chat_provider=str,
        waiting_period_threshold=int)
    ICON_FROM_GRAVATAR = 'G'
    ICON_UPLOADED = 'U'
    ICON_SOURCES = (ICON_FROM_GRAVATAR, 'Hosted by Gravatar'), (ICON_UPLOADED,
        'Uploaded by administrator')
    icon_source = models.CharField(default=ICON_FROM_GRAVATAR, choices=
        ICON_SOURCES, max_length=1)
    icon_version = models.PositiveSmallIntegerField(default=1)
    LOGO_DEFAULT = 'D'
    LOGO_UPLOADED = 'U'
    LOGO_SOURCES = (LOGO_DEFAULT, 'Default to Zulip'), (LOGO_UPLOADED,
        'Uploaded by administrator')
    logo_source = models.CharField(default=LOGO_DEFAULT, choices=
        LOGO_SOURCES, max_length=1)
    logo_version = models.PositiveSmallIntegerField(default=1)
    BOT_CREATION_POLICY_TYPES = [BOT_CREATION_EVERYONE,
        BOT_CREATION_LIMIT_GENERIC_BOTS, BOT_CREATION_ADMINS_ONLY]
    has_seat_based_plan = models.BooleanField(default=False)
    seat_limit = models.PositiveIntegerField(null=True)

    def authentication_methods_dict(self) ->Dict[str, bool]:
        """Returns the a mapping from authentication flags to their status,
        showing only those authentication flags that are supported on
        the current server (i.e. if EmailAuthBackend is not configured
        on the server, this will not return an entry for "Email")."""
        from zproject.backends import AUTH_BACKEND_NAME_MAP
        ret = {}
        supported_backends = {backend.__class__ for backend in django.
            contrib.auth.get_backends()}
        for k, v in self.authentication_methods.iteritems():
            backend = AUTH_BACKEND_NAME_MAP[k]
            if backend in supported_backends:
                ret[k] = v
        return ret

    def __str__(self) ->str:
        return '<Realm: %s %s>' % (self.string_id, self.id)

    @cache_with_key(get_realm_emoji_cache_key, timeout=3600 * 24 * 7)
    def get_emoji(self) ->Dict[str, Dict[str, Iterable[str]]]:
        return get_realm_emoji_uncached(self)

    @cache_with_key(get_active_realm_emoji_cache_key, timeout=3600 * 24 * 7)
    def get_active_emoji(self) ->Dict[str, Dict[str, Iterable[str]]]:
        return get_active_realm_emoji_uncached(self)

    def get_admin_users(self) ->Sequence['UserProfile']:
        return UserProfile.objects.filter(realm=self, is_realm_admin=True,
            is_active=True)

    def get_active_users(self) ->Sequence['UserProfile']:
        return UserProfile.objects.filter(realm=self, is_active=True
            ).select_related()

    def get_bot_domain(self) ->str:
        return self.host.split(':')[0]

    def get_notifications_stream(self) ->Optional['Stream']:
        if (self.notifications_stream is not None and not self.
            notifications_stream.deactivated):
            return self.notifications_stream
        return None

    def get_signup_notifications_stream(self) ->Optional['Stream']:
        if (self.signup_notifications_stream is not None and not self.
            signup_notifications_stream.deactivated):
            return self.signup_notifications_stream
        return None

    @property
    def max_invites(self) ->int:
        if self._max_invites is None:
            return settings.INVITES_DEFAULT_REALM_DAILY_MAX
        return self._max_invites

    @max_invites.setter
    def max_invites(self, value: int) ->None:
        self._max_invites = value

    def upload_quota_bytes(self) ->Optional[int]:
        if self.upload_quota_gb is None:
            return None
        return self.upload_quota_gb << 30

    @property
    def subdomain(self) ->str:
        return self.string_id

    @property
    def display_subdomain(self) ->str:
        """Likely to be temporary function to avoid signup messages being sent
        to an empty topic"""
        if self.string_id == '':
            return '.'
        return self.string_id

    @property
    def uri(self) ->str:
        return settings.EXTERNAL_URI_SCHEME + self.host

    @property
    def host(self) ->str:
        return self.host_for_subdomain(self.subdomain)

    @staticmethod
    def host_for_subdomain(subdomain: str) ->str:
        if subdomain == Realm.SUBDOMAIN_FOR_ROOT_DOMAIN:
            return settings.EXTERNAL_HOST
        default_host = '%s.%s' % (subdomain, settings.EXTERNAL_HOST)
        return settings.REALM_HOSTS.get(subdomain, default_host)

    @property
    def is_zephyr_mirror_realm(self) ->bool:
        return self.string_id == 'zephyr'

    @property
    def webathena_enabled(self) ->bool:
        return self.is_zephyr_mirror_realm

    @property
    def presence_disabled(self) ->bool:
        return self.is_zephyr_mirror_realm


    class Meta:
        permissions = ('administer', 'Administer a realm'), ('api_super_user',
            'Can send messages as other users for mirroring')


post_save.connect(flush_realm, sender=Realm)


def get_realm(string_id: str) ->Realm:
    return Realm.objects.filter(string_id=string_id).first()


def name_changes_disabled(realm: Optional[Realm]) ->bool:
    if realm is None:
        return settings.NAME_CHANGES_DISABLED
    return settings.NAME_CHANGES_DISABLED or realm.name_changes_disabled


class RealmDomain(models.Model):
    """For an organization with emails_restricted_to_domains enabled, the list of
    allowed domains"""
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    domain = models.CharField(max_length=80, db_index=True)
    allow_subdomains = models.BooleanField(default=False)


    class Meta:
        unique_together = 'realm', 'domain'


def email_to_username(email: str) ->str:
    return '@'.join(email.split('@')[:-1]).lower()


def email_to_domain(email: str) ->str:
    return email.split('@')[-1].lower()


class DomainNotAllowedForRealmError(Exception):
    pass


class DisposableEmailError(Exception):
    pass


class EmailContainsPlusError(Exception):
    pass


def email_allowed_for_realm(email: str, realm: Realm) ->None:
    if not realm.emails_restricted_to_domains:
        if realm.disallow_disposable_email_addresses and is_disposable_domain(
            email_to_domain(email)):
            raise DisposableEmailError
        return
    elif '+' in email_to_username(email):
        raise EmailContainsPlusError
    domain = email_to_domain(email)
    query = RealmDomain.objects.filter(realm=realm)
    if query.filter(domain=domain).exists():
        return
    else:
        query = query.filter(allow_subdomains=True)
        while len(domain) > 0:
            subdomain, sep, domain = domain.partition('.')
            if query.filter(domain=domain).exists():
                return
    raise DomainNotAllowedForRealmError


def get_realm_domains(realm: Realm) ->List[Dict[str, str]]:
    return list(realm.realmdomain_set.values('domain', 'allow_subdomains'))


class RealmEmoji(models.Model):
    author = models.ForeignKey('UserProfile', blank=True, null=True,
        on_delete=CASCADE)
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    name = models.TextField(validators=[MinLengthValidator(1),
        RegexValidator(regex='^[0-9a-z.\\-_]+(?<![.\\-_])$', message=_(
        'Invalid characters in emoji name'))])
    file_name = models.TextField(db_index=True, null=True, blank=True)
    deactivated = models.BooleanField(default=False)
    PATH_ID_TEMPLATE = '{realm_id}/emoji/images/{emoji_file_name}'

    def __str__(self) ->str:
        return '<RealmEmoji(%s): %s %s %s %s>' % (self.realm.string_id,
            self.id, self.name, self.deactivated, self.file_name)


def get_realm_emoji_dicts(realm: Realm, only_active_emojis: bool=False) ->Dict[
    str, Dict[str, Any]]:
    query = RealmEmoji.objects.filter(realm=realm).select_related('author')
    if only_active_emojis:
        query = query.filter(deactivated=False)
    d = {}
    from zerver.lib.emoji import get_emoji_url
    for realm_emoji in query.all():
        author = None
        if realm_emoji.author:
            author = {'id': realm_emoji.author.id, 'email': realm_emoji.
                author.email, 'full_name': realm_emoji.author.full_name}
        emoji_url = get_emoji_url(realm_emoji.file_name, realm_emoji.realm_id)
        d[str(realm_emoji.id)] = dict(id=str(realm_emoji.id), name=
            realm_emoji.name, source_url=emoji_url, deactivated=realm_emoji
            .deactivated, author=author)
    return d


def get_realm_emoji_uncached(realm: Realm) ->Dict[str, Dict[str, Any]]:
    return get_realm_emoji_dicts(realm)


def get_active_realm_emoji_uncached(realm: Realm) ->Dict[str, Dict[str, Any]]:
    realm_emojis = get_realm_emoji_dicts(realm, only_active_emojis=True)
    d = {}
    for emoji_id, emoji_dict in realm_emojis.items():
        d[emoji_dict['name']] = emoji_dict
    return d


def flush_realm_emoji(sender: Any, **kwargs: Any) ->None:
    realm = kwargs['instance'].realm
    cache_set(get_realm_emoji_cache_key(realm), get_realm_emoji_uncached(
        realm), timeout=3600 * 24 * 7)
    cache_set(get_active_realm_emoji_cache_key(realm),
        get_active_realm_emoji_uncached(realm), timeout=3600 * 24 * 7)


post_save.connect(flush_realm_emoji, sender=RealmEmoji)
post_delete.connect(flush_realm_emoji, sender=RealmEmoji)


def filter_pattern_validator(value: str) ->None:
    regex = re.compile('^(?:(?:[\\w\\-#_= /:]*|[+]|[!])(\\(\\?P<\\w+>.+\\)))+$'
        )
    error_msg = _('Invalid filter pattern.  Valid characters are %s.' % (
        '[ a-zA-Z_#=/:+!-]',))
    if not regex.match(str(value)):
        raise ValidationError(error_msg)
    try:
        re.compile(value)
    except sre_constants.error:
        raise ValidationError(error_msg)


def filter_format_validator(value: str) ->None:
    regex = re.compile(
        '^([\\.\\/:a-zA-Z0-9#_?=-]+%\\(([a-zA-Z0-9_-]+)\\)s)+[a-zA-Z0-9_-]*$')
    if not regex.match(value):
        raise ValidationError(_('Invalid URL format string.'))


class RealmFilter(models.Model):
    """Realm-specific regular expressions to automatically linkify certain
    strings inside the markdown processor.  See "Custom filters" in the settings UI.
    """
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    pattern = models.TextField(validators=[filter_pattern_validator])
    url_format_string = models.TextField(validators=[URLValidator(),
        filter_format_validator])


    class Meta:
        unique_together = 'realm', 'pattern'

    def __str__(self) ->str:
        return '<RealmFilter(%s): %s %s>' % (self.realm.string_id, self.
            pattern, self.url_format_string)


def get_realm_filters_cache_key(realm_id: int) ->str:
    return '%s:all_realm_filters:%s' % (cache.KEY_PREFIX, realm_id)


per_request_realm_filters_cache = {}


def realm_in_local_realm_filters_cache(realm_id: int) ->bool:
    return realm_id in per_request_realm_filters_cache


def realm_filters_for_realm(realm_id: int) ->List[Tuple[str, str, int]]:
    if not realm_in_local_realm_filters_cache(realm_id):
        per_request_realm_filters_cache[realm_id
            ] = realm_filters_for_realm_remote_cache(realm_id)
    return per_request_realm_filters_cache[realm_id]


@cache_with_key(get_realm_filters_cache_key, timeout=3600 * 24 * 7)
def realm_filters_for_realm_remote_cache(realm_id: int) ->List[Tuple[str,
    str, int]]:
    filters = []
    for realm_filter in RealmFilter.objects.filter(realm_id=realm_id):
        filters.append((realm_filter.pattern, realm_filter.
            url_format_string, realm_filter.id))
    return filters


def all_realm_filters() ->Dict[int, List[Tuple[str, str, int]]]:
    filters = defaultdict(list)
    for realm_filter in RealmFilter.objects.all():
        filters[realm_filter.realm_id].append((realm_filter.pattern,
            realm_filter.url_format_string, realm_filter.id))
    return filters


def flush_realm_filter(sender: Any, **kwargs: Any) ->None:
    realm_id = kwargs['instance'].realm_id
    cache_delete(get_realm_filters_cache_key(realm_id))
    try:
        per_request_realm_filters_cache.pop(realm_id)
    except KeyError:
        pass


post_save.connect(flush_realm_filter, sender=RealmFilter)
post_delete.connect(flush_realm_filter, sender=RealmFilter)


class UserProfile(AbstractBaseUser, PermissionsMixin):
    models.BooleanField = (annotations.
        annot_zerver_models_UserProfile_models_BooleanField)
    USERNAME_FIELD = 'email'
    MAX_NAME_LENGTH = 100
    MIN_NAME_LENGTH = 2
    API_KEY_LENGTH = 32
    NAME_INVALID_CHARS = ['*', '`', '>', '"', '@']
    DEFAULT_BOT = 1
    """
    Incoming webhook bots are limited to only sending messages via webhooks.
    Thus, it is less of a security risk to expose their API keys to third-party services,
    since they can't be used to read messages.
    """
    INCOMING_WEBHOOK_BOT = 2
    OUTGOING_WEBHOOK_BOT = 3
    """
    Embedded bots run within the Zulip server itself; events are added to the
    embedded_bots queue and then handled by a QueueProcessingWorker.
    """
    EMBEDDED_BOT = 4
    BOT_TYPES = {DEFAULT_BOT: 'Generic bot', INCOMING_WEBHOOK_BOT:
        'Incoming webhook', OUTGOING_WEBHOOK_BOT: 'Outgoing webhook',
        EMBEDDED_BOT: 'Embedded bot'}
    SERVICE_BOT_TYPES = [OUTGOING_WEBHOOK_BOT, EMBEDDED_BOT]
    email = models.EmailField(blank=False, db_index=True)
    delivery_email = models.EmailField(blank=False, db_index=True)
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    full_name = models.CharField(max_length=MAX_NAME_LENGTH)
    short_name = models.CharField(max_length=MAX_NAME_LENGTH)
    date_joined = models.DateTimeField(default=timezone_now)
    tos_version = models.CharField(null=True, max_length=10)
    api_key = models.CharField(max_length=API_KEY_LENGTH)
    pointer = models.IntegerField()
    last_pointer_updater = models.CharField(max_length=64)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True, db_index=True)
    is_realm_admin = models.BooleanField(default=False, db_index=True)
    is_billing_admin = models.BooleanField(default=False, db_index=True)
    is_guest = models.BooleanField(default=False, db_index=True)
    is_bot = models.BooleanField(default=False, db_index=True)
    bot_type = models.PositiveSmallIntegerField(null=True, db_index=True)
    bot_owner = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    long_term_idle = models.BooleanField(default=False, db_index=True)
    last_active_message_id = models.IntegerField(null=True)
    is_mirror_dummy = models.BooleanField(default=False)
    is_api_super_user = models.BooleanField(default=False, db_index=True)
    enable_stream_desktop_notifications = models.BooleanField(default=False)
    enable_stream_email_notifications = models.BooleanField(default=False)
    enable_stream_push_notifications = models.BooleanField(default=False)
    enable_stream_sounds = models.BooleanField(default=False)
    notification_sound = models.CharField(max_length=20, default='zulip')
    enable_desktop_notifications = models.BooleanField(default=True)
    pm_content_in_desktop_notifications = models.BooleanField(default=True)
    enable_sounds = models.BooleanField(default=True)
    enable_offline_email_notifications = models.BooleanField(default=True)
    message_content_in_email_notifications = models.BooleanField(default=True)
    enable_offline_push_notifications = models.BooleanField(default=True)
    enable_online_push_notifications = models.BooleanField(default=False)
    enable_digest_emails = models.BooleanField(default=True)
    enable_login_emails = models.BooleanField(default=True)
    realm_name_in_notifications = models.BooleanField(default=False)
    alert_words = models.TextField(default='[]')
    last_reminder = models.DateTimeField(default=None, null=True)
    BOT_OWNER_STREAM_ALERT_WAITPERIOD = 1
    rate_limits = models.CharField(default='', max_length=100)
    EMAIL_REMINDER_WAITPERIOD = 24
    default_sending_stream = models.ForeignKey('zerver.Stream', null=True,
        related_name='+', on_delete=CASCADE)
    default_events_register_stream = models.ForeignKey('zerver.Stream',
        null=True, related_name='+', on_delete=CASCADE)
    default_all_public_streams = models.BooleanField(default=False)
    enter_sends = models.NullBooleanField(default=False)
    left_side_userlist = models.BooleanField(default=False)
    twenty_four_hour_time = models.BooleanField(default=False)
    default_language = models.CharField(default='en', max_length=
        MAX_LANGUAGE_ID_LENGTH)
    high_contrast_mode = models.BooleanField(default=False)
    night_mode = models.BooleanField(default=False)
    translate_emoticons = models.BooleanField(default=False)
    dense_mode = models.BooleanField(default=True)
    starred_message_counts = models.BooleanField(default=False)
    timezone = models.CharField(max_length=40, default='')
    GOOGLE_EMOJISET = 'google'
    GOOGLE_BLOB_EMOJISET = 'google-blob'
    TEXT_EMOJISET = 'text'
    TWITTER_EMOJISET = 'twitter'
    EMOJISET_CHOICES = (GOOGLE_EMOJISET, 'Google modern'), (
        GOOGLE_BLOB_EMOJISET, 'Google classic'), (TWITTER_EMOJISET, 'Twitter'
        ), (TEXT_EMOJISET, 'Plain text')
    emojiset = models.CharField(default=GOOGLE_BLOB_EMOJISET, choices=
        EMOJISET_CHOICES, max_length=20)
    AVATAR_FROM_GRAVATAR = 'G'
    AVATAR_FROM_USER = 'U'
    AVATAR_SOURCES = (AVATAR_FROM_GRAVATAR, 'Hosted by Gravatar'), (
        AVATAR_FROM_USER, 'Uploaded by user')
    avatar_source = models.CharField(default=AVATAR_FROM_GRAVATAR, choices=
        AVATAR_SOURCES, max_length=1)
    avatar_version = models.PositiveSmallIntegerField(default=1)
    TUTORIAL_WAITING = 'W'
    TUTORIAL_STARTED = 'S'
    TUTORIAL_FINISHED = 'F'
    TUTORIAL_STATES = (TUTORIAL_WAITING, 'Waiting'), (TUTORIAL_STARTED,
        'Started'), (TUTORIAL_FINISHED, 'Finished')
    tutorial_status = models.CharField(default=TUTORIAL_WAITING, choices=
        TUTORIAL_STATES, max_length=1)
    onboarding_steps = models.TextField(default='[]')
    objects = UserManager()
    property_types = dict(default_language=str, dense_mode=bool, emojiset=
        str, left_side_userlist=bool, timezone=str, twenty_four_hour_time=
        bool, high_contrast_mode=bool, night_mode=bool, translate_emoticons
        =bool, starred_message_counts=bool)
    notification_setting_types = dict(enable_desktop_notifications=bool,
        enable_digest_emails=bool, enable_login_emails=bool,
        enable_offline_email_notifications=bool,
        enable_offline_push_notifications=bool,
        enable_online_push_notifications=bool, enable_sounds=bool,
        enable_stream_desktop_notifications=bool,
        enable_stream_email_notifications=bool,
        enable_stream_push_notifications=bool, enable_stream_sounds=bool,
        message_content_in_email_notifications=bool, notification_sound=str,
        pm_content_in_desktop_notifications=bool,
        realm_name_in_notifications=bool)


    class Meta:
        unique_together = ('realm', 'email'),

    @property
    def profile_data(self) ->ProfileData:
        values = CustomProfileFieldValue.objects.filter(user_profile=self)
        user_data = {v.field_id: {'value': v.value, 'rendered_value': v.
            rendered_value} for v in values}
        data = []
        for field in custom_profile_fields_for_realm(self.realm_id):
            field_values = user_data.get(field.id, None)
            if field_values:
                value, rendered_value = field_values.get('value'
                    ), field_values.get('rendered_value')
            else:
                value, rendered_value = None, None
            field_type = field.field_type
            if value is not None:
                converter = field.FIELD_CONVERTERS[field_type]
                value = converter(value)
            field_data = {}
            for k, v in field.as_dict().items():
                field_data[k] = v
            field_data['value'] = value
            field_data['rendered_value'] = rendered_value
            data.append(field_data)
        return data

    def can_admin_user(self, target_user: 'UserProfile') ->bool:
        """Returns whether this user has permission to modify target_user"""
        if target_user.bot_owner == self:
            return True
        elif self.is_realm_admin and self.realm == target_user.realm:
            return True
        else:
            return False

    def __str__(self) ->str:
        return '<UserProfile: %s %s>' % (self.email, self.realm)

    @property
    def is_incoming_webhook(self) ->bool:
        return self.bot_type == UserProfile.INCOMING_WEBHOOK_BOT

    @property
    def allowed_bot_types(self) ->List[int]:
        allowed_bot_types = []
        if (self.is_realm_admin or not self.realm.bot_creation_policy ==
            Realm.BOT_CREATION_LIMIT_GENERIC_BOTS):
            allowed_bot_types.append(UserProfile.DEFAULT_BOT)
        allowed_bot_types += [UserProfile.INCOMING_WEBHOOK_BOT, UserProfile
            .OUTGOING_WEBHOOK_BOT]
        if settings.EMBEDDED_BOTS_ENABLED:
            allowed_bot_types.append(UserProfile.EMBEDDED_BOT)
        return allowed_bot_types

    @staticmethod
    def emojiset_choices() ->Dict[str, str]:
        return OrderedDict((emojiset[0], emojiset[1]) for emojiset in
            UserProfile.EMOJISET_CHOICES)

    @staticmethod
    def emails_from_ids(user_ids: Sequence[int]) ->Dict[int, str]:
        rows = UserProfile.objects.filter(id__in=user_ids).values('id', 'email'
            )
        return {row['id']: row['email'] for row in rows}

    def can_create_streams(self) ->bool:
        if self.is_realm_admin:
            return True
        if self.realm.create_stream_by_admins_only:
            return False
        if self.is_guest:
            return False
        diff = (timezone_now() - self.date_joined).days
        if diff >= self.realm.waiting_period_threshold:
            return True
        return False

    def can_subscribe_other_users(self) ->bool:
        if self.is_realm_admin:
            return True
        if self.is_guest:
            return False
        diff = (timezone_now() - self.date_joined).days
        if diff >= self.realm.waiting_period_threshold:
            return True
        return False

    def can_access_public_streams(self) ->bool:
        return not (self.is_guest or self.realm.is_zephyr_mirror_realm)

    def can_access_all_realm_members(self) ->bool:
        return not (self.realm.is_zephyr_mirror_realm or self.is_guest)

    def major_tos_version(self) ->int:
        if self.tos_version is not None:
            return int(self.tos_version.split('.')[0])
        else:
            return -1


class UserGroup(models.Model):
    name = models.CharField(max_length=100)
    members = models.ManyToManyField(UserProfile, through='UserGroupMembership'
        )
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    description = models.TextField(default='')


    class Meta:
        unique_together = ('realm', 'name'),


class UserGroupMembership(models.Model):
    user_group = models.ForeignKey(UserGroup, on_delete=CASCADE)
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)


    class Meta:
        unique_together = ('user_group', 'user_profile'),


def receives_offline_push_notifications(user_profile: UserProfile) ->bool:
    return (user_profile.enable_offline_push_notifications and not
        user_profile.is_bot)


def receives_offline_email_notifications(user_profile: UserProfile) ->bool:
    return (user_profile.enable_offline_email_notifications and not
        user_profile.is_bot)


def receives_online_notifications(user_profile: UserProfile) ->bool:
    return (user_profile.enable_online_push_notifications and not
        user_profile.is_bot)


def receives_stream_notifications(user_profile: UserProfile) ->bool:
    return (user_profile.enable_stream_push_notifications and not
        user_profile.is_bot)


def remote_user_to_email(remote_user: str) ->str:
    if settings.SSO_APPEND_DOMAIN is not None:
        remote_user += '@' + settings.SSO_APPEND_DOMAIN
    return remote_user


post_save.connect(flush_user_profile, sender=UserProfile)


class PreregistrationUser(models.Model):
    email = models.EmailField()
    referred_by = models.ForeignKey(UserProfile, null=True, on_delete=CASCADE)
    streams = models.ManyToManyField('Stream')
    invited_at = models.DateTimeField(auto_now=True)
    realm_creation = models.BooleanField(default=False)
    password_required = models.BooleanField(default=True)
    status = models.IntegerField(default=0)
    realm = models.ForeignKey(Realm, null=True, on_delete=CASCADE)
    INVITE_AS = dict(NORMAL_USER=1, REALM_ADMIN=2, GUEST_USER=3)
    invited_as = models.PositiveSmallIntegerField(default=INVITE_AS[
        'NORMAL_USER'])
    invited_as_admin = models.BooleanField(default=False)


class MultiuseInvite(models.Model):
    referred_by = models.ForeignKey(UserProfile, on_delete=CASCADE)
    streams = models.ManyToManyField('Stream')
    realm = models.ForeignKey(Realm, on_delete=CASCADE)


class EmailChangeStatus(models.Model):
    new_email = models.EmailField()
    old_email = models.EmailField()
    updated_at = models.DateTimeField(auto_now=True)
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    status = models.IntegerField(default=0)
    realm = models.ForeignKey(Realm, on_delete=CASCADE)


class AbstractPushDeviceToken(models.Model):
    APNS = 1
    GCM = 2
    KINDS = (APNS, 'apns'), (GCM, 'gcm')
    kind = models.PositiveSmallIntegerField(choices=KINDS)
    token = models.CharField(max_length=4096, db_index=True)
    last_updated = models.DateTimeField(auto_now=True)
    ios_app_id = models.TextField(null=True)


    class Meta:
        abstract = True


class PushDeviceToken(AbstractPushDeviceToken):
    user = models.ForeignKey(UserProfile, db_index=True, on_delete=CASCADE)


    class Meta:
        unique_together = 'user', 'kind', 'token'


def generate_email_token_for_stream() ->str:
    return generate_random_token(32)


class Stream(models.Model):
    MAX_NAME_LENGTH = 60
    MAX_DESCRIPTION_LENGTH = 1024
    name = models.CharField(max_length=MAX_NAME_LENGTH, db_index=True)
    realm = models.ForeignKey(Realm, db_index=True, on_delete=CASCADE)
    date_created = models.DateTimeField(default=timezone_now)
    deactivated = models.BooleanField(default=False)
    description = models.CharField(max_length=MAX_DESCRIPTION_LENGTH,
        default='')
    invite_only = models.NullBooleanField(default=False)
    history_public_to_subscribers = models.BooleanField(default=False)
    is_web_public = models.BooleanField(default=False)
    is_announcement_only = models.BooleanField(default=False)
    is_in_zephyr_realm = models.BooleanField(default=False)
    email_token = models.CharField(max_length=32, default=
        generate_email_token_for_stream)

    def __str__(self) ->str:
        return '<Stream: %s>' % (self.name,)

    def is_public(self) ->bool:
        return not self.invite_only and not self.is_in_zephyr_realm

    def is_history_realm_public(self) ->bool:
        return self.is_public()

    def is_history_public_to_subscribers(self) ->bool:
        return self.history_public_to_subscribers


    class Meta:
        unique_together = 'name', 'realm'

    def to_dict(self) ->Dict[str, Any]:
        return dict(name=self.name, stream_id=self.id, description=self.
            description, invite_only=self.invite_only, is_announcement_only
            =self.is_announcement_only, history_public_to_subscribers=self.
            history_public_to_subscribers)


post_save.connect(flush_stream, sender=Stream)
post_delete.connect(flush_stream, sender=Stream)


class Recipient(models.Model):
    type_id = models.IntegerField(db_index=True)
    type = models.PositiveSmallIntegerField(db_index=True)
    PERSONAL = 1
    STREAM = 2
    HUDDLE = 3


    class Meta:
        unique_together = 'type', 'type_id'
    _type_names = {PERSONAL: 'personal', STREAM: 'stream', HUDDLE: 'huddle'}

    def type_name(self) ->str:
        return self._type_names[self.type]

    def __str__(self) ->str:
        display_recipient = get_display_recipient(self)
        return '<Recipient: %s (%d, %s)>' % (display_recipient, self.
            type_id, self.type)


class MutedTopic(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    stream = models.ForeignKey(Stream, on_delete=CASCADE)
    recipient = models.ForeignKey(Recipient, on_delete=CASCADE)
    topic_name = models.CharField(max_length=MAX_TOPIC_NAME_LENGTH)


    class Meta:
        unique_together = 'user_profile', 'stream', 'topic_name'

    def __str__(self) ->str:
        return '<MutedTopic: (%s, %s, %s)>' % (self.user_profile.email,
            self.stream.name, self.topic_name)


class Client(models.Model):
    name = models.CharField(max_length=30, db_index=True, unique=True)

    def __str__(self) ->str:
        return '<Client: %s>' % (self.name,)


get_client_cache = {}


def get_client(name: str) ->Client:
    cache_name = cache.KEY_PREFIX + name
    if cache_name not in get_client_cache:
        result = get_client_remote_cache(name)
        get_client_cache[cache_name] = result
    return get_client_cache[cache_name]


def get_client_cache_key(name: str) ->str:
    return 'get_client:%s' % (make_safe_digest(name),)


@cache_with_key(get_client_cache_key, timeout=3600 * 24 * 7)
def get_client_remote_cache(name: str) ->Client:
    client, _ = Client.objects.get_or_create(name=name)
    return client


@cache_with_key(get_stream_cache_key, timeout=3600 * 24 * 7)
def get_realm_stream(stream_name: str, realm_id: int) ->Stream:
    return Stream.objects.select_related('realm').get(name__iexact=
        stream_name.strip(), realm_id=realm_id)


def stream_name_in_use(stream_name: str, realm_id: int) ->bool:
    return Stream.objects.filter(name__iexact=stream_name.strip(), realm_id
        =realm_id).exists()


def get_active_streams(realm: Optional[Realm]) ->QuerySet:
    """
    Return all streams (including invite-only streams) that have not been deactivated.
    """
    return Stream.objects.filter(realm=realm, deactivated=False)


def get_stream(stream_name: str, realm: Realm) ->Stream:
    """
    Callers that don't have a Realm object already available should use
    get_realm_stream directly, to avoid unnecessarily fetching the
    Realm object.
    """
    return get_realm_stream(stream_name, realm.id)


def bulk_get_streams(realm: Realm, stream_names: STREAM_NAMES) ->Dict[str, Any
    ]:

    def fetch_streams_by_name(stream_names: List[str]) ->Sequence[Stream]:
        if len(stream_names) == 0:
            return []
        upper_list = ', '.join(['UPPER(%s)'] * len(stream_names))
        where_clause = 'UPPER(zerver_stream.name::text) IN (%s)' % (upper_list,
            )
        return get_active_streams(realm.id).select_related('realm').extra(where
            =[where_clause], params=stream_names)
    return generic_bulk_cached_fetch(lambda stream_name:
        get_stream_cache_key(stream_name, realm.id), fetch_streams_by_name,
        [stream_name.lower() for stream_name in stream_names], id_fetcher=
        lambda stream: stream.name.lower())


def get_recipient_cache_key(type: int, type_id: int) ->str:
    return '%s:get_recipient:%s:%s' % (cache.KEY_PREFIX, type, type_id)


@cache_with_key(get_recipient_cache_key, timeout=3600 * 24 * 7)
def get_recipient(type: int, type_id: int) ->Recipient:
    return Recipient.objects.get(type_id=type_id, type=type)


def get_stream_recipient(stream_id: int) ->Recipient:
    return get_recipient(Recipient.STREAM, stream_id)


def get_personal_recipient(user_profile_id: int) ->Recipient:
    return get_recipient(Recipient.PERSONAL, user_profile_id)


def get_huddle_recipient(user_profile_ids: Set[int]) ->Recipient:
    huddle = get_huddle(list(user_profile_ids))
    return get_recipient(Recipient.HUDDLE, huddle.id)


def get_huddle_user_ids(recipient: Recipient) ->List[int]:
    assert recipient.type == Recipient.HUDDLE
    return Subscription.objects.filter(recipient=recipient, active=True
        ).order_by('user_profile_id').values_list('user_profile_id', flat=True)


def bulk_get_recipients(type: int, type_ids: List[int]) ->Dict[int, Any]:

    def cache_key_function(type_id: int) ->str:
        return get_recipient_cache_key(type, type_id)

    def query_function(type_ids: List[int]) ->Sequence[Recipient]:
        return Recipient.objects.filter(type=type, type_id__in=type_ids)
    return generic_bulk_cached_fetch(cache_key_function, query_function,
        type_ids, id_fetcher=lambda recipient: recipient.type_id)


def get_stream_recipients(stream_ids: List[int]) ->List[Recipient]:
    """
    We could call bulk_get_recipients(...).values() here, but it actually
    leads to an extra query in test mode.
    """
    return Recipient.objects.filter(type=Recipient.STREAM, type_id__in=
        stream_ids)


class AbstractMessage(models.Model):
    sender = models.ForeignKey(UserProfile, on_delete=CASCADE)
    recipient = models.ForeignKey(Recipient, on_delete=CASCADE)
    subject = models.CharField(max_length=MAX_TOPIC_NAME_LENGTH, db_index=True)
    content = models.TextField()
    rendered_content = models.TextField(null=True)
    rendered_content_version = models.IntegerField(null=True)
    pub_date = models.DateTimeField('date published', db_index=True)
    sending_client = models.ForeignKey(Client, on_delete=CASCADE)
    last_edit_time = models.DateTimeField(null=True)
    edit_history = models.TextField(null=True)
    has_attachment = models.BooleanField(default=False, db_index=True)
    has_image = models.BooleanField(default=False, db_index=True)
    has_link = models.BooleanField(default=False, db_index=True)


    class Meta:
        abstract = True

    def __str__(self) ->str:
        display_recipient = get_display_recipient(self.recipient)
        return '<%s: %s / %s / %s>' % (self.__class__.__name__,
            display_recipient, self.subject, self.sender)


class ArchivedMessage(AbstractMessage):
    """Used as a temporary holding place for deleted messages before they
    are permanently deleted.  This is an important part of a robust
    'message retention' feature.
    """
    archive_timestamp = models.DateTimeField(default=timezone_now, db_index
        =True)


class Message(AbstractMessage):

    def topic_name(self) ->str:
        """
        Please start using this helper to facilitate an
        eventual switch over to a separate topic table.
        """
        return self.subject

    def set_topic_name(self, topic_name: str) ->None:
        self.subject = topic_name

    def is_stream_message(self) ->bool:
        """
        Find out whether a message is a stream message by
        looking up its recipient.type.  TODO: Make this
        an easier operation by denormalizing the message
        type onto Message, either explicity (message.type)
        or implicitly (message.stream_id is not None).
        """
        return self.recipient.type == Recipient.STREAM

    def get_realm(self) ->Realm:
        return self.sender.realm

    def save_rendered_content(self) ->None:
        self.save(update_fields=['rendered_content',
            'rendered_content_version'])

    @staticmethod
    def need_to_render_content(rendered_content: Optional[str],
        rendered_content_version: Optional[int], bugdown_version: int) ->bool:
        return (rendered_content is None or rendered_content_version is
            None or rendered_content_version < bugdown_version)

    def to_log_dict(self) ->Dict[str, Any]:
        return dict(id=self.id, sender_id=self.sender.id, sender_email=self
            .sender.email, sender_realm_str=self.sender.realm.string_id,
            sender_full_name=self.sender.full_name, sender_short_name=self.
            sender.short_name, sending_client=self.sending_client.name,
            type=self.recipient.type_name(), recipient=
            get_display_recipient(self.recipient), subject=self.topic_name(
            ), content=self.content, timestamp=datetime_to_timestamp(self.
            pub_date))

    def sent_by_human(self) ->bool:
        """Used to determine whether a message was sent by a full Zulip UI
        style client (and thus whether the message should be treated
        as sent by a human and automatically marked as read for the
        sender).  The purpose of this distinction is to ensure that
        message sent to the user by e.g. a Google Calendar integration
        using the user's own API key don't get marked as read
        automatically.
        """
        sending_client = self.sending_client.name.lower()
        return sending_client in ('zulipandroid', 'zulipios',
            'zulipdesktop', 'zulipmobile', 'zulipelectron', 'zulipterminal',
            'snipe', 'website', 'ios', 'android'
            ) or 'desktop app' in sending_client

    @staticmethod
    def content_has_attachment(content: str) ->Match:
        return re.search('[/\\-]user[\\-_]uploads[/\\.-]', content)

    @staticmethod
    def content_has_image(content: str) ->bool:
        return bool(re.search(
            '[/\\-]user[\\-_]uploads[/\\.-]\\S+\\.(bmp|gif|jpg|jpeg|png|webp)',
            content, re.IGNORECASE))

    @staticmethod
    def content_has_link(content: str) ->bool:
        return ('http://' in content or 'https://' in content or 
            '/user_uploads' in content or settings.ENABLE_FILE_LINKS and 
            'file:///' in content or 'bitcoin:' in content)

    @staticmethod
    def is_status_message(content: str, rendered_content: str) ->bool:
        """
        Returns True if content and rendered_content are from 'me_message'
        """
        if content.startswith('/me ') and '\n' not in content:
            if rendered_content.startswith('<p>'
                ) and rendered_content.endswith('</p>'):
                return True
        return False

    def update_calculated_fields(self) ->None:
        content = self.content
        self.has_attachment = bool(Message.content_has_attachment(content))
        self.has_image = bool(Message.content_has_image(content))
        self.has_link = bool(Message.content_has_link(content))


@receiver(pre_save, sender=Message)
def pre_save_message(sender: Any, **kwargs: Any) ->None:
    if kwargs['update_fields'] is None or 'content' in kwargs['update_fields']:
        message = kwargs['instance']
        message.update_calculated_fields()


def get_context_for_message(message: Message) ->Sequence[Message]:
    return Message.objects.filter(recipient_id=message.recipient_id,
        subject=message.subject, id__lt=message.id, pub_date__gt=message.
        pub_date - timedelta(minutes=15)).order_by('-id')[:10]


post_save.connect(flush_message, sender=Message)


class SubMessage(models.Model):
    message = models.ForeignKey(Message, on_delete=CASCADE)
    sender = models.ForeignKey(UserProfile, on_delete=CASCADE)
    msg_type = models.TextField()
    content = models.TextField()

    @staticmethod
    def get_raw_db_rows(needed_ids: List[int]) ->List[Dict[str, Any]]:
        fields = ['id', 'message_id', 'sender_id', 'msg_type', 'content']
        query = SubMessage.objects.filter(message_id__in=needed_ids).values(*
            fields)
        query = query.order_by('message_id', 'id')
        return list(query)


post_save.connect(flush_submessage, sender=SubMessage)


class Reaction(models.Model):
    """For emoji reactions to messages (and potentially future reaction types).

    Emoji are surprisingly complicated to implement correctly.  For details
    on how this subsystem works, see:
      https://zulip.readthedocs.io/en/latest/subsystems/emoji.html
    """
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    message = models.ForeignKey(Message, on_delete=CASCADE)
    emoji_name = models.TextField()
    UNICODE_EMOJI = 'unicode_emoji'
    REALM_EMOJI = 'realm_emoji'
    ZULIP_EXTRA_EMOJI = 'zulip_extra_emoji'
    REACTION_TYPES = (UNICODE_EMOJI, _('Unicode emoji')), (REALM_EMOJI, _(
        'Custom emoji')), (ZULIP_EXTRA_EMOJI, _('Zulip extra emoji'))
    reaction_type = models.CharField(default=UNICODE_EMOJI, choices=
        REACTION_TYPES, max_length=30)
    emoji_code = models.TextField()


    class Meta:
        unique_together = 'user_profile', 'message', 'emoji_name'

    @staticmethod
    def get_raw_db_rows(needed_ids: List[int]) ->List[Dict[str, Any]]:
        fields = ['message_id', 'emoji_name', 'emoji_code', 'reaction_type',
            'user_profile__email', 'user_profile__id',
            'user_profile__full_name']
        return Reaction.objects.filter(message_id__in=needed_ids).values(*
            fields)


class AbstractUserMessage(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    ALL_FLAGS = ['read', 'starred', 'collapsed', 'mentioned',
        'wildcard_mentioned', 'summarize_in_home', 'summarize_in_stream',
        'force_expand', 'force_collapse', 'has_alert_word', 'historical',
        'is_private', 'active_mobile_push_notification']
    NON_API_FLAGS = {'is_private', 'active_mobile_push_notification'}
    flags = BitField(flags=ALL_FLAGS, default=0)


    class Meta:
        abstract = True
        unique_together = 'user_profile', 'message'

    @staticmethod
    def where_unread() ->str:
        return 'flags & 1 = 0'

    @staticmethod
    def where_starred() ->str:
        return 'flags & 2 <> 0'

    @staticmethod
    def where_active_push_notification() ->str:
        return 'flags & 4096 <> 0'

    def flags_list(self) ->List[str]:
        flags = int(self.flags)
        return self.flags_list_for_flags(flags)

    @staticmethod
    def flags_list_for_flags(val: int) ->List[str]:
        """
        This function is highly optimized, because it actually slows down
        sending messages in a naive implementation.
        """
        flags = []
        mask = 1
        for flag in UserMessage.ALL_FLAGS:
            if val & mask and flag not in AbstractUserMessage.NON_API_FLAGS:
                flags.append(flag)
            mask <<= 1
        return flags

    def __str__(self) ->str:
        display_recipient = get_display_recipient(self.message.recipient)
        return '<%s: %s / %s (%s)>' % (self.__class__.__name__,
            display_recipient, self.user_profile.email, self.flags_list())


class UserMessage(AbstractUserMessage):
    message = models.ForeignKey(Message, on_delete=CASCADE)


def get_usermessage_by_message_id(user_profile: UserProfile, message_id: int
    ) ->Optional[UserMessage]:
    try:
        return UserMessage.objects.select_related().get(user_profile=
            user_profile, message__id=message_id)
    except UserMessage.DoesNotExist:
        return None


class ArchivedUserMessage(AbstractUserMessage):
    """Used as a temporary holding place for deleted UserMessages objects
    before they are permanently deleted.  This is an important part of
    a robust 'message retention' feature.
    """
    message = models.ForeignKey(ArchivedMessage, on_delete=CASCADE)
    archive_timestamp = models.DateTimeField(default=timezone_now, db_index
        =True)


class AbstractAttachment(models.Model):
    file_name = models.TextField(db_index=True)
    path_id = models.TextField(db_index=True, unique=True)
    owner = models.ForeignKey(UserProfile, on_delete=CASCADE)
    realm = models.ForeignKey(Realm, blank=True, null=True, on_delete=CASCADE)
    create_time = models.DateTimeField(default=timezone_now, db_index=True)
    size = models.IntegerField(null=True)
    is_realm_public = models.BooleanField(default=False)


    class Meta:
        abstract = True

    def __str__(self) ->str:
        return '<%s: %s>' % (self.__class__.__name__, self.file_name)


class ArchivedAttachment(AbstractAttachment):
    """Used as a temporary holding place for deleted Attachment objects
    before they are permanently deleted.  This is an important part of
    a robust 'message retention' feature.
    """
    archive_timestamp = models.DateTimeField(default=timezone_now, db_index
        =True)
    messages = models.ManyToManyField(ArchivedMessage)


class Attachment(AbstractAttachment):
    messages = models.ManyToManyField(Message)

    def is_claimed(self) ->bool:
        return self.messages.count() > 0

    def to_dict(self) ->Dict[str, Any]:
        return {'id': self.id, 'name': self.file_name, 'path_id': self.
            path_id, 'size': self.size, 'create_time': time.mktime(self.
            create_time.timetuple()) * 1000, 'messages': [{'id': m.id,
            'name': time.mktime(m.pub_date.timetuple()) * 1000} for m in
            self.messages.all()]}


def validate_attachment_request(user_profile: UserProfile, path_id: str
    ) ->Optional[bool]:
    try:
        attachment = Attachment.objects.get(path_id=path_id)
    except Attachment.DoesNotExist:
        return None
    if user_profile == attachment.owner:
        return True
    if (attachment.is_realm_public and attachment.realm == user_profile.
        realm and user_profile.can_access_public_streams()):
        return True
    messages = attachment.messages.all()
    if UserMessage.objects.filter(user_profile=user_profile, message__in=
        messages).exists():
        return True
    relevant_stream_ids = Subscription.objects.filter(user_profile=
        user_profile, active=True, recipient__type=Recipient.STREAM,
        recipient__in=[m.recipient_id for m in messages]).values_list(
        'recipient__type_id', flat=True)
    if len(relevant_stream_ids) == 0:
        return False
    return Stream.objects.filter(id__in=relevant_stream_ids,
        history_public_to_subscribers=True).exists()


def get_old_unclaimed_attachments(weeks_ago: int) ->Sequence[Attachment]:
    delta_weeks_ago = timezone_now() - datetime.timedelta(weeks=weeks_ago)
    old_attachments = Attachment.objects.filter(messages=None,
        create_time__lt=delta_weeks_ago)
    return old_attachments


class Subscription(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    recipient = models.ForeignKey(Recipient, on_delete=CASCADE)
    active = models.BooleanField(default=True)
    in_home_view = models.NullBooleanField(default=True)
    DEFAULT_STREAM_COLOR = '#c2c2c2'
    color = models.CharField(max_length=10, default=DEFAULT_STREAM_COLOR)
    pin_to_top = models.BooleanField(default=False)
    desktop_notifications = models.BooleanField(default=True)
    audible_notifications = models.BooleanField(default=True)
    push_notifications = models.BooleanField(default=False)
    email_notifications = models.BooleanField(default=False)


    class Meta:
        unique_together = 'user_profile', 'recipient'

    def __str__(self) ->str:
        return '<Subscription: %s -> %s>' % (self.user_profile, self.recipient)


@cache_with_key(user_profile_by_id_cache_key, timeout=3600 * 24 * 7)
def get_user_profile_by_id(uid: int) ->UserProfile:
    return UserProfile.objects.select_related().get(id=uid)


@cache_with_key(user_profile_by_email_cache_key, timeout=3600 * 24 * 7)
def get_user_profile_by_email(email: str) ->UserProfile:
    return UserProfile.objects.select_related().get(delivery_email__iexact=
        email.strip())


@cache_with_key(user_profile_by_api_key_cache_key, timeout=3600 * 24 * 7)
def get_user_profile_by_api_key(api_key: str) ->UserProfile:
    return UserProfile.objects.select_related().get(api_key=api_key)


def get_user_by_delivery_email(email: str, realm: Realm) ->UserProfile:
    return UserProfile.objects.select_related().get(delivery_email__iexact=
        email.strip(), realm=realm)


@cache_with_key(user_profile_cache_key, timeout=3600 * 24 * 7)
def get_user(email: str, realm: Realm) ->UserProfile:
    return UserProfile.objects.select_related().get(email__iexact=email.
        strip(), realm=realm)


def get_active_user_by_delivery_email(email: str, realm: Realm) ->UserProfile:
    user_profile = get_user_by_delivery_email(email, realm)
    if not user_profile.is_active:
        raise UserProfile.DoesNotExist()
    return user_profile


def get_active_user(email: str, realm: Realm) ->UserProfile:
    user_profile = get_user(email, realm)
    if not user_profile.is_active:
        raise UserProfile.DoesNotExist()
    return user_profile


def get_user_profile_by_id_in_realm(uid: int, realm: Realm) ->UserProfile:
    return UserProfile.objects.select_related().get(id=uid, realm=realm)


def get_user_including_cross_realm(email: str, realm: Optional[Realm]=None
    ) ->UserProfile:
    if is_cross_realm_bot_email(email):
        return get_system_bot(email)
    assert realm is not None
    return get_user(email, realm)


@cache_with_key(bot_profile_cache_key, timeout=3600 * 24 * 7)
def get_system_bot(email: str) ->UserProfile:
    return UserProfile.objects.select_related().get(email__iexact=email.strip()
        )


def get_user_by_id_in_realm_including_cross_realm(uid: int, realm: Realm
    ) ->UserProfile:
    user_profile = get_user_profile_by_id(uid)
    if user_profile.realm == realm:
        return user_profile
    if user_profile.email in settings.CROSS_REALM_BOT_EMAILS:
        return user_profile
    raise UserProfile.DoesNotExist()


@cache_with_key(realm_user_dicts_cache_key, timeout=3600 * 24 * 7)
def get_realm_user_dicts(realm_id: int) ->List[Dict[str, Any]]:
    return UserProfile.objects.filter(realm_id=realm_id).values(*
        realm_user_dict_fields)


@cache_with_key(active_user_ids_cache_key, timeout=3600 * 24 * 7)
def active_user_ids(realm_id: int) ->List[int]:
    query = UserProfile.objects.filter(realm_id=realm_id, is_active=True
        ).values_list('id', flat=True)
    return list(query)


@cache_with_key(active_non_guest_user_ids_cache_key, timeout=3600 * 24 * 7)
def active_non_guest_user_ids(realm_id: int) ->List[int]:
    query = UserProfile.objects.filter(realm_id=realm_id, is_active=True,
        is_guest=False).values_list('id', flat=True)
    return list(query)


def get_source_profile(email: str, string_id: str) ->Optional[UserProfile]:
    realm = get_realm(string_id)
    if realm is None:
        return None
    try:
        return get_user_by_delivery_email(email, realm)
    except UserProfile.DoesNotExist:
        return None


@cache_with_key(bot_dicts_in_realm_cache_key, timeout=3600 * 24 * 7)
def get_bot_dicts_in_realm(realm: Realm) ->List[Dict[str, Any]]:
    return UserProfile.objects.filter(realm=realm, is_bot=True).values(*
        bot_dict_fields)


def is_cross_realm_bot_email(email: str) ->bool:
    return email.lower() in settings.CROSS_REALM_BOT_EMAILS


class Huddle(models.Model):
    huddle_hash = models.CharField(max_length=40, db_index=True, unique=True)


def get_huddle_hash(id_list: List[int]) ->str:
    id_list = sorted(set(id_list))
    hash_key = ','.join(str(x) for x in id_list)
    return make_safe_digest(hash_key)


def huddle_hash_cache_key(huddle_hash: str) ->str:
    return 'huddle_by_hash:%s' % (huddle_hash,)


def get_huddle(id_list: List[int]) ->Huddle:
    huddle_hash = get_huddle_hash(id_list)
    return get_huddle_backend(huddle_hash, id_list)


@cache_with_key(lambda huddle_hash, id_list: huddle_hash_cache_key(
    huddle_hash), timeout=3600 * 24 * 7)
def get_huddle_backend(huddle_hash: str, id_list: List[int]) ->Huddle:
    with transaction.atomic():
        huddle, created = Huddle.objects.get_or_create(huddle_hash=huddle_hash)
        if created:
            recipient = Recipient.objects.create(type_id=huddle.id, type=
                Recipient.HUDDLE)
            subs_to_create = [Subscription(recipient=recipient,
                user_profile_id=user_profile_id) for user_profile_id in id_list
                ]
            Subscription.objects.bulk_create(subs_to_create)
        return huddle


def clear_database() ->None:
    pylibmc.Client(['127.0.0.1']).flush_all()
    model = None
    for model in [Message, Stream, UserProfile, Recipient, Realm,
        Subscription, Huddle, UserMessage, Client, DefaultStream]:
        model.objects.all().delete()
    Session.objects.all().delete()


class UserActivity(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    client = models.ForeignKey(Client, on_delete=CASCADE)
    query = models.CharField(max_length=50, db_index=True)
    count = models.IntegerField()
    last_visit = models.DateTimeField('last visit')


    class Meta:
        unique_together = 'user_profile', 'client', 'query'


class UserActivityInterval(models.Model):
    MIN_INTERVAL_LENGTH = datetime.timedelta(minutes=15)
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    start = models.DateTimeField('start time', db_index=True)
    end = models.DateTimeField('end time', db_index=True)


class UserPresence(models.Model):
    """A record from the last time we heard from a given user on a given client.

    This is a tricky subsystem, because it is highly optimized.  See the docs:
      https://zulip.readthedocs.io/en/latest/subsystems/presence.html
    """


    class Meta:
        unique_together = 'user_profile', 'client'
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    client = models.ForeignKey(Client, on_delete=CASCADE)
    timestamp = models.DateTimeField('presence changed')
    ACTIVE = 1
    IDLE = 2
    status = models.PositiveSmallIntegerField(default=ACTIVE)

    @staticmethod
    def status_to_string(status: int) ->str:
        if status == UserPresence.ACTIVE:
            return 'active'
        elif status == UserPresence.IDLE:
            return 'idle'
        else:
            raise ValueError('Unknown status: %s' % (status,))

    @staticmethod
    def get_status_dict_by_user(user_profile: UserProfile) ->Dict[str, Dict
        [str, Any]]:
        query = UserPresence.objects.filter(user_profile=user_profile).values(
            'client__name', 'status', 'timestamp', 'user_profile__email',
            'user_profile__id',
            'user_profile__enable_offline_push_notifications')
        presence_rows = list(query)
        mobile_user_ids = set()
        if PushDeviceToken.objects.filter(user=user_profile).exists():
            mobile_user_ids.add(user_profile.id)
        return UserPresence.get_status_dicts_for_rows(presence_rows,
            mobile_user_ids)

    @staticmethod
    def get_status_dict_by_realm(realm_id: int) ->Dict[str, Dict[str, Any]]:
        user_profile_ids = UserProfile.objects.filter(realm_id=realm_id,
            is_active=True, is_bot=False).order_by('id').values_list('id',
            flat=True)
        user_profile_ids = list(user_profile_ids)
        if not user_profile_ids:
            return {}
        two_weeks_ago = timezone_now() - datetime.timedelta(weeks=2)
        query = UserPresence.objects.filter(timestamp__gte=two_weeks_ago
            ).values('client__name', 'status', 'timestamp',
            'user_profile__email', 'user_profile__id',
            'user_profile__enable_offline_push_notifications')
        query = query_for_ids(query=query, user_ids=user_profile_ids, field
            ='user_profile_id')
        presence_rows = list(query)
        mobile_query = PushDeviceToken.objects.distinct('user_id').values_list(
            'user_id', flat=True)
        mobile_query = query_for_ids(query=mobile_query, user_ids=
            user_profile_ids, field='user_id')
        mobile_user_ids = set(mobile_query)
        return UserPresence.get_status_dicts_for_rows(presence_rows,
            mobile_user_ids)

    @staticmethod
    def get_status_dicts_for_rows(presence_rows: List[Dict[str, Any]],
        mobile_user_ids: Set[int]) ->Dict[str, Dict[str, Any]]:
        info_row_dct = defaultdict(list)
        for row in presence_rows:
            email = row['user_profile__email']
            client_name = row['client__name']
            status = UserPresence.status_to_string(row['status'])
            dt = row['timestamp']
            timestamp = datetime_to_timestamp(dt)
            push_enabled = row[
                'user_profile__enable_offline_push_notifications']
            has_push_devices = row['user_profile__id'] in mobile_user_ids
            pushable = push_enabled and has_push_devices
            info = dict(client=client_name, status=status, dt=dt, timestamp
                =timestamp, pushable=pushable)
            info_row_dct[email].append(info)
        user_statuses = dict()
        for email, info_rows in info_row_dct.items():
            by_time = lambda row: row['dt']
            most_recent_info = max(info_rows, key=by_time)
            for r in info_rows:
                del r['dt']
            client_dict = {info['client']: info for info in info_rows}
            user_statuses[email] = client_dict
            user_statuses[email]['aggregated'] = dict(client=
                most_recent_info['client'], status=most_recent_info[
                'status'], timestamp=most_recent_info['timestamp'])
        return user_statuses

    @staticmethod
    def to_presence_dict(client_name: str, status: int, dt: datetime.
        datetime, push_enabled: bool=False, has_push_devices: bool=False
        ) ->Dict[str, Any]:
        presence_val = UserPresence.status_to_string(status)
        timestamp = datetime_to_timestamp(dt)
        return dict(client=client_name, status=presence_val, timestamp=
            timestamp, pushable=push_enabled and has_push_devices)

    def to_dict(self) ->Dict[str, Any]:
        return UserPresence.to_presence_dict(self.client.name, self.status,
            self.timestamp)

    @staticmethod
    def status_from_string(status: str) ->Optional[int]:
        if status == 'active':
            status_val = UserPresence.ACTIVE
        elif status == 'idle':
            status_val = UserPresence.IDLE
        else:
            status_val = None
        return status_val


class DefaultStream(models.Model):
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    stream = models.ForeignKey(Stream, on_delete=CASCADE)


    class Meta:
        unique_together = 'realm', 'stream'


class DefaultStreamGroup(models.Model):
    MAX_NAME_LENGTH = 60
    name = models.CharField(max_length=MAX_NAME_LENGTH, db_index=True)
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    streams = models.ManyToManyField('Stream')
    description = models.CharField(max_length=1024, default='')


    class Meta:
        unique_together = 'realm', 'name'

    def to_dict(self) ->Dict[str, Any]:
        return dict(name=self.name, id=self.id, description=self.
            description, streams=[stream.to_dict() for stream in self.
            streams.all()])


def get_default_stream_groups(realm: Realm) ->List[DefaultStreamGroup]:
    return DefaultStreamGroup.objects.filter(realm=realm)


class AbstractScheduledJob(models.Model):
    scheduled_timestamp = models.DateTimeField(db_index=True)
    data = models.TextField()
    realm = models.ForeignKey(Realm, on_delete=CASCADE)


    class Meta:
        abstract = True


class ScheduledEmail(AbstractScheduledJob):
    user = models.ForeignKey(UserProfile, null=True, on_delete=CASCADE)
    address = models.EmailField(null=True, db_index=True)
    WELCOME = 1
    DIGEST = 2
    INVITATION_REMINDER = 3
    type = models.PositiveSmallIntegerField()

    def __str__(self) ->str:
        return '<ScheduledEmail: %s %s %s>' % (self.type, self.user or self
            .address, self.scheduled_timestamp)


class ScheduledMessage(models.Model):
    sender = models.ForeignKey(UserProfile, on_delete=CASCADE)
    recipient = models.ForeignKey(Recipient, on_delete=CASCADE)
    subject = models.CharField(max_length=MAX_TOPIC_NAME_LENGTH)
    content = models.TextField()
    sending_client = models.ForeignKey(Client, on_delete=CASCADE)
    stream = models.ForeignKey(Stream, null=True, on_delete=CASCADE)
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    scheduled_timestamp = models.DateTimeField(db_index=True)
    delivered = models.BooleanField(default=False)
    SEND_LATER = 1
    REMIND = 2
    DELIVERY_TYPES = (SEND_LATER, 'send_later'), (REMIND, 'remind')
    delivery_type = models.PositiveSmallIntegerField(choices=DELIVERY_TYPES,
        default=SEND_LATER)

    def topic_name(self) ->str:
        return self.subject

    def set_topic_name(self, topic_name: str) ->None:
        self.subject = topic_name

    def __str__(self) ->str:
        display_recipient = get_display_recipient(self.recipient)
        return '<ScheduledMessage: %s %s %s %s>' % (display_recipient, self
            .subject, self.sender, self.scheduled_timestamp)


EMAIL_TYPES = {'followup_day1': ScheduledEmail.WELCOME, 'followup_day2':
    ScheduledEmail.WELCOME, 'digest': ScheduledEmail.DIGEST,
    'invitation_reminder': ScheduledEmail.INVITATION_REMINDER}


class RealmAuditLog(models.Model):
    """
    RealmAuditLog tracks important changes to users, streams, and
    realms in Zulip.  It is intended to support both
    debugging/introspection (e.g. determining when a user's left a
    given stream?) as well as help with some database migrations where
    we might be able to do a better data backfill with it.  Here are a
    few key details about how this works:

    * acting_user is the user who initiated the state change
    * modified_user (if present) is the user being modified
    * modified_stream (if present) is the stream being modified

    For example:
    * When a user subscribes another user to a stream, modified_user,
      acting_user, and modified_stream will all be present and different.
    * When an administrator changes an organization's realm icon,
      acting_user is that administrator and both modified_user and
      modified_stream will be None.
    """
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    acting_user = models.ForeignKey(UserProfile, null=True, related_name=
        '+', on_delete=CASCADE)
    modified_user = models.ForeignKey(UserProfile, null=True, related_name=
        '+', on_delete=CASCADE)
    modified_stream = models.ForeignKey(Stream, null=True, on_delete=CASCADE)
    event_last_message_id = models.IntegerField(null=True)
    event_time = models.DateTimeField(db_index=True)
    backfilled = models.BooleanField(default=False)
    requires_billing_update = models.BooleanField(default=False)
    extra_data = models.TextField(null=True)
    STRIPE_CUSTOMER_CREATED = 'stripe_customer_created'
    STRIPE_CARD_CHANGED = 'stripe_card_changed'
    STRIPE_PLAN_CHANGED = 'stripe_plan_changed'
    STRIPE_PLAN_QUANTITY_RESET = 'stripe_plan_quantity_reset'
    CUSTOMER_CREATED = 'customer_created'
    CUSTOMER_PLAN_CREATED = 'customer_plan_created'
    USER_CREATED = 'user_created'
    USER_ACTIVATED = 'user_activated'
    USER_DEACTIVATED = 'user_deactivated'
    USER_REACTIVATED = 'user_reactivated'
    USER_SOFT_ACTIVATED = 'user_soft_activated'
    USER_SOFT_DEACTIVATED = 'user_soft_deactivated'
    USER_PASSWORD_CHANGED = 'user_password_changed'
    USER_AVATAR_SOURCE_CHANGED = 'user_avatar_source_changed'
    USER_FULL_NAME_CHANGED = 'user_full_name_changed'
    USER_EMAIL_CHANGED = 'user_email_changed'
    USER_TOS_VERSION_CHANGED = 'user_tos_version_changed'
    USER_API_KEY_CHANGED = 'user_api_key_changed'
    USER_BOT_OWNER_CHANGED = 'user_bot_owner_changed'
    REALM_DEACTIVATED = 'realm_deactivated'
    REALM_REACTIVATED = 'realm_reactivated'
    REALM_SCRUBBED = 'realm_scrubbed'
    REALM_PLAN_TYPE_CHANGED = 'realm_plan_type_changed'
    REALM_LOGO_CHANGED = 'realm_logo_changed'
    SUBSCRIPTION_CREATED = 'subscription_created'
    SUBSCRIPTION_ACTIVATED = 'subscription_activated'
    SUBSCRIPTION_DEACTIVATED = 'subscription_deactivated'
    event_type = models.CharField(max_length=40)

    def __str__(self) ->str:
        if self.modified_user is not None:
            return '<RealmAuditLog: %s %s %s %s>' % (self.modified_user,
                self.event_type, self.event_time, self.id)
        if self.modified_stream is not None:
            return '<RealmAuditLog: %s %s %s %s>' % (self.modified_stream,
                self.event_type, self.event_time, self.id)
        return '<RealmAuditLog: %s %s %s %s>' % (self.realm, self.
            event_type, self.event_time, self.id)


class UserHotspot(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=CASCADE)
    hotspot = models.CharField(max_length=30)
    timestamp = models.DateTimeField(default=timezone_now)


    class Meta:
        unique_together = 'user', 'hotspot'


def check_valid_user_ids(realm_id: int, user_ids: List[int],
    allow_deactivated: bool=False) ->Optional[str]:
    error = check_list(check_int)('User IDs', user_ids)
    if error:
        return error
    realm = Realm.objects.get(id=realm_id)
    for user_id in user_ids:
        try:
            user_profile = get_user_profile_by_id_in_realm(user_id, realm)
        except UserProfile.DoesNotExist:
            return _('Invalid user ID: %d') % user_id
        if not allow_deactivated:
            if not user_profile.is_active:
                return _('User with ID %d is deactivated') % user_id
        if user_profile.is_bot:
            return _('User with ID %d is a bot') % user_id
    return None


class CustomProfileField(models.Model):
    """Defines a form field for the per-realm custom profile fields feature.

    See CustomProfileFieldValue for an individual user's values for one of
    these fields.
    """
    HINT_MAX_LENGTH = 80
    NAME_MAX_LENGTH = 40
    realm = models.ForeignKey(Realm, on_delete=CASCADE)
    name = models.CharField(max_length=NAME_MAX_LENGTH)
    hint = models.CharField(max_length=HINT_MAX_LENGTH, default='', null=True)
    order = models.IntegerField(default=0)
    SHORT_TEXT = 1
    LONG_TEXT = 2
    CHOICE = 3
    DATE = 4
    URL = 5
    USER = 6
    CHOICE_FIELD_TYPE_DATA = [(CHOICE, str(_('List of options')),
        validate_choice_field, str, 'CHOICE')]
    USER_FIELD_TYPE_DATA = [(USER, str(_('Person picker')),
        check_valid_user_ids, eval, 'USER')]
    CHOICE_FIELD_VALIDATORS = {item[0]: item[2] for item in
        CHOICE_FIELD_TYPE_DATA}
    USER_FIELD_VALIDATORS = {item[0]: item[2] for item in USER_FIELD_TYPE_DATA}
    FIELD_TYPE_DATA = [(SHORT_TEXT, str(_('Short text')),
        check_short_string, str, 'SHORT_TEXT'), (LONG_TEXT, str(_(
        'Long text')), check_long_string, str, 'LONG_TEXT'), (DATE, str(_(
        'Date picker')), check_date, str, 'DATE'), (URL, str(_('Link')),
        check_url, str, 'URL')]
    ALL_FIELD_TYPES = (FIELD_TYPE_DATA + CHOICE_FIELD_TYPE_DATA +
        USER_FIELD_TYPE_DATA)
    FIELD_VALIDATORS = {item[0]: item[2] for item in FIELD_TYPE_DATA}
    FIELD_CONVERTERS = {item[0]: item[3] for item in ALL_FIELD_TYPES}
    FIELD_TYPE_CHOICES = [(item[0], item[1]) for item in ALL_FIELD_TYPES]
    FIELD_TYPE_CHOICES_DICT = {item[4]: {'id': item[0], 'name': item[1]} for
        item in ALL_FIELD_TYPES}
    field_type = models.PositiveSmallIntegerField(choices=
        FIELD_TYPE_CHOICES, default=SHORT_TEXT)
    field_data = models.TextField(default='', null=True)


    class Meta:
        unique_together = 'realm', 'name'

    def as_dict(self) ->ProfileDataElement:
        return {'id': self.id, 'name': self.name, 'type': self.field_type,
            'hint': self.hint, 'field_data': self.field_data, 'order': self
            .order}

    def is_renderable(self) ->bool:
        if self.field_type in [CustomProfileField.SHORT_TEXT,
            CustomProfileField.LONG_TEXT]:
            return True
        return False

    def __str__(self) ->str:
        return '<CustomProfileField: %s %s %s %d>' % (self.realm, self.name,
            self.field_type, self.order)


def custom_profile_fields_for_realm(realm_id: int) ->List[CustomProfileField]:
    return CustomProfileField.objects.filter(realm=realm_id).order_by('order')


class CustomProfileFieldValue(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    field = models.ForeignKey(CustomProfileField, on_delete=CASCADE)
    value = models.TextField()
    rendered_value = models.TextField(null=True, default=None)


    class Meta:
        unique_together = 'user_profile', 'field'

    def __str__(self) ->str:
        return '<CustomProfileFieldValue: %s %s %s>' % (self.user_profile,
            self.field, self.value)


GENERIC_INTERFACE = 'GenericService'
SLACK_INTERFACE = 'SlackOutgoingWebhookService'


class Service(models.Model):
    name = models.CharField(max_length=UserProfile.MAX_NAME_LENGTH)
    user_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    base_url = models.TextField()
    token = models.TextField()
    interface = models.PositiveSmallIntegerField(default=1)
    GENERIC = 1
    SLACK = 2
    ALLOWED_INTERFACE_TYPES = [GENERIC, SLACK]
    _interfaces = {GENERIC: GENERIC_INTERFACE, SLACK: SLACK_INTERFACE}

    def interface_name(self) ->str:
        return self._interfaces[self.interface]


def get_bot_services(user_profile_id: str) ->List[Service]:
    return list(Service.objects.filter(user_profile__id=user_profile_id))


def get_service_profile(user_profile_id: str, service_name: str) ->Service:
    return Service.objects.get(user_profile__id=user_profile_id, name=
        service_name)


class BotStorageData(models.Model):
    bot_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    key = models.TextField(db_index=True)
    value = models.TextField()


    class Meta:
        unique_together = 'bot_profile', 'key'


class BotConfigData(models.Model):
    bot_profile = models.ForeignKey(UserProfile, on_delete=CASCADE)
    key = models.TextField(db_index=True)
    value = models.TextField()


    class Meta(object):
        unique_together = 'bot_profile', 'key'
