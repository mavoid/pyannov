import annotations
import collections
import abc
import inspect
import sys
import six


class ComponentRegistry:
    """Holds a registry of the namespaces used by components."""
    registry = set()
    __dist_cache = {}

    @classmethod
    def get_resources(cls, resource_name):
        cached = cls.__dist_cache.get(resource_name)
        if cached:
            return cached
        cls.__dist_cache[resource_name] = resources = []
        for module_name in cls.registry:
            module = sys.modules[module_name]
            resources.extend(getattr(module, resource_name, []))
        return resources


class ComponentMeta(abc.ABCMeta):

    def __new__(mcs, name, bases, attributes):
        component = abc.ABCMeta.__new__(mcs, name, bases, attributes)
        module = attributes['__module__'].split('.')[0]
        if name == 'Component' or module == 'builtins':
            return component
        ComponentRegistry.registry.add(module)
        return component


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def _check_if_has_indexable_children(item):
    if not hasattr(item, 'children') or not isinstance(item.children, Component
        ) and not isinstance(item.children, (tuple, collections.
        MutableSequence)):
        raise KeyError


@six.add_metaclass(ComponentMeta)
class Component(collections.MutableMapping):


    class _UNDEFINED(object):

        def __repr__(self):
            return 'undefined'

        def __str__(self):
            return 'undefined'
    UNDEFINED = _UNDEFINED()


    class _REQUIRED(object):

        def __repr__(self):
            return 'required'

        def __str__(self):
            return 'required'
    REQUIRED = _REQUIRED()

    def __init__(self, **kwargs):
        for k, v in list(kwargs.items()):
            k_in_propnames = k in self._prop_names
            k_in_wildcards = any([k.startswith(w) for w in self.
                _valid_wildcard_attributes])
            if not k_in_propnames and not k_in_wildcards:
                raise TypeError('Unexpected keyword argument `{}`'.format(k
                    ) + """
Allowed arguments: {}""".format(', '.join(
                    sorted(self._prop_names))))
            setattr(self, k, v)

    def to_plotly_json(self):
        props = {p: getattr(self, p) for p in self._prop_names if hasattr(
            self, p)}
        props.update({k: getattr(self, k) for k in self.__dict__ if any(k.
            startswith(w) for w in self._valid_wildcard_attributes)})
        as_json = {'props': props, 'type': self._type, 'namespace': self.
            _namespace}
        return as_json

    def _get_set_or_delete(self, id, operation, new_item=None):
        _check_if_has_indexable_children(self)
        if isinstance(self.children, Component):
            if getattr(self.children, 'id', None) is not None:
                if self.children.id == id:
                    if operation == 'get':
                        return self.children
                    elif operation == 'set':
                        self.children = new_item
                        return
                    elif operation == 'delete':
                        self.children = None
                        return
            try:
                if operation == 'get':
                    return self.children.__getitem__(id)
                elif operation == 'set':
                    self.children.__setitem__(id, new_item)
                    return
                elif operation == 'delete':
                    self.children.__delitem__(id)
                    return
            except KeyError:
                pass
        if isinstance(self.children, (tuple, collections.MutableSequence)):
            for i, item in enumerate(self.children):
                if getattr(item, 'id', None) == id:
                    if operation == 'get':
                        return item
                    elif operation == 'set':
                        self.children[i] = new_item
                        return
                    elif operation == 'delete':
                        del self.children[i]
                        return
                elif isinstance(item, Component):
                    try:
                        if operation == 'get':
                            return item.__getitem__(id)
                        elif operation == 'set':
                            item.__setitem__(id, new_item)
                            return
                        elif operation == 'delete':
                            item.__delitem__(id)
                            return
                    except KeyError:
                        pass
        raise KeyError(id)

    def __getitem__(self, id):
        """Recursively find the element with the given ID through the tree
        of children.
        """
        return self._get_set_or_delete(id, 'get')

    def __setitem__(self, id, item):
        """Set an element by its ID."""
        return self._get_set_or_delete(id, 'set', item)

    def __delitem__(self, id):
        """Delete items by ID in the tree of children."""
        return self._get_set_or_delete(id, 'delete')

    def traverse(self):
        """Yield each item in the tree."""
        for t in self.traverse_with_paths():
            yield t[1]

    def traverse_with_paths(self):
        """Yield each item with its path in the tree."""
        children = getattr(self, 'children', None)
        children_type = type(children).__name__
        children_id = '(id={:s})'.format(children.id) if getattr(children,
            'id', False) else ''
        children_string = children_type + ' ' + children_id
        if isinstance(children, Component):
            yield '[*] ' + children_string, children
            for p, t in children.traverse_with_paths():
                yield '\n'.join(['[*] ' + children_string, p]), t
        elif isinstance(children, (tuple, collections.MutableSequence)):
            for idx, i in enumerate(children):
                list_path = '[{:d}] {:s} {}'.format(idx, type(i).__name__, 
                    '(id={:s})'.format(i.id) if getattr(i, 'id', False) else ''
                    )
                yield list_path, i
                if isinstance(i, Component):
                    for p, t in i.traverse_with_paths():
                        yield '\n'.join([list_path, p]), t

    def __iter__(self):
        """Yield IDs in the tree of children."""
        for t in self.traverse():
            if isinstance(t, Component) and getattr(t, 'id', None) is not None:
                yield t.id

    def __len__(self):
        """Return the number of items in the tree."""
        length = 0
        if getattr(self, 'children', None) is None:
            length = 0
        elif isinstance(self.children, Component):
            length = 1
            length += len(self.children)
        elif isinstance(self.children, (tuple, collections.MutableSequence)):
            for c in self.children:
                length += 1
                if isinstance(c, Component):
                    length += len(c)
        else:
            length = 1
        return length


def _explicitize_args(func):
    varnames = annotations.annot_dash_development_base_component__explicitize_args_wrapper_varnames
    if hasattr(func, 'func_code'):
        varnames = func.func_code.co_varnames
    else:
        varnames = func.__code__.co_varnames

    def wrapper(*args, **kwargs):
        if '_explicit_args' in kwargs.keys():
            raise Exception('Variable _explicit_args should not be set.')
        kwargs['_explicit_args'] = list(set(list(varnames[:len(args)]) + [k for
            k, _ in kwargs.items()]))
        if 'self' in kwargs['_explicit_args']:
            kwargs['_explicit_args'].remove('self')
        return func(*args, **kwargs)
    if hasattr(inspect, 'signature'):
        new_sig = inspect.signature(wrapper).replace(parameters=inspect.
            signature(func).parameters.values())
        wrapper.__signature__ = new_sig
    return wrapper
