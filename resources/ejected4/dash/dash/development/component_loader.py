import annotations
import collections
import json
import os
from ._py_components_generation import generate_class_file, generate_imports, generate_classes_files, generate_class
from .base_component import ComponentRegistry


def _get_metadata(metadata_path):
    with open(metadata_path) as data_file:
        json_string = data_file.read()
        data = json.JSONDecoder(object_pairs_hook=collections.OrderedDict
            ).decode(json_string)
    return data


def load_components(metadata_path, namespace='default_namespace'):
    data = annotations.annot_dash_development_component_loader_load_components_data
    """Load React component metadata into a format Dash can parse.

    Usage: load_components('../../component-suites/lib/metadata.json')

    Keyword arguments:
    metadata_path -- a path to a JSON file created by
    [`react-docgen`](https://github.com/reactjs/react-docgen).

    Returns:
    components -- a list of component objects with keys
    `type`, `valid_kwargs`, and `setup`.
    """
    ComponentRegistry.registry.add(namespace)
    components = []
    data = _get_metadata(metadata_path)
    for componentPath in data:
        componentData = data[componentPath]
        name = componentPath.split('/').pop().split('.')[0]
        component = generate_class(name, componentData['props'],
            componentData['description'], namespace)
        components.append(component)
    return components


def generate_classes(namespace, metadata_path='lib/metadata.json'):
    namespace = annotations.annot_dash_development_component_loader_generate_classes_namespace
    """Load React component metadata into a format Dash can parse,
    then create python class files.

    Usage: generate_classes()

    Keyword arguments:
    namespace -- name of the generated python package (also output dir)

    metadata_path -- a path to a JSON file created by
    [`react-docgen`](https://github.com/reactjs/react-docgen).

    Returns:
    """
    data = _get_metadata(metadata_path)
    imports_path = os.path.join(namespace, '_imports_.py')
    if os.path.exists(imports_path):
        os.remove(imports_path)
    components = generate_classes_files(namespace, data, generate_class_file)
    generate_imports(namespace, components)
