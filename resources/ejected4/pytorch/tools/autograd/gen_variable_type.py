import annotations
CodeTemplate = annotations.annot_tools_autograd_gen_variable_type_CodeTemplate
from __future__ import print_function
import os
import sys
from .utils import CodeTemplate, nested_dict, write, uninplace_api_name
from .gen_autograd import VIEW_FUNCTIONS
from .gen_autograd_functions import uses_single_grad
MANUAL_IMPLEMENTATIONS = {'resize_', 'resize_as_', 'detach', 'detach_',
    's_copy_', '_s_copy_from'}
DONT_RECORD_TRACE = {'convolution', 'conv1d', 'conv2d', 'conv3d',
    'conv_transpose1d', 'conv_transpose2d', 'conv_transpose3d', 'lstm_cell',
    'gru_cell', 'rnn_tanh_cell', 'rnn_relu_cell', 'linear', '_coalesced_'}
RENAME_TRACE = {'zero': 'zeros_like', 'fill': 'full_like'}
RENAME_ATTRIBUTES = {('fill_', 'value'): 'fill_value'}
DONT_PROFILE = {'data_ptr', 'get_device', 'is_contiguous', 'is_cuda',
    'is_distributed', 'is_same_size', 'is_set_to', 'is_signed', 'is_sparse',
    'numel', 'size', 'storage_offset', 'stride'}
DONT_REQUIRE_DERIVATIVE = {'ones_like', 'zeros_like', 'rand_like',
    'randn_like', '__and__', '__iand__', '__ilshift__', '__ior__',
    '__irshift__', '__ixor__', '__lshift__', '__or__', '__rshift__',
    '__xor__', '_coalesced_'}
METHOD_DECLARATION = CodeTemplate(
    """${return_type} ${method_prefix_derived}${api_name}(${type_method_formals}) const override;
"""
    )
METHOD_DEFINITION = CodeTemplate(
    """${return_type} VariableType::${method_prefix_derived}${api_name}(${type_method_formals}) const {
  ${type_definition_body}
}
"""
    )
UNPACK_TENSOR = CodeTemplate(
    'auto${ref} ${arg_name}_ = unpack${suffix}(${arg_name}, "${arg_name}", ${arg_pos});'
    )
UNPACK_OPTIONS = CodeTemplate(
    'auto ${arg_name}_ = TensorOptions(${arg_name}).is_variable(false);')
DECLARE_GRAD_FN = CodeTemplate('std::shared_ptr<${op}> grad_fn;\n')
SETUP_DERIVATIVE = CodeTemplate(
    """if (compute_requires_grad( ${args_with_derivatives} )) {
  ${setup}
}
"""
    )
ASSIGN_GRAD_FN = CodeTemplate(
    """grad_fn = std::shared_ptr<${op}>(new ${op}(${op_ctor}), deleteFunction);
grad_fn->set_next_edges(collect_next_edges( ${args_with_derivatives} ));
"""
    )
CALL_VIA_TYPE = CodeTemplate(
    'TypeDefault::${method_prefix_derived}${api_name}(${type_method_args})')
CALL_VIA_DERIVED = CodeTemplate(
    'baseType->${method_prefix_derived}${base_name}(${unpacked_args})')
SET_HISTORY = CodeTemplate(
    '${fn}_history(${differentiable_outputs}, grad_fn);\n')
CONDITIONAL = CodeTemplate("""if (${cond}) {
  ${statements}
}
""")
RECORD_FUNCTION = CodeTemplate(
    'profiler::RecordFunction profiler("${name}", Function::peek_at_next_sequence_nr());'
    )
SELECT = CodeTemplate("""if (${cond}) {
  ${true}
} else {
  ${false}
}
""")
OP_NAME = CodeTemplate(
    'op_name = jit::Symbol::fromQualString("aten::${trace_name}");\n')
PRE_RECORD_TRACE = CodeTemplate(
    """torch::jit::Node* node = nullptr;
std::shared_ptr<jit::tracer::TracingState> tracer_state;
if (jit::tracer::isTracing()) {
  tracer_state = jit::tracer::getTracingState();
  at::Symbol op_name;
  ${set_op_name}
  node = tracer_state->graph->create(op_name, /*num_outputs=*/0);
  jit::tracer::recordSourceLocation(node);
  ${add_trace_inputs}
  tracer_state->graph->appendNode(node);
  ${inplace_guard}
  jit::tracer::setTracingState(nullptr);
}
"""
    )
INPLACE_GUARD = CodeTemplate(
    """jit::tracer::ensureUniqueIfOutOfPlaced("${name}", ${mutable_input});
"""
    )
ADD_TRACE_INPUT = CodeTemplate(
    'jit::tracer::addInputs(node, "${name}", ${input});')
POST_RECORD_TRACE = CodeTemplate(
    """if (tracer_state) {
  jit::tracer::setTracingState(std::move(tracer_state));
  ${add_trace_outputs}
}
"""
    )
FACTORY_FUNCTION_NAMES = None


def find_factory_functions(declarations):
    global FACTORY_FUNCTION_NAMES
    FACTORY_FUNCTION_NAMES = set()
    for declaration in declarations:
        if declaration['is_factory_method']:
            FACTORY_FUNCTION_NAMES.add(declaration['api_name'])


def should_trace(declaration):
    if any(arg['simple_type'] in {'Storage', 'Type'} for arg in declaration
        ['arguments']):
        return False
    if 'Tensor' not in declaration['return_type']:
        return False
    name = declaration['name']
    base_name = name[:-1] if declaration['inplace'] else name[:-4
        ] if name.endswith('_out') else name
    if base_name in DONT_RECORD_TRACE or name in DONT_RECORD_TRACE:
        return False
    return True


def is_out_overload(declaration):
    return declaration['api_name'].endswith('_out')


def format_postrecord_trace(declaration):
    if is_out_overload(declaration):
        output_names_outplace = [arg['name'] for arg in declaration[
            'arguments'] if arg.get('output', False)]
        output_names_inplace = [r['name'] for r in declaration['returns']]
        if output_names_outplace == output_names_inplace:
            outputs = ['jit::tracer::addOutput(node, {});'.format(n) for n in
                output_names_outplace]
            return POST_RECORD_TRACE.substitute(add_trace_outputs=outputs)
        local = {}
        local['cond'] = 'force_outplace'
        local['true'] = ['jit::tracer::addOutput(node, {});'.format(n) for
            n in output_names_outplace]
        local['false'] = ['jit::tracer::addOutput(node, {});'.format(n) for
            n in output_names_inplace]
        selection = SELECT.substitute(local)
        return POST_RECORD_TRACE.substitute(add_trace_outputs=selection)
    output_names = [r['name'] for r in declaration['returns']]
    outputs = ['jit::tracer::addOutput(node, {});'.format(n) for n in
        output_names]
    return POST_RECORD_TRACE.substitute(add_trace_outputs=outputs)


def format_trace_op_name(declaration):
    is_inplace = declaration['api_name'] != uninplace_api_name(declaration[
        'api_name'])
    if not is_inplace or is_out_overload(declaration):
        trace_name = uninplace_api_name(declaration['api_name'])
        trace_name = RENAME_TRACE.get(trace_name, trace_name)
        return OP_NAME.substitute(trace_name=trace_name)
    outplace_trace_name = uninplace_api_name(declaration['api_name'])
    inplace_trace_name = declaration['api_name']
    outplace_trace_name = RENAME_TRACE.get(outplace_trace_name,
        outplace_trace_name)
    inplace_trace_name = RENAME_TRACE.get(inplace_trace_name,
        inplace_trace_name)
    select_params = {}
    select_params['cond'] = 'tracer_state->force_outplace'
    select_params['true'] = OP_NAME.substitute(trace_name=outplace_trace_name)
    select_params['false'] = OP_NAME.substitute(trace_name=inplace_trace_name)
    return SELECT.substitute(select_params)


def format_trace_inputs(declaration):
    trace_inputs = declaration['arguments']
    if is_out_overload(declaration):
        out_input = trace_inputs[0]
        trace_inputs = trace_inputs[1:]
    trace_input_spec = [(i['name'], i['name']) for i in trace_inputs]
    trace_inputs = '\n'.join(ADD_TRACE_INPUT.substitute(name=name, input=
        value) for name, value in trace_input_spec)
    if is_out_overload(declaration):
        inplace = ADD_TRACE_INPUT.substitute(name=out_input['name'], input=
            out_input['name'])
        trace_name = uninplace_api_name(declaration['api_name'])
        has_factory_name = trace_name in FACTORY_FUNCTION_NAMES
        if has_factory_name:
            outplace = ADD_TRACE_INPUT.substitute(name='result', input=
                'result.options()')
        else:
            outplace = ''
        trace_inputs += '\n'
        trace_inputs += SELECT.substitute(cond=
            'tracer_state->force_outplace', true=outplace, false=inplace)
    return trace_inputs


def format_prerecord_trace(declaration):
    local = {}
    is_inplace = declaration['api_name'] != uninplace_api_name(declaration[
        'api_name'])
    local['set_op_name'] = format_trace_op_name(declaration)
    local['add_trace_inputs'] = format_trace_inputs(declaration)
    local['inplace_guard'] = ''
    if is_inplace:
        local['inplace_guard'] = INPLACE_GUARD.substitute(name=declaration[
            'api_name'], mutable_input=declaration['arguments'][0]['name'])
    return PRE_RECORD_TRACE.substitute(local)


def format_trace(declaration):
    return format_prerecord_trace(declaration), format_postrecord_trace(
        declaration)


def gen_variable_type(out, aten_declarations, template_path):
    """VariableType.h and VariableType.cpp body

    This is the at::Type subclass for differentiable tensors. The
    implementation of each function dispatches to the base tensor type to
    compute the output. The grad_fn is attached to differentiable functions.
    """
    find_factory_functions(aten_declarations)
    aten_declarations = list(sorted(aten_declarations, key=lambda decl:
        decl['name']))
    gen_variable_type_shard(out, aten_declarations, template_path, None, True)
    num_shards = 5
    shards = [[] for _ in range(num_shards)]
    for decl in aten_declarations:
        x = sum(ord(c) for c in decl['name']) % num_shards
        shards[x].append(decl)
    for i, shard in enumerate(shards):
        gen_variable_type_shard(out, shard, template_path, '_%d' % i, False)
    gen_variable_type_shard(out, aten_declarations, template_path,
        'Everything', False)


def gen_variable_type_shard(out, aten_declarations, template_path, suffix,
    header):
    VARIABLE_TYPE_H = CodeTemplate.from_file(template_path + '/VariableType.h')
    VARIABLE_TYPE_CPP = CodeTemplate.from_file(template_path +
        '/VariableType.cpp')
    type_declarations = []
    type_definitions = []
    for declaration in aten_declarations:
        if declaration['is_factory_method']:
            continue
        type_declarations.append(METHOD_DECLARATION.substitute(declaration))
        if declaration['name'] not in MANUAL_IMPLEMENTATIONS:
            type_definitions.append(emit_method_definition(declaration))
    env = {'type_derived_method_declarations': type_declarations,
        'type_derived_method_definitions': type_definitions}
    if header:
        write(out, 'VariableType.h', VARIABLE_TYPE_H, env)
    else:
        write(out, 'VariableType%s.cpp' % suffix, VARIABLE_TYPE_CPP, env)


def emit_method_definition(declaration):
    body = emit_body(declaration)
    return METHOD_DEFINITION.substitute(declaration, type_definition_body=body)


def emit_body(declaration):
    strategy = dispatch_strategy(declaration)
    arguments = declaration['arguments']
    returns = declaration['returns']
    func = declaration['derivative']
    name = declaration['name']
    inplace = declaration['inplace']
    is_out_fn = name.endswith('_out')
    modifies_arguments = inplace or is_out_fn
    returns_void = len(returns) == 1 and returns[0]['type'] == 'void'
    base_name = name[:-1] if inplace else name[:-4] if is_out_fn else name
    view_info = VIEW_FUNCTIONS.get(base_name, None)

    def is_differentiable(arg):
        if 'TensorOptions' in arg['type']:
            return False
        if 'Tensor' not in arg['type']:
            return False
        if arg['dynamic_type'] in {'IndexTensor', 'BoolTensor'}:
            return False
        return True
    inputs = [arg for arg in arguments if not arg.get('output', False)]
    differentiable_inputs = list(filter(is_differentiable, inputs))
    candidate_differentiable_outputs = list(filter(is_differentiable, returns))
    if func is not None and func.get('output_differentiability') is not None:
        differentiable_outputs = []
        output_differentiability = func.get('output_differentiability')
        for differentiable, output in zip(output_differentiability, returns):
            if differentiable:
                differentiable_outputs.append(output)
    elif uses_single_grad(func):
        differentiable_outputs = candidate_differentiable_outputs[:1]
    else:
        differentiable_outputs = candidate_differentiable_outputs
    requires_derivative = (base_name not in DONT_REQUIRE_DERIVATIVE and 
        name not in DONT_REQUIRE_DERIVATIVE and len(differentiable_inputs) >
        0 and len(differentiable_outputs) > 0 and strategy == 'use_derived')
    if func is not None and not requires_derivative:
        print('WARNING: derivative ignored for {}'.format(name), file=sys.
            stderr)

    def setup_derivative():
        args_with_derivatives = find_args_with_derivatives()
        env = {}
        env['args_with_derivatives'] = reference_args(args_with_derivatives)
        env['op'] = func['op'] if func is not None else 'NotImplemented'
        env['op_ctor'] = '' if func is not None else '"{}"'.format(declaration
            ['api_name'])
        if is_out_fn:
            setup = ['throw_error_out_requires_grad("{}");'.format(base_name)]
            body = []
            body.append(DECLARE_GRAD_FN.substitute(op='Function'))
            body.append(SETUP_DERIVATIVE.substitute(setup=setup,
                args_with_derivatives=reference_args(differentiable_inputs)))
            body.append(SETUP_DERIVATIVE.substitute(setup=setup,
                args_with_derivatives=reference_args(differentiable_outputs)))
            return body
        setup = []
        setup.extend(ASSIGN_GRAD_FN.substitute(env).split('\n'))
        if func is not None:
            setup.extend(save_variables(func['saved_inputs'], False))
            for arg in func['args_with_gradients']:
                if arg['type'] == 'TensorList':
                    setup.append('grad_fn->{}_size_ = {}.size();'.format(
                        arg['name'], arg['name']))
        body = []
        body.extend(emit_check_no_requires_grad(differentiable_inputs,
            args_with_derivatives))
        body.append(DECLARE_GRAD_FN.substitute(env))
        body.append(SETUP_DERIVATIVE.substitute(env, setup=setup))
        return body

    def find_args_with_derivatives():
        """Find arguments that have derivative definitions"""
        if func is None:
            return differentiable_inputs
        names = set(name for d in func['derivatives'] for name in d[
            'var_names'])
        differentiable = [arg for arg in differentiable_inputs if arg[
            'name'] in names]
        if len(differentiable) != len(names):
            missing = names - set(arg['name'] for arg in differentiable)
            raise RuntimeError('Missing arguments for derivatives: {} in {}'
                .format(missing, func['name']))
        return differentiable

    def emit_check_no_requires_grad(tensor_args, args_with_derivatives):
        """Checks that arguments without derivatives don't require grad"""
        body = []
        for arg in tensor_args:
            if arg in args_with_derivatives:
                continue
            name = arg['name']
            if name == 'output':
                continue
            if arg['dynamic_type'] in {'IndexTensor', 'BoolTensor'}:
                continue
            body.append('check_no_requires_grad({}, "{}");'.format(name, name))
        return body

    def save_variables(saved_variables, is_output):
        stmts = []
        for arg in saved_variables:
            name = arg['name']
            expr = arg.get('expr', arg['name'])
            if arg['type'] == 'Tensor' or is_output and arg['type'
                ] == 'Scalar':
                name += '_'
                var = arg['name']
                if var == 'self' and inplace:
                    var = 'self.clone()'
                    assert not is_output
                if inplace and is_output:
                    var = 'self'
                expr = 'SavedVariable({}, {})'.format(var, str(is_output).
                    lower())
            elif arg['type'] == 'TensorList':
                name += '_'
                expr = 'make_saved_variable_list({})'.format(arg['name'])
            elif arg['type'] == 'IntList':
                expr = expr + '.vec()'
            stmts.append('grad_fn->{} = {};'.format(name, expr))
        return stmts

    def reference_args(args):
        res = []
        for arg in args:
            if arg['type'] == 'SparseTensorRef':
                res.append('{}.tref'.format(arg['name']))
            else:
                res.append(arg['name'])
        return res

    def emit_record_trace(env):
        if not should_trace(declaration):
            return '', ''
        return format_trace(declaration)

    def declare_returned_variables():
        if modifies_arguments:
            return ''
        if len(declaration['returns']) == 1:
            return ''
        names = [(ret['type'] + ' ' + ret['name'] + ';') for ret in
            declaration['returns']]
        return '\n'.join(names)

    def wrap_output(call):
        if 'Tensor' not in declaration['return_type']:
            return call, []
        elif view_info is not None:
            differentiable_output_vars = {r['name'] for r in
                differentiable_outputs}
            tensor_output_vars = {r['name'] for r in returns if 'Tensor' in
                r['type']}
            if not isinstance(view_info, dict):
                if len(differentiable_output_vars) == len(tensor_output_vars):
                    return 'as_view({}, {}, true)'.format(view_info, call), []
                elif len(differentiable_output_vars) == 0:
                    return 'as_view({}, {}, false)'.format(view_info, call), []
                else:
                    base_name = view_info
                    view_info_dict = {}
                    for i, return_info in enumerate(returns):
                        if 'Tensor' in return_info['type']:
                            view_info_dict[i] = base_name
            else:
                view_info_dict = view_info

            def wrap_view_single(output_var, base_var):
                fmt = (
                    '{output_var} = as_view({base_var}, {output_var}, {is_differentiable});'
                    )
                if output_var in differentiable_output_vars:
                    is_differentiable = 'true'
                else:
                    is_differentiable = 'false'
                return fmt.format(output_var=output_var, base_var=base_var,
                    is_differentiable=is_differentiable)
            extra_wrapping_stmts = []
            for output_idx, return_info in enumerate(returns):
                if 'Tensor' not in return_info['type']:
                    assert output_idx not in view_info_dict, 'Can not wrap non-Tensor output as a view'
                    continue
                output_var = return_info['name']
                if output_idx in view_info_dict:
                    stmt = wrap_view_single(output_var, view_info_dict[
                        output_idx])
                elif 'Tensor' in return_info['type']:
                    stmt = '{output_var} = as_variable({output_var});'.format(
                        output_var=output_var)
                extra_wrapping_stmts.append(stmt)
            return call, extra_wrapping_stmts
        else:
            return 'as_variable({})'.format(call), []

    def emit_call(env):
        combined = nested_dict(env, declaration)
        extra_wrapping_stmts = []
        if strategy == 'use_derived':
            call = CALL_VIA_DERIVED.substitute(combined)
            if not modifies_arguments:
                call, extra_wrapping_stmts = wrap_output(call)
        else:
            call = CALL_VIA_TYPE.substitute(declaration)
        if not modifies_arguments and not returns_void:
            call = '{} = {}'.format(tie_return_values(), call)
        call = call + ';'
        for stmt in extra_wrapping_stmts:
            call += '\n' + stmt
        return call

    def tie_return_values():
        if len(declaration['returns']) == 1:
            return 'auto {}'.format(declaration['returns'][0]['name'])
        names = [ret['name'] for ret in declaration['returns']]
        return 'std::tie({})'.format(', '.join(names))

    def get_return_value():
        if inplace:
            return 'self'
        if is_out_fn:
            return_names = [arg['name'] for arg in arguments if arg.get(
                'output', False)]
            if len(return_names) == 1:
                return return_names[0]
            return 'std::forward_as_tuple({})'.format(', '.join(return_names))
        returns = declaration['returns']
        if len(returns) == 1:
            return returns[0]['name']
        moved = ['std::move({})'.format(r['name']) for r in returns]
        return 'std::make_tuple({})'.format(', '.join(moved))

    def emit_history():
        fn = 'rebase' if modifies_arguments and view_info is None else 'set'
        output_names = [r['name'] for r in differentiable_outputs]
        outs = CodeTemplate('flatten_tensor_args( ${outs} )').substitute(outs
            =output_names)
        return SET_HISTORY.substitute(fn=fn, differentiable_outputs=outs)

    def emit_save_outputs():
        if is_out_fn:
            return ''
        func = declaration['derivative']
        if func is not None:
            stmts = save_variables(func['saved_outputs'], True)
            if len(stmts) == 0:
                return ''
            return CONDITIONAL.substitute(cond='grad_fn', statements=stmts)
        return ''

    def emit_check_inplace():
        if not inplace:
            return []
        return ['check_inplace({});'.format(arg['name']) for arg in
            differentiable_outputs]

    def emit_increment_version():
        if not modifies_arguments:
            return []
        return ['increment_version({});'.format(arg['name']) for arg in
            differentiable_outputs]
    env = {}
    combined = nested_dict(env, declaration)
    body = []
    if base_name not in DONT_PROFILE:
        body.append(RECORD_FUNCTION.substitute(combined))
    if strategy != 'use_type':
        body.extend(unpack_args(env, declaration))
    if requires_derivative:
        body.extend(emit_check_inplace())
        body.extend(setup_derivative())
    body.append(declare_returned_variables())
    pre_record_trace, post_record_trace = emit_record_trace(env)
    body.append(pre_record_trace)
    body.append(emit_call(env))
    if requires_derivative:
        body.extend(emit_increment_version())
        body.append(emit_history())
    body.append(post_record_trace)
    if requires_derivative:
        body.append(emit_save_outputs())
    if not returns_void:
        body.append('return {};'.format(get_return_value()))
    return body


def unpack_args(env, declaration):

    def requires_unpack(arg):
        return 'Tensor' in arg['dynamic_type']
    body = []
    unpacked_args = []
    for i, arg in enumerate(declaration['arguments']):
        if not requires_unpack(arg):
            unpacked_args.append(arg['name'])
            continue
        dynamic_type = arg['dynamic_type']
        if 'TensorOptions' not in dynamic_type:
            is_nullable = arg.get('is_nullable', False)
            ref = not is_nullable and dynamic_type not in ['TensorList',
                'SparseTensorRef']
            suffix = '_opt' if is_nullable else ''
            body.append(UNPACK_TENSOR.substitute(arg_name=arg['name'],
                arg_pos=i, suffix=suffix, ref='&' if ref else ''))
        else:
            body.append(UNPACK_OPTIONS.substitute(arg_name=arg['name']))
        unpacked_args.append(arg['name'] + '_')
    env['unpacked_args'] = unpacked_args
    return body


def dispatch_strategy(declaration):
    """How are we going to call the underlying implementation of a
    declaration?  There are two strategies:

        - use_derived: we want to call the implementation on CPUDoubleType
          (or a similar, derived Type instance).  Because these derived
          instances deal in Tensors, not Variables (it's a completely different
          object, so it doesn't dispatch back to VariableType), code on
          this dispatch path needs to wrap/unwrap tensors.  If the
          derived implementation takes and returns tensors, the
          implementation is usually differentiable (although we also use
          the derived dispatch path for non-differentiable functions
          that we still want to dispatch on the derived Type instance;
          e.g., size())

        - use_type: we want to call the implementation on Type, because
          it is implemented concretely, and the functions it invokes will
          get dispatched back to VariableType (which will ensure that they
          are differentiable.)
    """
    if declaration['abstract'] or declaration['requires_tensor'
        ] or declaration['derivative'] is not None:
        return 'use_derived'
    else:
        return 'use_type'
