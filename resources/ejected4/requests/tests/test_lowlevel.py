import annotations
var.upper = annotations.annot_tests_test_lowlevel_var_upper
import pytest
import threading
import requests
from tests.testserver.server import Server, consume_socket_content
from .utils import override_environ


def test_chunked_upload():
    """can safely send generators"""
    close_server = threading.Event()
    server = Server.basic_response_server(wait_to_close_event=close_server)
    data = iter([b'a', b'b', b'c'])
    with server as (host, port):
        url = 'http://{}:{}/'.format(host, port)
        r = requests.post(url, data=data, stream=True)
        close_server.set()
    assert r.status_code == 200
    assert r.request.headers['Transfer-Encoding'] == 'chunked'


def test_digestauth_401_count_reset_on_redirect():
    """Ensure we correctly reset num_401_calls after a successful digest auth,
    followed by a 302 redirect to another digest auth prompt.

    See https://github.com/requests/requests/issues/1979.
    """
    text_401 = (
        b'HTTP/1.1 401 UNAUTHORIZED\r\nContent-Length: 0\r\nWWW-Authenticate: Digest nonce="6bf5d6e4da1ce66918800195d6b9130d", opaque="372825293d1c26955496c80ed6426e9e", realm="me@kennethreitz.com", qop=auth\r\n\r\n'
        )
    text_302 = (
        b'HTTP/1.1 302 FOUND\r\nContent-Length: 0\r\nLocation: /\r\n\r\n')
    text_200 = b'HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n'
    expected_digest = (
        b'Authorization: Digest username="user", realm="me@kennethreitz.com", nonce="6bf5d6e4da1ce66918800195d6b9130d", uri="/"'
        )
    auth = requests.auth.HTTPDigestAuth('user', 'pass')

    def digest_response_handler(sock):
        request_content = consume_socket_content(sock, timeout=0.5)
        assert request_content.startswith(b'GET / HTTP/1.1')
        sock.send(text_401)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert expected_digest in request_content
        sock.send(text_302)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert b'Authorization:' not in request_content
        sock.send(text_401)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert expected_digest in request_content
        sock.send(text_200)
        return request_content
    close_server = threading.Event()
    server = Server(digest_response_handler, wait_to_close_event=close_server)
    with server as (host, port):
        url = 'http://{}:{}/'.format(host, port)
        r = requests.get(url, auth=auth)
        assert r.status_code == 200
        assert 'Authorization' in r.request.headers
        assert r.request.headers['Authorization'].startswith('Digest ')
        assert r.history[0].status_code == 302
        close_server.set()


def test_digestauth_401_only_sent_once():
    """Ensure we correctly respond to a 401 challenge once, and then
    stop responding if challenged again.
    """
    text_401 = (
        b'HTTP/1.1 401 UNAUTHORIZED\r\nContent-Length: 0\r\nWWW-Authenticate: Digest nonce="6bf5d6e4da1ce66918800195d6b9130d", opaque="372825293d1c26955496c80ed6426e9e", realm="me@kennethreitz.com", qop=auth\r\n\r\n'
        )
    expected_digest = (
        b'Authorization: Digest username="user", realm="me@kennethreitz.com", nonce="6bf5d6e4da1ce66918800195d6b9130d", uri="/"'
        )
    auth = requests.auth.HTTPDigestAuth('user', 'pass')

    def digest_failed_response_handler(sock):
        request_content = consume_socket_content(sock, timeout=0.5)
        assert request_content.startswith(b'GET / HTTP/1.1')
        sock.send(text_401)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert expected_digest in request_content
        sock.send(text_401)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert request_content == b''
        return request_content
    close_server = threading.Event()
    server = Server(digest_failed_response_handler, wait_to_close_event=
        close_server)
    with server as (host, port):
        url = 'http://{}:{}/'.format(host, port)
        r = requests.get(url, auth=auth)
        assert r.status_code == 401
        assert r.history[0].status_code == 401
        close_server.set()


def test_digestauth_only_on_4xx():
    """Ensure we only send digestauth on 4xx challenges.

    See https://github.com/requests/requests/issues/3772.
    """
    text_200_chal = (
        b'HTTP/1.1 200 OK\r\nContent-Length: 0\r\nWWW-Authenticate: Digest nonce="6bf5d6e4da1ce66918800195d6b9130d", opaque="372825293d1c26955496c80ed6426e9e", realm="me@kennethreitz.com", qop=auth\r\n\r\n'
        )
    auth = requests.auth.HTTPDigestAuth('user', 'pass')

    def digest_response_handler(sock):
        request_content = consume_socket_content(sock, timeout=0.5)
        assert request_content.startswith(b'GET / HTTP/1.1')
        sock.send(text_200_chal)
        request_content = consume_socket_content(sock, timeout=0.5)
        assert request_content == b''
        return request_content
    close_server = threading.Event()
    server = Server(digest_response_handler, wait_to_close_event=close_server)
    with server as (host, port):
        url = 'http://{}:{}/'.format(host, port)
        r = requests.get(url, auth=auth)
        assert r.status_code == 200
        assert len(r.history) == 0
        close_server.set()


_schemes_by_var_prefix = [('http', ['http']), ('https', ['https']), ('all',
    ['http', 'https'])]
_proxy_combos = []
for prefix, schemes in _schemes_by_var_prefix:
    for scheme in schemes:
        _proxy_combos.append(('{}_proxy'.format(prefix), scheme))
_proxy_combos += [(var.upper(), scheme) for var, scheme in _proxy_combos]


@pytest.mark.parametrize('var,scheme', _proxy_combos)
def test_use_proxy_from_environment(httpbin, var, scheme):
    url = '{}://httpbin.org'.format(scheme)
    fake_proxy = Server()
    with fake_proxy as (host, port):
        proxy_url = 'socks5://{}:{}'.format(host, port)
        kwargs = {var: proxy_url}
        with override_environ(**kwargs):
            with pytest.raises(requests.exceptions.ConnectionError):
                requests.get(url)
        assert len(fake_proxy.handler_results) == 1
        assert len(fake_proxy.handler_results[0]) > 0


def test_redirect_rfc1808_to_non_ascii_location():
    path = 'š'
    expected_path = b'%C5%A1'
    redirect_request = []

    def redirect_resp_handler(sock):
        consume_socket_content(sock, timeout=0.5)
        location = '//{}:{}/{}'.format(host, port, path)
        sock.send(
            b'HTTP/1.1 301 Moved Permanently\r\nContent-Length: 0\r\nLocation: '
             + location.encode('utf8') + b'\r\n\r\n')
        redirect_request.append(consume_socket_content(sock, timeout=0.5))
        sock.send(b'HTTP/1.1 200 OK\r\n\r\n')
    close_server = threading.Event()
    server = Server(redirect_resp_handler, wait_to_close_event=close_server)
    with server as (host, port):
        url = 'http://{}:{}'.format(host, port)
        r = requests.get(url=url, allow_redirects=True)
        assert r.status_code == 200
        assert len(r.history) == 1
        assert r.history[0].status_code == 301
        assert redirect_request[0].startswith(b'GET /' + expected_path +
            b' HTTP/1.1')
        assert r.url == '{}/{}'.format(url, expected_path.decode('ascii'))
        close_server.set()


def test_fragment_not_sent_with_request():
    """Verify that the fragment portion of a URI isn't sent to the server."""

    def response_handler(sock):
        req = consume_socket_content(sock, timeout=0.5)
        sock.send(b'HTTP/1.1 200 OK\r\nContent-Length: ' + bytes(len(req)) +
            b'\r\n\r\n' + req)
    close_server = threading.Event()
    server = Server(response_handler, wait_to_close_event=close_server)
    with server as (host, port):
        url = 'http://{}:{}/path/to/thing/#view=edit&token=hunter2'.format(host
            , port)
        r = requests.get(url)
        raw_request = r.content
        assert r.status_code == 200
        headers, body = raw_request.split(b'\r\n\r\n', 1)
        status_line, headers = headers.split(b'\r\n', 1)
        assert status_line == b'GET /path/to/thing/ HTTP/1.1'
        for frag in (b'view', b'edit', b'token', b'hunter2'):
            assert frag not in headers
            assert frag not in body
        close_server.set()


def test_fragment_update_on_redirect():
    """Verify we only append previous fragment if one doesn't exist on new
    location. If a new fragment is encountered in a Location header, it should
    be added to all subsequent requests.
    """

    def response_handler(sock):
        consume_socket_content(sock, timeout=0.5)
        sock.send(
            b'HTTP/1.1 302 FOUND\r\nContent-Length: 0\r\nLocation: /get#relevant-section\r\n\r\n'
            )
        consume_socket_content(sock, timeout=0.5)
        sock.send(
            b'HTTP/1.1 302 FOUND\r\nContent-Length: 0\r\nLocation: /final-url/\r\n\r\n'
            )
        consume_socket_content(sock, timeout=0.5)
        sock.send(b'HTTP/1.1 200 OK\r\n\r\n')
    close_server = threading.Event()
    server = Server(response_handler, wait_to_close_event=close_server)
    with server as (host, port):
        url = 'http://{}:{}/path/to/thing/#view=edit&token=hunter2'.format(host
            , port)
        r = requests.get(url)
        raw_request = r.content
        assert r.status_code == 200
        assert len(r.history) == 2
        assert r.history[0].request.url == url
        assert r.history[1
            ].request.url == 'http://{}:{}/get#relevant-section'.format(host,
            port)
        assert r.url == 'http://{}:{}/final-url/#relevant-section'.format(host,
            port)
        close_server.set()
