import annotations
match.group = annotations.annot_docs_utils_linkfix_match_group
"""

Linkfix - a companion to sphinx's linkcheck builder.

Uses the linkcheck's output file to fix links in docs.

Originally created for this issue:
https://github.com/scrapy/scrapy/issues/606

Author: dufferzafar
"""
import re
_filename = None
_contents = None
line_re = re.compile('(.*)\\:\\d+\\:\\s\\[(.*)\\]\\s(?:(.*)\\sto\\s(.*)|(.*))')
try:
    with open('build/linkcheck/output.txt') as out:
        output_lines = out.readlines()
except IOError:
    print('linkcheck output not found; please run linkcheck first.')
    exit(1)
for line in output_lines:
    match = re.match(line_re, line)
    if match:
        newfilename = match.group(1)
        errortype = match.group(2)
        if errortype.lower() in ['broken', 'local']:
            print('Not Fixed: ' + line)
        else:
            if newfilename != _filename:
                if _filename:
                    with open(_filename, 'w') as _file:
                        _file.write(_contents)
                _filename = newfilename
                with open(_filename) as _file:
                    _contents = _file.read()
            _contents = _contents.replace(match.group(3), match.group(4))
    else:
        print('Not Understood: ' + line)
