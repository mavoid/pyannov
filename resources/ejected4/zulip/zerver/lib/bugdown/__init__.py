import annotations
Union = annotations.annot_zerver_lib_bugdown___init___Union
from typing import Any, Callable, Dict, Iterable, List, NamedTuple, Optional, Set, Tuple, TypeVar, Union, cast
from mypy_extensions import TypedDict
from typing.re import Match, Pattern
import markdown
import logging
import traceback
import urllib
import re
import os
import html
import platform
import time
import functools
import ujson
import xml.etree.cElementTree as etree
from xml.etree.cElementTree import Element, SubElement
from collections import deque, defaultdict
import requests
from django.core import mail
from django.conf import settings
from django.db.models import Q
from markdown.extensions import codehilite, nl2br, tables
from zerver.lib.bugdown import fenced_code
from zerver.lib.bugdown.fenced_code import FENCE_RE
from zerver.lib.camo import get_camo_url
from zerver.lib.emoji import translate_emoticons, emoticon_regex
from zerver.lib.mention import possible_mentions, possible_user_group_mentions, extract_user_group
from zerver.lib.url_encoding import encode_stream
from zerver.lib.thumbnail import is_thumbor_enabled, user_uploads_or_external
from zerver.lib.timeout import timeout, TimeoutExpired
from zerver.lib.cache import cache_with_key, NotFoundInCache
from zerver.lib.url_preview import preview as link_preview
from zerver.models import all_realm_filters, get_active_streams, MAX_MESSAGE_LENGTH, Message, Realm, RealmFilter, realm_filters_for_realm, UserProfile, UserGroup, UserGroupMembership
import zerver.lib.mention as mention
from zerver.lib.tex import render_tex
from zerver.lib.exceptions import BugdownRenderingException
FullNameInfo = TypedDict('FullNameInfo', {'id': int, 'email': str,
    'full_name': str})
DbData = Dict[str, Any]
version = 1
_T = TypeVar('_T')
ElementStringNone = Union[Element, Optional[str]]
AVATAR_REGEX = '!avatar\\((?P<email>[^)]*)\\)'
GRAVATAR_REGEX = '!gravatar\\((?P<email>[^)]*)\\)'
EMOJI_REGEX = '(?P<syntax>:[\\w\\-\\+]+:)'


def verbose_compile(pattern: str) ->Any:
    return re.compile('^(.*?)%s(.*?)$' % pattern, re.DOTALL | re.UNICODE |
        re.VERBOSE)


STREAM_LINK_REGEX = """
                     (?<![^\\s'"\\(,:<])            # Start after whitespace or specified chars
                     \\#\\*\\*                       # and after hash sign followed by double asterisks
                         (?P<stream_name>[^\\*]+)  # stream name can contain anything
                     \\*\\*                         # ends by double asterisks
                    """
LINK_REGEX = None


def get_web_link_regex() ->str:
    global LINK_REGEX
    if LINK_REGEX is None:
        tlds = '|'.join(list_of_tlds())
        inner_paren_contents = '[^\\s()\\"]*'
        paren_group = """
                        [^\\s()\\"]*?            # Containing characters that won't end the URL
                        (?: \\( %s \\)           # and more characters in matched parens
                            [^\\s()\\"]*?        # followed by more characters
                        )*                     # zero-or-more sets of paired parens
                       """
        nested_paren_chunk = paren_group
        for i in range(6):
            nested_paren_chunk = nested_paren_chunk % (paren_group,)
        nested_paren_chunk = nested_paren_chunk % (inner_paren_contents,)
        file_links = ('| (?:file://(/[^/ ]*)+/?)' if settings.
            ENABLE_FILE_LINKS else '')
        regex = (
            """
            (?<![^\\s'"\\(,:<])    # Start after whitespace or specified chars
                                 # (Double-negative lookbehind to allow start-of-string)
            (?P<url>             # Main group
                (?:(?:           # Domain part
                    https?://[\\w.:@-]+?   # If it has a protocol, anything goes.
                   |(?:                   # Or, if not, be more strict to avoid false-positives
                        (?:[\\w-]+\\.)+     # One or more domain components, separated by dots
                        (?:%s)            # TLDs (filled in via format from tlds-alpha-by-domain.txt)
                    )
                )
                (?:/             # A path, beginning with /
                    %s           # zero-to-6 sets of paired parens
                )?)              # Path is optional
                | (?:[\\w.-]+\\@[\\w.-]+\\.[\\w]+) # Email is separate, since it can't have a path
                %s               # File path start with file:///, enable by setting ENABLE_FILE_LINKS=True
                | (?:bitcoin:[13][a-km-zA-HJ-NP-Z1-9]{25,34})  # Bitcoin address pattern, see https://mokagio.github.io/tech-journal/2014/11/21/regex-bitcoin.html
            )
            (?=                            # URL must be followed by (not included in group)
                [!:;\\?\\),\\.\\'\\"\\>]*         # Optional punctuation characters
                (?:\\Z|\\s)                  # followed by whitespace or end of string
            )
            """
             % (tlds, nested_paren_chunk, file_links))
        LINK_REGEX = verbose_compile(regex)
    return LINK_REGEX


def clear_state_for_testing() ->None:
    global LINK_REGEX
    LINK_REGEX = None


bugdown_logger = logging.getLogger()


def rewrite_local_links_to_relative(db_data: Optional[DbData], link: str
    ) ->str:
    """ If the link points to a local destination we can just switch to that
    instead of opening a new tab. """
    if db_data:
        realm_uri_prefix = db_data['realm_uri'] + '/'
        if link.startswith(realm_uri_prefix):
            return link[len(realm_uri_prefix):]
    return link


def url_embed_preview_enabled_for_realm(message: Optional[Message]=None,
    realm: Optional[Realm]=None) ->bool:
    if not settings.INLINE_URL_EMBED_PREVIEW:
        return False
    if realm is None:
        if message is not None:
            realm = message.get_realm()
    if realm is None:
        return True
    return realm.inline_url_embed_preview


def image_preview_enabled_for_realm(message: Optional[Message]=None, realm:
    Optional[Realm]=None) ->bool:
    if not settings.INLINE_IMAGE_PREVIEW:
        return False
    if realm is None:
        if message is not None:
            realm = message.get_realm()
    if realm is None:
        return True
    return realm.inline_image_preview


def list_of_tlds() ->List[str]:
    blacklist = ['PY\n', 'MD\n']
    tlds_file = os.path.join(os.path.dirname(__file__),
        'tlds-alpha-by-domain.txt')
    tlds = [tld.lower().strip() for tld in open(tlds_file, 'r') if tld not in
        blacklist and not tld[0].startswith('#')]
    tlds.sort(key=len, reverse=True)
    return tlds


def walk_tree(root: Element, processor: Callable[[Element], Optional[_T]],
    stop_after_first: bool=False) ->List[_T]:
    results = []
    queue = deque([root])
    while queue:
        currElement = queue.popleft()
        for child in currElement.getchildren():
            if child.getchildren():
                queue.append(child)
            result = processor(child)
            if result is not None:
                results.append(result)
                if stop_after_first:
                    return results
    return results


ElementFamily = NamedTuple('ElementFamily', [('grandparent', Optional[
    Element]), ('parent', Element), ('child', Element)])
ResultWithFamily = NamedTuple('ResultWithFamily', [('family', ElementFamily
    ), ('result', Any)])
ElementPair = NamedTuple('ElementPair', [('parent', Optional[Element]), (
    'value', Element)])


def walk_tree_with_family(root: Element, processor: Callable[[Element],
    Optional[_T]]) ->List[ResultWithFamily]:
    results = []
    queue = deque([ElementPair(parent=None, value=root)])
    while queue:
        currElementPair = queue.popleft()
        for child in currElementPair.value.getchildren():
            if child.getchildren():
                queue.append(ElementPair(parent=currElementPair, value=child))
            result = processor(child)
            if result is not None:
                if currElementPair.parent is not None:
                    grandparent_element = cast(ElementPair, currElementPair
                        .parent)
                    grandparent = grandparent_element.value
                else:
                    grandparent = None
                family = ElementFamily(grandparent=grandparent, parent=
                    currElementPair.value, child=child)
                results.append(ResultWithFamily(family=family, result=result))
    return results


def add_a(root: Element, url: str, link: str, title: Optional[str]=None,
    desc: Optional[str]=None, class_attr: str='message_inline_image',
    data_id: Optional[str]=None, insertion_index: Optional[int]=None,
    already_thumbnailed: Optional[bool]=False) ->None:
    title = title if title is not None else url_filename(link)
    title = title if title else ''
    desc = desc if desc is not None else ''
    if insertion_index is not None:
        div = markdown.util.etree.Element('div')
        root.insert(insertion_index, div)
    else:
        div = markdown.util.etree.SubElement(root, 'div')
    div.set('class', class_attr)
    a = markdown.util.etree.SubElement(div, 'a')
    a.set('href', link)
    a.set('target', '_blank')
    a.set('title', title)
    if data_id is not None:
        a.set('data-id', data_id)
    img = markdown.util.etree.SubElement(a, 'img')
    if is_thumbor_enabled(
        ) and not already_thumbnailed and user_uploads_or_external(url):
        url = url.lstrip('/')
        img.set('src', '/thumbnail?url={0}&size=thumbnail'.format(urllib.
            parse.quote(url, safe='')))
        img.set('data-src-fullsize', '/thumbnail?url={0}&size=full'.format(
            urllib.parse.quote(url, safe='')))
    else:
        img.set('src', url)
    if class_attr == 'message_inline_ref':
        summary_div = markdown.util.etree.SubElement(div, 'div')
        title_div = markdown.util.etree.SubElement(summary_div, 'div')
        title_div.set('class', 'message_inline_image_title')
        title_div.text = title
        desc_div = markdown.util.etree.SubElement(summary_div, 'desc')
        desc_div.set('class', 'message_inline_image_desc')


def add_embed(root: Element, link: str, extracted_data: Dict[str, Any]) ->None:
    container = markdown.util.etree.SubElement(root, 'div')
    container.set('class', 'message_embed')
    img_link = extracted_data.get('image')
    if img_link:
        parsed_img_link = urllib.parse.urlparse(img_link)
        if not parsed_img_link.netloc:
            parsed_url = urllib.parse.urlparse(link)
            domain = '{url.scheme}://{url.netloc}/'.format(url=parsed_url)
            img_link = urllib.parse.urljoin(domain, img_link)
        img = markdown.util.etree.SubElement(container, 'a')
        img.set('style', 'background-image: url(' + img_link + ')')
        img.set('href', link)
        img.set('target', '_blank')
        img.set('class', 'message_embed_image')
    data_container = markdown.util.etree.SubElement(container, 'div')
    data_container.set('class', 'data-container')
    title = extracted_data.get('title')
    if title:
        title_elm = markdown.util.etree.SubElement(data_container, 'div')
        title_elm.set('class', 'message_embed_title')
        a = markdown.util.etree.SubElement(title_elm, 'a')
        a.set('href', link)
        a.set('target', '_blank')
        a.set('title', title)
        a.text = title
    description = extracted_data.get('description')
    if description:
        description_elm = markdown.util.etree.SubElement(data_container, 'div')
        description_elm.set('class', 'message_embed_description')
        description_elm.text = description


@cache_with_key(lambda tweet_id: tweet_id, cache_name='database',
    with_statsd_key='tweet_data')
def fetch_tweet_data(tweet_id: str) ->Optional[Dict[str, Any]]:
    if settings.TEST_SUITE:
        from . import testing_mocks
        res = testing_mocks.twitter(tweet_id)
    else:
        creds = {'consumer_key': settings.TWITTER_CONSUMER_KEY,
            'consumer_secret': settings.TWITTER_CONSUMER_SECRET,
            'access_token_key': settings.TWITTER_ACCESS_TOKEN_KEY,
            'access_token_secret': settings.TWITTER_ACCESS_TOKEN_SECRET}
        if not all(creds.values()):
            return None
        import twitter
        try:
            api = twitter.Api(tweet_mode='extended', **creds)
            tweet = timeout(3, api.GetStatus, tweet_id)
            res = tweet.AsDict()
        except AttributeError:
            bugdown_logger.error(
                'Unable to load twitter api, you may have the wrong library installed, see https://github.com/zulip/zulip/issues/86'
                )
            return None
        except TimeoutExpired:
            raise
        except twitter.TwitterError as e:
            t = e.args[0]
            if len(t) == 1 and 'code' in t[0] and t[0]['code'] == 34:
                return None
            elif len(t) == 1 and 'code' in t[0] and (t[0]['code'] == 88 or 
                t[0]['code'] == 130):
                raise
            else:
                bugdown_logger.error(traceback.format_exc())
                return None
    return res


HEAD_START_RE = re.compile('^head[ >]')
HEAD_END_RE = re.compile('^/head[ >]')
META_START_RE = re.compile('^meta[ >]')
META_END_RE = re.compile('^/meta[ >]')


def fetch_open_graph_image(url: str) ->Optional[Dict[str, Any]]:
    in_head = False
    last_closed = True
    head = []
    try:
        content = requests.get(url, timeout=1).text
    except Exception:
        return None
    for part in content.split('<'):
        if not in_head and HEAD_START_RE.match(part):
            in_head = True
            head.append('<head>')
        elif in_head and HEAD_END_RE.match(part):
            in_head = False
            if not last_closed:
                last_closed = True
                head.append('</meta>')
            head.append('</head>')
            break
        elif in_head and META_START_RE.match(part):
            if not last_closed:
                head.append('</meta>')
                last_closed = True
            head.append('<')
            head.append(part)
            if '/>' not in part:
                last_closed = False
        elif in_head and META_END_RE.match(part):
            head.append('<')
            head.append(part)
            last_closed = True
    try:
        doc = etree.fromstring(''.join(head))
    except etree.ParseError:
        return None
    og_image = doc.find('meta[@property="og:image"]')
    og_title = doc.find('meta[@property="og:title"]')
    og_desc = doc.find('meta[@property="og:description"]')
    title = None
    desc = None
    if og_image is not None:
        image = og_image.get('content')
    else:
        return None
    if og_title is not None:
        title = og_title.get('content')
    if og_desc is not None:
        desc = og_desc.get('content')
    return {'image': image, 'title': title, 'desc': desc}


def get_tweet_id(url: str) ->Optional[str]:
    parsed_url = urllib.parse.urlparse(url)
    if not (parsed_url.netloc == 'twitter.com' or parsed_url.netloc.
        endswith('.twitter.com')):
        return None
    to_match = parsed_url.path
    if parsed_url.path == '/' and len(parsed_url.fragment) > 5:
        to_match = parsed_url.fragment
    tweet_id_match = re.match(
        '^!?/.*?/status(es)?/(?P<tweetid>\\d{10,30})(/photo/[0-9])?/?$',
        to_match)
    if not tweet_id_match:
        return None
    return tweet_id_match.group('tweetid')


class InlineHttpsProcessor(markdown.treeprocessors.Treeprocessor):

    def run(self, root: Element) ->None:
        found_imgs = walk_tree(root, lambda e: e if e.tag == 'img' else None)
        for img in found_imgs:
            url = img.get('src')
            if not url.startswith('http://'):
                continue
            img.set('src', get_camo_url(url))


class BacktickPattern(markdown.inlinepatterns.Pattern):
    """ Return a `<code>` element containing the matching text. """

    def __init__(self, pattern: str) ->None:
        markdown.inlinepatterns.Pattern.__init__(self, pattern)
        self.ESCAPED_BSLASH = '%s%s%s' % (markdown.util.STX, ord('\\'),
            markdown.util.ETX)
        self.tag = 'code'

    def handleMatch(self, m: Match[str]) ->Union[str, Element]:
        if m.group(4):
            el = markdown.util.etree.Element(self.tag)
            el.text = markdown.util.AtomicString(m.group(4))
            return el
        else:
            return m.group(2).replace('\\\\', self.ESCAPED_BSLASH)


class InlineInterestingLinkProcessor(markdown.treeprocessors.Treeprocessor):
    TWITTER_MAX_IMAGE_HEIGHT = 400
    TWITTER_MAX_TO_PREVIEW = 3
    INLINE_PREVIEW_LIMIT_PER_MESSAGE = 5

    def __init__(self, md: markdown.Markdown, bugdown: 'Bugdown') ->None:
        self.bugdown = bugdown
        markdown.treeprocessors.Treeprocessor.__init__(self, md)

    def get_actual_image_url(self, url: str) ->str:
        parsed_url = urllib.parse.urlparse(url)
        if parsed_url.netloc == 'github.com' or parsed_url.netloc.endswith(
            '.github.com'):
            split_path = parsed_url.path.split('/')
            if len(split_path) > 3 and split_path[3] == 'blob':
                return urllib.parse.urljoin('https://raw.githubusercontent.com'
                    , '/'.join(split_path[0:3] + split_path[4:]))
        return url

    def image_preview_enabled(self) ->bool:
        return image_preview_enabled_for_realm(self.markdown.zulip_message,
            self.markdown.zulip_realm)

    def is_image(self, url: str) ->bool:
        if not self.image_preview_enabled():
            return False
        parsed_url = urllib.parse.urlparse(url)
        for ext in ['.bmp', '.gif', '.jpg', 'jpeg', '.png', '.webp']:
            if parsed_url.path.lower().endswith(ext):
                return True
        return False

    def dropbox_image(self, url: str) ->Optional[Dict[str, Any]]:
        parsed_url = urllib.parse.urlparse(url)
        if parsed_url.netloc == 'dropbox.com' or parsed_url.netloc.endswith(
            '.dropbox.com'):
            is_album = parsed_url.path.startswith('/sc/'
                ) or parsed_url.path.startswith('/photos/')
            if not (parsed_url.path.startswith('/s/') or parsed_url.path.
                startswith('/sh/') or is_album):
                return None
            image_info = fetch_open_graph_image(url)
            is_image = is_album or self.is_image(url)
            if is_album or not is_image:
                if image_info is None:
                    return None
                image_info['is_image'] = is_image
                return image_info
            if image_info is None:
                image_info = dict()
            image_info['is_image'] = True
            parsed_url_list = list(parsed_url)
            parsed_url_list[4] = 'dl=1'
            image_info['image'] = urllib.parse.urlunparse(parsed_url_list)
            return image_info
        return None

    def youtube_id(self, url: str) ->Optional[str]:
        if not self.image_preview_enabled():
            return None
        youtube_re = (
            '^((?:https?://)?(?:youtu\\.be/|(?:\\w+\\.)?youtube(?:-nocookie)?\\.com/)'
             +
            '(?:(?:(?:v|embed)/)|(?:(?:watch(?:_popup)?(?:\\.php)?)?(?:\\?|#!?)(?:.+&)?v=)))'
             + '?([0-9A-Za-z_-]+)(?(1).+)?$')
        match = re.match(youtube_re, url)
        if match is None:
            return None
        return match.group(2)

    def youtube_image(self, url: str) ->Optional[str]:
        yt_id = self.youtube_id(url)
        if yt_id is not None:
            return 'https://i.ytimg.com/vi/%s/default.jpg' % (yt_id,)
        return None

    def vimeo_id(self, url: str) ->Optional[str]:
        if not self.image_preview_enabled():
            return None
        vimeo_re = ('^((http|https)?:\\/\\/(www\\.)?vimeo.com\\/' +
            '(?:channels\\/(?:\\w+\\/)?|groups\\/' +
            '([^\\/]*)\\/videos\\/|)(\\d+)(?:|\\/\\?))$')
        match = re.match(vimeo_re, url)
        if match is None:
            return None
        return match.group(5)

    def vimeo_title(self, extracted_data: Dict[str, Any]) ->Optional[str]:
        title = extracted_data.get('title')
        if title is not None:
            return 'Vimeo - {}'.format(title)
        return None

    def twitter_text(self, text: str, urls: List[Dict[str, str]],
        user_mentions: List[Dict[str, Any]], media: List[Dict[str, Any]]
        ) ->Element:
        """
        Use data from the twitter API to turn links, mentions and media into A
        tags. Also convert unicode emojis to images.

        This works by using the urls, user_mentions and media data from
        the twitter API and searching for unicode emojis in the text using
        `unicode_emoji_regex`.

        The first step is finding the locations of the URLs, mentions, media and
        emoji in the text. For each match we build a dictionary with type, the start
        location, end location, the URL to link to, and the text(codepoint and title
        in case of emojis) to be used in the link(image in case of emojis).

        Next we sort the matches by start location. And for each we add the
        text from the end of the last link to the start of the current link to
        the output. The text needs to added to the text attribute of the first
        node (the P tag) or the tail the last link created.

        Finally we add any remaining text to the last node.
        """
        to_process = []
        for url_data in urls:
            short_url = url_data['url']
            full_url = url_data['expanded_url']
            for match in re.finditer(re.escape(short_url), text, re.IGNORECASE
                ):
                to_process.append({'type': 'url', 'start': match.start(),
                    'end': match.end(), 'url': short_url, 'text': full_url})
        for user_mention in user_mentions:
            screen_name = user_mention['screen_name']
            mention_string = '@' + screen_name
            for match in re.finditer(re.escape(mention_string), text, re.
                IGNORECASE):
                to_process.append({'type': 'mention', 'start': match.start(
                    ), 'end': match.end(), 'url': 'https://twitter.com/' +
                    urllib.parse.quote(screen_name), 'text': mention_string})
        for media_item in media:
            short_url = media_item['url']
            expanded_url = media_item['expanded_url']
            for match in re.finditer(re.escape(short_url), text, re.IGNORECASE
                ):
                to_process.append({'type': 'media', 'start': match.start(),
                    'end': match.end(), 'url': short_url, 'text': expanded_url}
                    )
        for match in re.finditer(unicode_emoji_regex, text, re.IGNORECASE):
            orig_syntax = match.group('syntax')
            codepoint = unicode_emoji_to_codepoint(orig_syntax)
            if codepoint in codepoint_to_name:
                display_string = ':' + codepoint_to_name[codepoint] + ':'
                to_process.append({'type': 'emoji', 'start': match.start(),
                    'end': match.end(), 'codepoint': codepoint, 'title':
                    display_string})
        to_process.sort(key=lambda x: x['start'])
        p = current_node = markdown.util.etree.Element('p')

        def set_text(text: str) ->None:
            """
            Helper to set the text or the tail of the current_node
            """
            if current_node == p:
                current_node.text = text
            else:
                current_node.tail = text
        db_data = self.markdown.zulip_db_data
        current_index = 0
        for item in to_process:
            if item['start'] < current_index:
                continue
            set_text(text[current_index:item['start']])
            current_index = item['end']
            if item['type'] != 'emoji':
                current_node = elem = url_to_a(db_data, item['url'], item[
                    'text'])
            else:
                current_node = elem = make_emoji(item['codepoint'], item[
                    'title'])
            p.append(elem)
        set_text(text[current_index:])
        return p

    def twitter_link(self, url: str) ->Optional[Element]:
        tweet_id = get_tweet_id(url)
        if tweet_id is None:
            return None
        try:
            res = fetch_tweet_data(tweet_id)
            if res is None:
                return None
            user = res['user']
            tweet = markdown.util.etree.Element('div')
            tweet.set('class', 'twitter-tweet')
            img_a = markdown.util.etree.SubElement(tweet, 'a')
            img_a.set('href', url)
            img_a.set('target', '_blank')
            profile_img = markdown.util.etree.SubElement(img_a, 'img')
            profile_img.set('class', 'twitter-avatar')
            image_url = user.get('profile_image_url_https', user[
                'profile_image_url'])
            profile_img.set('src', image_url)
            text = html.unescape(res['full_text'])
            urls = res.get('urls', [])
            user_mentions = res.get('user_mentions', [])
            media = res.get('media', [])
            p = self.twitter_text(text, urls, user_mentions, media)
            tweet.append(p)
            span = markdown.util.etree.SubElement(tweet, 'span')
            span.text = '- %s (@%s)' % (user['name'], user['screen_name'])
            for media_item in media:
                if media_item['type'] != 'photo':
                    continue
                size_name_tuples = list(media_item['sizes'].items())
                size_name_tuples.sort(reverse=True, key=lambda x: x[1]['h'])
                for size_name, size in size_name_tuples:
                    if size['h'] < self.TWITTER_MAX_IMAGE_HEIGHT:
                        break
                media_url = '%s:%s' % (media_item['media_url_https'], size_name
                    )
                img_div = markdown.util.etree.SubElement(tweet, 'div')
                img_div.set('class', 'twitter-image')
                img_a = markdown.util.etree.SubElement(img_div, 'a')
                img_a.set('href', media_item['url'])
                img_a.set('target', '_blank')
                img_a.set('title', media_item['url'])
                img = markdown.util.etree.SubElement(img_a, 'img')
                img.set('src', media_url)
            return tweet
        except Exception:
            bugdown_logger.warning(traceback.format_exc())
            return None

    def get_url_data(self, e: Element) ->Optional[Tuple[str, str]]:
        if e.tag == 'a':
            if e.text is not None:
                return e.get('href'), e.text
            return e.get('href'), e.get('href')
        return None

    def handle_image_inlining(self, root: Element, found_url: ResultWithFamily
        ) ->None:
        grandparent = found_url.family.grandparent
        parent = found_url.family.parent
        ahref_element = found_url.family.child
        url, text = found_url.result
        actual_url = self.get_actual_image_url(url)
        url_eq_text = url == text
        if parent.tag == 'li':
            add_a(parent, self.get_actual_image_url(url), url, title=text)
            if not parent.text and not ahref_element.tail and url_eq_text:
                parent.remove(ahref_element)
        elif parent.tag == 'p':
            parent_index = None
            for index, uncle in enumerate(grandparent.getchildren()):
                if uncle is parent:
                    parent_index = index
                    break
            if parent_index is not None:
                ins_index = self.find_proper_insertion_index(grandparent,
                    parent, parent_index)
                add_a(grandparent, actual_url, url, title=text,
                    insertion_index=ins_index)
            else:
                add_a(grandparent, actual_url, url, title=text)
            if len(parent.getchildren()) == 1 and (not parent.text or 
                parent.text == '\n'
                ) and not ahref_element.tail and url_eq_text:
                grandparent.remove(parent)
        else:
            add_a(root, actual_url, url, title=text)

    def find_proper_insertion_index(self, grandparent: Element, parent:
        Element, parent_index_in_grandparent: int) ->int:
        uncles = grandparent.getchildren()
        parent_links = [ele.attrib['href'] for ele in parent.iter(tag='a')]
        insertion_index = parent_index_in_grandparent
        while True:
            insertion_index += 1
            if insertion_index >= len(uncles):
                return insertion_index
            uncle = uncles[insertion_index]
            inline_image_classes = ['message_inline_image',
                'message_inline_ref']
            if uncle.tag != 'div' or 'class' not in uncle.keys(
                ) or uncle.attrib['class'] not in inline_image_classes:
                return insertion_index
            uncle_link = list(uncle.iter(tag='a'))[0].attrib['href']
            if uncle_link not in parent_links:
                return insertion_index

    def is_absolute_url(self, url: str) ->bool:
        return bool(urllib.parse.urlparse(url).netloc)

    def run(self, root: Element) ->None:
        found_urls = walk_tree_with_family(root, self.get_url_data)
        if len(found_urls) == 0 or len(found_urls
            ) > self.INLINE_PREVIEW_LIMIT_PER_MESSAGE:
            return
        rendered_tweet_count = 0
        for found_url in found_urls:
            url, text = found_url.result
            if not self.is_absolute_url(url):
                if self.is_image(url):
                    self.handle_image_inlining(root, found_url)
                continue
            dropbox_image = self.dropbox_image(url)
            if dropbox_image is not None:
                class_attr = 'message_inline_ref'
                is_image = dropbox_image['is_image']
                if is_image:
                    class_attr = 'message_inline_image'
                add_a(root, dropbox_image['image'], url, title=
                    dropbox_image.get('title', ''), desc=dropbox_image.get(
                    'desc', ''), class_attr=class_attr, already_thumbnailed
                    =True)
                continue
            if self.is_image(url):
                self.handle_image_inlining(root, found_url)
                continue
            if get_tweet_id(url) is not None:
                if rendered_tweet_count >= self.TWITTER_MAX_TO_PREVIEW:
                    continue
                twitter_data = self.twitter_link(url)
                if twitter_data is None:
                    continue
                rendered_tweet_count += 1
                div = markdown.util.etree.SubElement(root, 'div')
                div.set('class', 'inline-preview-twitter')
                div.insert(0, twitter_data)
                continue
            youtube = self.youtube_image(url)
            if youtube is not None:
                yt_id = self.youtube_id(url)
                add_a(root, youtube, url, None, None,
                    'youtube-video message_inline_image', yt_id,
                    already_thumbnailed=True)
                continue
            db_data = self.markdown.zulip_db_data
            if db_data and db_data['sent_by_bot']:
                continue
            if not url_embed_preview_enabled_for_realm(self.markdown.
                zulip_message, self.markdown.zulip_realm):
                continue
            try:
                extracted_data = link_preview.link_embed_data_from_cache(url)
            except NotFoundInCache:
                self.markdown.zulip_message.links_for_preview.add(url)
                continue
            if extracted_data:
                vm_id = self.vimeo_id(url)
                if vm_id is not None:
                    vimeo_image = extracted_data.get('image')
                    vimeo_title = self.vimeo_title(extracted_data)
                    if vimeo_image is not None:
                        add_a(root, vimeo_image, url, vimeo_title, None,
                            'vimeo-video message_inline_image', vm_id,
                            already_thumbnailed=True)
                    if vimeo_title is not None:
                        found_url.family.child.text = vimeo_title
                else:
                    add_embed(root, url, extracted_data)


class Avatar(markdown.inlinepatterns.Pattern):

    def handleMatch(self, match: Match[str]) ->Optional[Element]:
        img = markdown.util.etree.Element('img')
        email_address = match.group('email')
        email = email_address.strip().lower()
        profile_id = None
        db_data = self.markdown.zulip_db_data
        if db_data is not None:
            user_dict = db_data['email_info'].get(email)
            if user_dict is not None:
                profile_id = user_dict['id']
        img.set('class', 'message_body_gravatar')
        img.set('src', '/avatar/{0}?s=30'.format(profile_id or email))
        img.set('title', email)
        img.set('alt', email)
        return img


def possible_avatar_emails(content: str) ->Set[str]:
    emails = set()
    for regex in [AVATAR_REGEX, GRAVATAR_REGEX]:
        matches = re.findall(regex, content)
        for email in matches:
            if email:
                emails.add(email)
    return emails


path_to_name_to_codepoint = os.path.join(settings.STATIC_ROOT, 'generated',
    'emoji', 'name_to_codepoint.json')
with open(path_to_name_to_codepoint) as name_to_codepoint_file:
    name_to_codepoint = ujson.load(name_to_codepoint_file)
path_to_codepoint_to_name = os.path.join(settings.STATIC_ROOT, 'generated',
    'emoji', 'codepoint_to_name.json')
with open(path_to_codepoint_to_name) as codepoint_to_name_file:
    codepoint_to_name = ujson.load(codepoint_to_name_file)
unicode_emoji_regex = (
    '(?P<syntax>[🄀-🙏🚀-\U0001f6ff\U0001f900-\U0001f9ff\u2000-\u206f⌀-➿⤀-⥿⬀-\u2bff\u3000-〿㈀-\u32ff])'
    )


def make_emoji(codepoint: str, display_string: str) ->Element:
    title = display_string[1:-1].replace('_', ' ')
    span = markdown.util.etree.Element('span')
    span.set('class', 'emoji emoji-%s' % (codepoint,))
    span.set('title', title)
    span.text = display_string
    return span


def make_realm_emoji(src: str, display_string: str) ->Element:
    elt = markdown.util.etree.Element('img')
    elt.set('src', src)
    elt.set('class', 'emoji')
    elt.set('alt', display_string)
    elt.set('title', display_string[1:-1].replace('_', ' '))
    return elt


def unicode_emoji_to_codepoint(unicode_emoji: str) ->str:
    codepoint = hex(ord(unicode_emoji))[2:]
    while len(codepoint) < 4:
        codepoint = '0' + codepoint
    return codepoint


class EmoticonTranslation(markdown.inlinepatterns.Pattern):
    """ Translates emoticons like `:)` into emoji like `:smile:`. """

    def handleMatch(self, match: Match[str]) ->Optional[Element]:
        db_data = self.markdown.zulip_db_data
        if db_data is None or not db_data['translate_emoticons']:
            return None
        emoticon = match.group('emoticon')
        translated = translate_emoticons(emoticon)
        name = translated[1:-1]
        return make_emoji(name_to_codepoint[name], translated)


class UnicodeEmoji(markdown.inlinepatterns.Pattern):

    def handleMatch(self, match: Match[str]) ->Optional[Element]:
        orig_syntax = match.group('syntax')
        codepoint = unicode_emoji_to_codepoint(orig_syntax)
        if codepoint in codepoint_to_name:
            display_string = ':' + codepoint_to_name[codepoint] + ':'
            return make_emoji(codepoint, display_string)
        else:
            return None


class Emoji(markdown.inlinepatterns.Pattern):

    def handleMatch(self, match: Match[str]) ->Optional[Element]:
        orig_syntax = match.group('syntax')
        name = orig_syntax[1:-1]
        active_realm_emoji = {}
        db_data = self.markdown.zulip_db_data
        if db_data is not None:
            active_realm_emoji = db_data['active_realm_emoji']
        if self.markdown.zulip_message and name in active_realm_emoji:
            return make_realm_emoji(active_realm_emoji[name]['source_url'],
                orig_syntax)
        elif name == 'zulip':
            return make_realm_emoji(
                '/static/generated/emoji/images/emoji/unicode/zulip.png',
                orig_syntax)
        elif name in name_to_codepoint:
            return make_emoji(name_to_codepoint[name], orig_syntax)
        else:
            return None


def content_has_emoji_syntax(content: str) ->bool:
    return re.search(EMOJI_REGEX, content) is not None


class ModalLink(markdown.inlinepatterns.Pattern):
    """
    A pattern that allows including in-app modal links in messages.
    """

    def handleMatch(self, match: Match[str]) ->Element:
        relative_url = match.group('relative_url')
        text = match.group('text')
        a_tag = markdown.util.etree.Element('a')
        a_tag.set('href', relative_url)
        a_tag.set('title', relative_url)
        a_tag.text = text
        return a_tag


class Tex(markdown.inlinepatterns.Pattern):

    def handleMatch(self, match: Match[str]) ->Element:
        rendered = render_tex(match.group('body'), is_inline=True)
        if rendered is not None:
            return etree.fromstring(rendered.encode('utf-8'))
        else:
            span = markdown.util.etree.Element('span')
            span.set('class', 'tex-error')
            span.text = '$$' + match.group('body') + '$$'
            return span


upload_title_re = re.compile(
    '^(https?://[^/]*)?(/user_uploads/\\d+)(/[^/]*)?/[^/]*/(?P<filename>[^/]*)$'
    )


def url_filename(url: str) ->str:
    """Extract the filename if a URL is an uploaded file, or return the original URL"""
    match = upload_title_re.match(url)
    if match:
        return match.group('filename')
    else:
        return url


def fixup_link(link: markdown.util.etree.Element, target_blank: bool=True
    ) ->None:
    """Set certain attributes we want on every link."""
    if target_blank:
        link.set('target', '_blank')
    link.set('title', url_filename(link.get('href')))


def sanitize_url(url: str) ->Optional[str]:
    """
    Sanitize a url against xss attacks.
    See the docstring on markdown.inlinepatterns.LinkPattern.sanitize_url.
    """
    try:
        parts = urllib.parse.urlparse(url.replace(' ', '%20'))
        scheme, netloc, path, params, query, fragment = parts
    except ValueError:
        return ''
    if scheme == '' and netloc == '' and '@' in path:
        scheme = 'mailto'
    elif scheme == '' and netloc == '' and len(path) > 0 and path[0] == '/':
        return urllib.parse.urlunparse(('', '', path, params, query, fragment))
    elif (scheme, netloc, path, params, query) == ('', '', '', '', '') and len(
        fragment) > 0:
        return urllib.parse.urlunparse(('', '', '', '', '', fragment))
    if not scheme:
        return sanitize_url('http://' + url)
    locless_schemes = ['mailto', 'news', 'file', 'bitcoin']
    if netloc == '' and scheme not in locless_schemes:
        return None
    if scheme not in ('http', 'https', 'ftp', 'mailto', 'file', 'bitcoin'):
        return None
    return urllib.parse.urlunparse((scheme, netloc, path, params, query,
        fragment))


def url_to_a(db_data: Optional[DbData], url: str, text: Optional[str]=None
    ) ->Union[Element, str]:
    a = markdown.util.etree.Element('a')
    href = sanitize_url(url)
    target_blank = True
    if href is None:
        return url
    if text is None:
        text = markdown.util.AtomicString(url)
    href = rewrite_local_links_to_relative(db_data, href)
    target_blank = not href.startswith('#narrow') and not href.startswith(
        'mailto:')
    a.set('href', href)
    a.text = text
    fixup_link(a, target_blank)
    return a


class VerbosePattern(markdown.inlinepatterns.Pattern):

    def __init__(self, compiled_re: Pattern, md: markdown.Markdown) ->None:
        markdown.inlinepatterns.Pattern.__init__(self, ' ', md)
        self.compiled_re = compiled_re


class AutoLink(VerbosePattern):

    def handleMatch(self, match: Match[str]) ->ElementStringNone:
        url = match.group('url')
        db_data = self.markdown.zulip_db_data
        return url_to_a(db_data, url)


class UListProcessor(markdown.blockprocessors.UListProcessor):
    """ Process unordered list blocks.

        Based on markdown.blockprocessors.UListProcessor, but does not accept
        '+' or '-' as a bullet character."""
    TAG = 'ul'
    RE = re.compile('^[ ]{0,3}[*][ ]+(.*)')

    def __init__(self, parser: Any) ->None:
        parser.markdown.tab_length = 2
        super().__init__(parser)
        parser.markdown.tab_length = 4


class ListIndentProcessor(markdown.blockprocessors.ListIndentProcessor):
    """ Process unordered list blocks.

        Based on markdown.blockprocessors.ListIndentProcessor, but with 2-space indent
    """

    def __init__(self, parser: Any) ->None:
        parser.markdown.tab_length = 2
        super().__init__(parser)
        parser.markdown.tab_length = 4


class BugdownUListPreprocessor(markdown.preprocessors.Preprocessor):
    """ Allows unordered list blocks that come directly after a
        paragraph to be rendered as an unordered list

        Detects paragraphs that have a matching list item that comes
        directly after a line of text, and inserts a newline between
        to satisfy Markdown"""
    LI_RE = re.compile('^[ ]{0,3}[*][ ]+(.*)', re.MULTILINE)
    HANGING_ULIST_RE = re.compile('^.+\\n([ ]{0,3}[*][ ]+.*)', re.MULTILINE)

    def run(self, lines: List[str]) ->List[str]:
        """ Insert a newline between a paragraph and ulist if missing """
        inserts = 0
        fence = None
        copy = lines[:]
        for i in range(len(lines) - 1):
            m = FENCE_RE.match(lines[i])
            if not fence and m:
                fence = m.group('fence')
            elif fence and m and fence == m.group('fence'):
                fence = None
            if not fence and lines[i] and self.LI_RE.match(lines[i + 1]
                ) and not self.LI_RE.match(lines[i]):
                copy.insert(i + inserts + 1, '')
                inserts += 1
        return copy


class AutoNumberOListPreprocessor(markdown.preprocessors.Preprocessor):
    """ Finds a sequence of lines numbered by the same number"""
    RE = re.compile('^([ ]*)(\\d+)\\.[ ]+(.*)')
    TAB_LENGTH = 2

    def run(self, lines: List[str]) ->List[str]:
        new_lines = []
        current_list = []
        current_indent = 0
        for line in lines:
            m = self.RE.match(line)
            is_next_item = m and current_list and current_indent == len(m.
                group(1)) // self.TAB_LENGTH
            if not is_next_item:
                new_lines.extend(self.renumber(current_list))
                current_list = []
            if not m:
                new_lines.append(line)
            elif is_next_item:
                current_list.append(m)
            else:
                current_list = [m]
                current_indent = len(m.group(1)) // self.TAB_LENGTH
        new_lines.extend(self.renumber(current_list))
        return new_lines

    def renumber(self, mlist: List[Match[str]]) ->List[str]:
        if not mlist:
            return []
        start_number = int(mlist[0].group(2))
        change_numbers = True
        for m in mlist:
            if int(m.group(2)) != start_number:
                change_numbers = False
                break
        lines = []
        counter = start_number
        for m in mlist:
            number = str(counter) if change_numbers else m.group(2)
            lines.append('%s%s. %s' % (m.group(1), number, m.group(3)))
            counter += 1
        return lines


class LinkPattern(markdown.inlinepatterns.Pattern):
    """ Return a link element from the given match. """

    def handleMatch(self, m: Match[str]) ->Optional[Element]:
        href = m.group(9)
        if not href:
            return None
        if href[0] == '<':
            href = href[1:-1]
        href = sanitize_url(self.unescape(href.strip()))
        if href is None:
            return None
        db_data = self.markdown.zulip_db_data
        href = rewrite_local_links_to_relative(db_data, href)
        el = markdown.util.etree.Element('a')
        el.text = m.group(2)
        el.set('href', href)
        fixup_link(el, target_blank=href[:1] != '#')
        return el


def prepare_realm_pattern(source: str) ->str:
    """ Augment a realm filter so it only matches after start-of-string,
    whitespace, or opening delimiters, won't match if there are word
    characters directly after, and saves what was matched as "name". """
    return '(?<![^\\s\'"\\(,:<])(?P<name>' + source + ')(?!\\w)'


class RealmFilterPattern(markdown.inlinepatterns.Pattern):
    """ Applied a given realm filter to the input """

    def __init__(self, source_pattern: str, format_string: str,
        markdown_instance: Optional[markdown.Markdown]=None) ->None:
        self.pattern = prepare_realm_pattern(source_pattern)
        self.format_string = format_string
        markdown.inlinepatterns.Pattern.__init__(self, self.pattern,
            markdown_instance)

    def handleMatch(self, m: Match[str]) ->Union[Element, str]:
        db_data = self.markdown.zulip_db_data
        return url_to_a(db_data, self.format_string % m.groupdict(), m.
            group('name'))


class UserMentionPattern(markdown.inlinepatterns.Pattern):

    def handleMatch(self, m: Match[str]) ->Optional[Element]:
        match = m.group(2)
        db_data = self.markdown.zulip_db_data
        if self.markdown.zulip_message and db_data is not None:
            if match.startswith('**') and match.endswith('**'):
                name = match[2:-2]
            else:
                return None
            wildcard = mention.user_mention_matches_wildcard(name)
            id_syntax_match = re.match('.+\\|(?P<user_id>\\d+)$', name)
            if id_syntax_match:
                id = id_syntax_match.group('user_id')
                user = db_data['mention_data'].get_user_by_id(id)
            else:
                user = db_data['mention_data'].get_user_by_name(name)
            if wildcard:
                self.markdown.zulip_message.mentions_wildcard = True
                user_id = '*'
            elif user:
                self.markdown.zulip_message.mentions_user_ids.add(user['id'])
                name = user['full_name']
                user_id = str(user['id'])
            else:
                return None
            el = markdown.util.etree.Element('span')
            el.set('class', 'user-mention')
            el.set('data-user-id', user_id)
            el.text = '@%s' % (name,)
            return el
        return None


class UserGroupMentionPattern(markdown.inlinepatterns.Pattern):

    def handleMatch(self, m: Match[str]) ->Optional[Element]:
        match = m.group(2)
        db_data = self.markdown.zulip_db_data
        if self.markdown.zulip_message and db_data is not None:
            name = extract_user_group(match)
            user_group = db_data['mention_data'].get_user_group(name)
            if user_group:
                self.markdown.zulip_message.mentions_user_group_ids.add(
                    user_group.id)
                name = user_group.name
                user_group_id = str(user_group.id)
            else:
                return None
            el = markdown.util.etree.Element('span')
            el.set('class', 'user-group-mention')
            el.set('data-user-group-id', user_group_id)
            el.text = '@%s' % (name,)
            return el
        return None


class StreamPattern(VerbosePattern):

    def find_stream_by_name(self, name: Match[str]) ->Optional[Dict[str, Any]]:
        db_data = self.markdown.zulip_db_data
        if db_data is None:
            return None
        stream = db_data['stream_names'].get(name)
        return stream

    def handleMatch(self, m: Match[str]) ->Optional[Element]:
        name = m.group('stream_name')
        if self.markdown.zulip_message:
            stream = self.find_stream_by_name(name)
            if stream is None:
                return None
            el = markdown.util.etree.Element('a')
            el.set('class', 'stream')
            el.set('data-stream-id', str(stream['id']))
            stream_url = encode_stream(stream['id'], name)
            el.set('href', '/#narrow/stream/{stream_url}'.format(stream_url
                =stream_url))
            el.text = '#{stream_name}'.format(stream_name=name)
            return el
        return None


def possible_linked_stream_names(content: str) ->Set[str]:
    matches = re.findall(STREAM_LINK_REGEX, content, re.VERBOSE)
    return set(matches)


class AlertWordsNotificationProcessor(markdown.preprocessors.Preprocessor):

    def run(self, lines: Iterable[str]) ->Iterable[str]:
        db_data = self.markdown.zulip_db_data
        if self.markdown.zulip_message and db_data is not None:
            realm_words = db_data['possible_words']
            content = '\n'.join(lines).lower()
            allowed_before_punctuation = '|'.join(['\\s', '^',
                '[\\(\\".,\\\';\\[\\*`>]'])
            allowed_after_punctuation = '|'.join(['\\s', '$',
                '[\\)\\"\\?:.,\\\';\\]!\\*`]'])
            for word in realm_words:
                escaped = re.escape(word.lower())
                match_re = re.compile('(?:%s)%s(?:%s)' % (
                    allowed_before_punctuation, escaped,
                    allowed_after_punctuation))
                if re.search(match_re, content):
                    self.markdown.zulip_message.alert_words.add(word)
        return lines


class AtomicLinkPattern(LinkPattern):

    def handleMatch(self, m: Match[str]) ->Optional[Element]:
        ret = LinkPattern.handleMatch(self, m)
        if ret is None:
            return None
        if not isinstance(ret, str):
            ret.text = markdown.util.AtomicString(ret.text)
        return ret


DEFAULT_BUGDOWN_KEY = -1
ZEPHYR_MIRROR_BUGDOWN_KEY = -2


class Bugdown(markdown.Extension):

    def __init__(self, *args: Any, **kwargs: Union[bool, int, List[Any]]
        ) ->None:
        self.config = {'realm_filters': [kwargs['realm_filters'], 
            'Realm-specific filters for realm_filters_key %s' % (kwargs[
            'realm'],)], 'realm': [kwargs['realm'], 'Realm id'],
            'code_block_processor_disabled': [kwargs[
            'code_block_processor_disabled'], 'Disabled for email gateway']}
        super().__init__(*args, **kwargs)

    def extendMarkdown(self, md: markdown.Markdown, md_globals: Dict[str, Any]
        ) ->None:
        del md.preprocessors['reference']
        if self.getConfig('code_block_processor_disabled'):
            del md.parser.blockprocessors['code']
        for k in ('image_link', 'image_reference', 'automail', 'autolink',
            'link', 'reference', 'short_reference', 'escape', 'strong_em',
            'emphasis', 'emphasis2', 'linebreak', 'strong', 'backtick'):
            del md.inlinePatterns[k]
        try:
            del md.inlinePatterns['linebreak2']
        except Exception:
            pass
        self.extend_alert_words(md)
        self.extend_text_formatting(md)
        self.extend_block_formatting(md)
        self.extend_avatars(md)
        self.extend_modal_links(md)
        self.extend_mentions(md)
        self.extend_stream_links(md)
        self.extend_emojis(md)
        self.extend_misc(md)

    def extend_alert_words(self, md: markdown.Markdown) ->None:
        md.preprocessors.add('custom_text_notifications',
            AlertWordsNotificationProcessor(md), '_end')

    def extend_text_formatting(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('backtick', BacktickPattern(
            '(?:(?<!\\\\)((?:\\\\{2})+)(?=`+)|(?<!\\\\)(`+)(.+?)(?<!`)\\3(?!`))'
            ), '_begin')
        md.inlinePatterns.add('strong_em', markdown.inlinepatterns.
            DoubleTagPattern(
            '(\\*\\*\\*)(?!\\s+)([^\\*^\\n]+)(?<!\\s)\\*\\*\\*',
            'strong,em'), '>backtick')
        md.inlinePatterns.add('strong', markdown.inlinepatterns.
            SimpleTagPattern('(\\*\\*)([^\\n]+?)\\2', 'strong'), '>not_strong')
        md.inlinePatterns.add('del', markdown.inlinepatterns.
            SimpleTagPattern('(?<!~)(\\~\\~)([^~\\n]+?)(\\~\\~)(?!~)',
            'del'), '>strong')
        md.inlinePatterns.add('emphasis', markdown.inlinepatterns.
            SimpleTagPattern('(\\*)(?!\\s+)([^\\*^\\n]+)(?<!\\s)\\*', 'em'),
            '>strong')

    def extend_block_formatting(self, md: markdown.Markdown) ->None:
        for k in ('hashheader', 'setextheader', 'olist', 'ulist', 'indent'):
            del md.parser.blockprocessors[k]
        md.parser.blockprocessors.add('ulist', UListProcessor(md.parser), '>hr'
            )
        md.parser.blockprocessors.add('indent', ListIndentProcessor(md.
            parser), '<ulist')
        md.parser.blockprocessors['quote'].RE = re.compile(
            '(^|\\n)(?!(?:[ ]{0,3}>\\s*(?:$|\\n))*(?:$|\\n))[ ]{0,3}>[ ]?(.*)')

    def extend_avatars(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('avatar', Avatar(AVATAR_REGEX, md), '>backtick')
        md.inlinePatterns.add('gravatar', Avatar(GRAVATAR_REGEX, md),
            '>backtick')

    def extend_modal_links(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('modal_link', ModalLink(
            '!modal_link\\((?P<relative_url>[^)]*), (?P<text>[^)]*)\\)'),
            '>avatar')

    def extend_mentions(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('usermention', UserMentionPattern(mention.
            find_mentions, md), '>backtick')
        md.inlinePatterns.add('usergroupmention', UserGroupMentionPattern(
            mention.user_group_mentions, md), '>backtick')

    def extend_stream_links(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('stream', StreamPattern(verbose_compile(
            STREAM_LINK_REGEX), md), '>backtick')

    def extend_emojis(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('tex', Tex(
            '\\B(?<!\\$)\\$\\$(?P<body>[^\\n_$](\\\\\\$|[^$\\n])*)\\$\\$(?!\\$)\\B'
            ), '>backtick')
        md.inlinePatterns.add('emoji', Emoji(EMOJI_REGEX, md), '<nl')
        md.inlinePatterns.add('translate_emoticons', EmoticonTranslation(
            emoticon_regex, md), '>emoji')
        md.inlinePatterns.add('unicodeemoji', UnicodeEmoji(
            unicode_emoji_regex), '_end')

    def extend_misc(self, md: markdown.Markdown) ->None:
        md.inlinePatterns.add('link', AtomicLinkPattern(markdown.
            inlinepatterns.LINK_RE, md), '>avatar')
        for pattern, format_string, id in self.getConfig('realm_filters'):
            md.inlinePatterns.add('realm_filters/%s' % (pattern,),
                RealmFilterPattern(pattern, format_string, md), '>link')
        md.inlinePatterns.add('autolink', AutoLink(get_web_link_regex(), md
            ), '>link')
        md.preprocessors.add('hanging_ulists', BugdownUListPreprocessor(md),
            '_begin')
        md.preprocessors.add('auto_number_olist',
            AutoNumberOListPreprocessor(md), '_begin')
        md.treeprocessors.add('inline_interesting_links',
            InlineInterestingLinkProcessor(md, self), '_end')
        if settings.CAMO_URI:
            md.treeprocessors.add('rewrite_to_https', InlineHttpsProcessor(
                md), '_end')
        if self.getConfig('realm') == ZEPHYR_MIRROR_BUGDOWN_KEY:
            for k in list(md.inlinePatterns.keys()):
                if k not in ['autolink']:
                    del md.inlinePatterns[k]
            for k in list(md.treeprocessors.keys()):
                if k not in ['inline_interesting_links', 'inline',
                    'rewrite_to_https']:
                    del md.treeprocessors[k]
            for k in list(md.preprocessors.keys()):
                if k not in ['custom_text_notifications']:
                    del md.preprocessors[k]
            for k in list(md.parser.blockprocessors.keys()):
                if k not in ['paragraph']:
                    del md.parser.blockprocessors[k]


md_engines = {}
realm_filter_data = {}


class EscapeHtml(markdown.Extension):

    def extendMarkdown(self, md: markdown.Markdown, md_globals: Dict[str, Any]
        ) ->None:
        del md.preprocessors['html_block']
        del md.inlinePatterns['html']


def make_md_engine(realm_filters_key: int, email_gateway: bool) ->None:
    md_engine_key = realm_filters_key, email_gateway
    if md_engine_key in md_engines:
        del md_engines[md_engine_key]
    realm_filters = realm_filter_data[realm_filters_key]
    md_engines[md_engine_key] = build_engine(realm_filters=realm_filters,
        realm_filters_key=realm_filters_key, email_gateway=email_gateway)


def build_engine(realm_filters: List[Tuple[str, str, int]],
    realm_filters_key: int, email_gateway: bool) ->markdown.Markdown:
    engine = markdown.Markdown(output_format='html', extensions=[nl2br.
        makeExtension(), tables.makeExtension(), codehilite.makeExtension(
        linenums=False, guess_lang=False), fenced_code.makeExtension(),
        EscapeHtml(), Bugdown(realm_filters=realm_filters, realm=
        realm_filters_key, code_block_processor_disabled=email_gateway)])
    return engine


def topic_links(realm_filters_key: int, topic_name: str) ->List[str]:
    matches = []
    realm_filters = realm_filters_for_realm(realm_filters_key)
    for realm_filter in realm_filters:
        pattern = prepare_realm_pattern(realm_filter[0])
        for m in re.finditer(pattern, topic_name):
            matches += [realm_filter[1] % m.groupdict()]
    return matches


def maybe_update_markdown_engines(realm_filters_key: Optional[int],
    email_gateway: bool) ->None:
    global realm_filter_data
    if realm_filters_key is None:
        all_filters = all_realm_filters()
        all_filters[DEFAULT_BUGDOWN_KEY] = []
        for realm_filters_key, filters in all_filters.items():
            realm_filter_data[realm_filters_key] = filters
            make_md_engine(realm_filters_key, email_gateway)
        realm_filter_data[ZEPHYR_MIRROR_BUGDOWN_KEY] = []
        make_md_engine(ZEPHYR_MIRROR_BUGDOWN_KEY, False)
    else:
        realm_filters = realm_filters_for_realm(realm_filters_key)
        if realm_filters_key not in realm_filter_data or realm_filter_data[
            realm_filters_key] != realm_filters:
            realm_filter_data[realm_filters_key] = realm_filters
            for email_gateway_flag in [True, False]:
                if (realm_filters_key, email_gateway_flag) in md_engines:
                    make_md_engine(realm_filters_key, email_gateway_flag)
        if (realm_filters_key, email_gateway) not in md_engines:
            make_md_engine(realm_filters_key, email_gateway)


_privacy_re = re.compile('\\w', flags=re.UNICODE)


def privacy_clean_markdown(content: str) ->str:
    return repr(_privacy_re.sub('x', content))


def log_bugdown_error(msg: str) ->None:
    """We use this unusual logging approach to log the bugdown error, in
    order to prevent AdminNotifyHandler from sending the santized
    original markdown formatting into another Zulip message, which
    could cause an infinite exception loop."""
    bugdown_logger.error(msg)


def get_email_info(realm_id: int, emails: Set[str]) ->Dict[str, FullNameInfo]:
    if not emails:
        return dict()
    q_list = {Q(email__iexact=email.strip().lower()) for email in emails}
    rows = UserProfile.objects.filter(realm_id=realm_id).filter(functools.
        reduce(lambda a, b: a | b, q_list)).values('id', 'email')
    dct = {row['email'].strip().lower(): row for row in rows}
    return dct


def get_possible_mentions_info(realm_id: int, mention_texts: Set[str]) ->List[
    FullNameInfo]:
    if not mention_texts:
        return list()
    full_names = set()
    name_re = '(?P<full_name>.+)\\|\\d+$'
    for mention_text in mention_texts:
        name_syntax_match = re.match(name_re, mention_text)
        if name_syntax_match:
            full_names.add(name_syntax_match.group('full_name'))
        else:
            full_names.add(mention_text)
    q_list = {Q(full_name__iexact=full_name) for full_name in full_names}
    rows = UserProfile.objects.filter(realm_id=realm_id, is_active=True
        ).filter(functools.reduce(lambda a, b: a | b, q_list)).values('id',
        'full_name', 'email')
    return list(rows)


class MentionData:

    def __init__(self, realm_id: int, content: str) ->None:
        mention_texts = possible_mentions(content)
        possible_mentions_info = get_possible_mentions_info(realm_id,
            mention_texts)
        self.full_name_info = {row['full_name'].lower(): row for row in
            possible_mentions_info}
        self.user_id_info = {row['id']: row for row in possible_mentions_info}
        self.init_user_group_data(realm_id=realm_id, content=content)

    def init_user_group_data(self, realm_id: int, content: str) ->None:
        user_group_names = possible_user_group_mentions(content)
        self.user_group_name_info = get_user_group_name_info(realm_id,
            user_group_names)
        self.user_group_members = defaultdict(list)
        group_ids = [group.id for group in self.user_group_name_info.values()]
        if not group_ids:
            return
        membership = UserGroupMembership.objects.filter(user_group_id__in=
            group_ids)
        for info in membership.values('user_group_id', 'user_profile_id'):
            group_id = info['user_group_id']
            user_profile_id = info['user_profile_id']
            self.user_group_members[group_id].append(user_profile_id)

    def get_user_by_name(self, name: str) ->Optional[FullNameInfo]:
        return self.full_name_info.get(name.lower(), None)

    def get_user_by_id(self, id: str) ->Optional[FullNameInfo]:
        return self.user_id_info.get(int(id), None)

    def get_user_ids(self) ->Set[int]:
        """
        Returns the user IDs that might have been mentioned by this
        content.  Note that because this data structure has not parsed
        the message and does not know about escaping/code blocks, this
        will overestimate the list of user ids.
        """
        return set(self.user_id_info.keys())

    def get_user_group(self, name: str) ->Optional[UserGroup]:
        return self.user_group_name_info.get(name.lower(), None)

    def get_group_members(self, user_group_id: int) ->List[int]:
        return self.user_group_members.get(user_group_id, [])


def get_user_group_name_info(realm_id: int, user_group_names: Set[str]) ->Dict[
    str, UserGroup]:
    if not user_group_names:
        return dict()
    rows = UserGroup.objects.filter(realm_id=realm_id, name__in=
        user_group_names)
    dct = {row.name.lower(): row for row in rows}
    return dct


def get_stream_name_info(realm: Realm, stream_names: Set[str]) ->Dict[str,
    FullNameInfo]:
    if not stream_names:
        return dict()
    q_list = {Q(name=name) for name in stream_names}
    rows = get_active_streams(realm=realm).filter(functools.reduce(lambda a,
        b: a | b, q_list)).values('id', 'name')
    dct = {row['name']: row for row in rows}
    return dct


def do_convert(content: str, message: Optional[Message]=None, message_realm:
    Optional[Realm]=None, possible_words: Optional[Set[str]]=None,
    sent_by_bot: Optional[bool]=False, translate_emoticons: Optional[bool]=
    False, mention_data: Optional[MentionData]=None, email_gateway:
    Optional[bool]=False) ->str:
    """Convert Markdown to HTML, with Zulip-specific settings and hacks."""
    if message is not None:
        if message_realm is None:
            message_realm = message.get_realm()
    if message_realm is None:
        realm_filters_key = DEFAULT_BUGDOWN_KEY
    else:
        realm_filters_key = message_realm.id
    if message is not None and message_realm is not None:
        if message_realm.is_zephyr_mirror_realm:
            if message.sending_client.name == 'zephyr_mirror':
                realm_filters_key = ZEPHYR_MIRROR_BUGDOWN_KEY
    maybe_update_markdown_engines(realm_filters_key, email_gateway)
    md_engine_key = realm_filters_key, email_gateway
    if md_engine_key in md_engines:
        _md_engine = md_engines[md_engine_key]
    else:
        if DEFAULT_BUGDOWN_KEY not in md_engines:
            maybe_update_markdown_engines(realm_filters_key=None,
                email_gateway=False)
        _md_engine = md_engines[DEFAULT_BUGDOWN_KEY, email_gateway]
    _md_engine.reset()
    _md_engine.zulip_message = message
    _md_engine.zulip_realm = message_realm
    _md_engine.zulip_db_data = None
    if message is not None:
        assert message_realm is not None
        if possible_words is None:
            possible_words = set()
        if mention_data is None:
            mention_data = MentionData(message_realm.id, content)
        emails = possible_avatar_emails(content)
        email_info = get_email_info(message_realm.id, emails)
        stream_names = possible_linked_stream_names(content)
        stream_name_info = get_stream_name_info(message_realm, stream_names)
        if content_has_emoji_syntax(content):
            active_realm_emoji = message_realm.get_active_emoji()
        else:
            active_realm_emoji = dict()
        _md_engine.zulip_db_data = {'possible_words': possible_words,
            'email_info': email_info, 'mention_data': mention_data,
            'active_realm_emoji': active_realm_emoji, 'realm_uri':
            message_realm.uri, 'sent_by_bot': sent_by_bot, 'stream_names':
            stream_name_info, 'translate_emoticons': translate_emoticons}
    try:
        rendered_content = timeout(5, _md_engine.convert, content)
        if len(rendered_content) > MAX_MESSAGE_LENGTH * 10:
            raise BugdownRenderingException(
                'Rendered content exceeds %s characters' % (
                MAX_MESSAGE_LENGTH * 10,))
        return rendered_content
    except Exception:
        cleaned = privacy_clean_markdown(content)
        exception_message = (
            'Exception in Markdown parser: %sInput (sanitized) was: %s' % (
            traceback.format_exc(), cleaned))
        bugdown_logger.exception(exception_message)
        raise BugdownRenderingException()
    finally:
        _md_engine.zulip_message = None
        _md_engine.zulip_realm = None
        _md_engine.zulip_db_data = None


bugdown_time_start = 0.0
bugdown_total_time = 0.0
bugdown_total_requests = 0


def get_bugdown_time() ->float:
    return bugdown_total_time


def get_bugdown_requests() ->int:
    return bugdown_total_requests


def bugdown_stats_start() ->None:
    global bugdown_time_start
    bugdown_time_start = time.time()


def bugdown_stats_finish() ->None:
    global bugdown_total_time
    global bugdown_total_requests
    global bugdown_time_start
    bugdown_total_requests += 1
    bugdown_total_time += time.time() - bugdown_time_start


def convert(content: str, message: Optional[Message]=None, message_realm:
    Optional[Realm]=None, possible_words: Optional[Set[str]]=None,
    sent_by_bot: Optional[bool]=False, translate_emoticons: Optional[bool]=
    False, mention_data: Optional[MentionData]=None, email_gateway:
    Optional[bool]=False) ->str:
    bugdown_stats_start()
    ret = do_convert(content, message, message_realm, possible_words,
        sent_by_bot, translate_emoticons, mention_data, email_gateway)
    bugdown_stats_finish()
    return ret
