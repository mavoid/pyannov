import annotations
import logging
import re
from typing import Any, Dict, List, Optional, Tuple
import ujson
from django.conf import settings
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.utils.translation import ugettext as _
from zerver.decorator import api_key_only_webhook_view
from zerver.lib.request import REQ, has_request_variables
from zerver.lib.response import json_error, json_success
from zerver.lib.webhooks.common import check_send_webhook_message, UnexpectedWebhookEventType
from zerver.models import Realm, UserProfile, get_user_by_delivery_email
IGNORED_EVENTS = ['comment_created', 'comment_updated', 'comment_deleted']


def guess_zulip_user_from_jira(jira_username: str, realm: Realm) ->Optional[
    UserProfile]:
    try:
        user = UserProfile.objects.filter(Q(full_name__iexact=jira_username
            ) | Q(short_name__iexact=jira_username) | Q(email__istartswith=
            jira_username), is_active=True, realm=realm).order_by('id')[0]
        return user
    except IndexError:
        return None


def convert_jira_markup(content: str, realm: Realm) ->str:
    content = re.sub('\\*([^\\*]+)\\*', '**\\1**', content)
    content = re.sub('{{([^\\*]+?)}}', '`\\1`', content)
    content = re.sub('bq\\. (.*)', '> \\1', content)
    quote_re = re.compile('{quote}(.*?){quote}', re.DOTALL)
    content = re.sub(quote_re, '~~~ quote\\n\\1\\n~~~', content)
    noformat_re = re.compile('{noformat}(.*?){noformat}', re.DOTALL)
    content = re.sub(noformat_re, '~~~\\n\\1\\n~~~', content)
    code_re = re.compile('{code[^\\n]*}(.*?){code}', re.DOTALL)
    content = re.sub(code_re, '~~~\\n\\1\\n~~~', content)
    content = re.sub('\\[([^\\|~]+?)\\]', '[\\1](\\1)', content)
    full_link_re = re.compile('\\[(?:(?P<title>[^|~]+)\\|)(?P<url>.*)\\]')
    content = re.sub(full_link_re, '[\\g<title>](\\g<url>)', content)
    if realm:
        mention_re = re.compile('\\[~(.*?)\\]')
        for username in mention_re.findall(content):
            user_profile = guess_zulip_user_from_jira(username, realm)
            if user_profile:
                replacement = '**{}**'.format(user_profile.full_name)
            else:
                replacement = '**{}**'.format(username)
            content = content.replace('[~{}]'.format(username), replacement)
    return content


def get_in(payload: Dict[str, Any], keys: List[str], default: str='') ->Any:
    try:
        for key in keys:
            payload = payload[key]
    except (AttributeError, KeyError, TypeError):
        return default
    return payload


def get_issue_string(payload: Dict[str, Any], issue_id: Optional[str]=None
    ) ->str:
    if issue_id is None:
        issue_id = get_issue_id(payload)
    base_url = re.match('(.*)\\/rest\\/api/.*', get_in(payload, ['issue',
        'self']))
    if base_url and len(base_url.groups()):
        return '[{}]({}/browse/{})'.format(issue_id, base_url.group(1),
            issue_id)
    else:
        return issue_id


def get_assignee_mention(assignee_email: str, realm: Realm) ->str:
    if assignee_email != '':
        try:
            assignee_name = get_user_by_delivery_email(assignee_email, realm
                ).full_name
        except UserProfile.DoesNotExist:
            assignee_name = assignee_email
        return '**{}**'.format(assignee_name)
    return ''


def get_issue_author(payload: Dict[str, Any]) ->str:
    return get_in(payload, ['user', 'displayName'])


def get_issue_id(payload: Dict[str, Any]) ->str:
    return get_in(payload, ['issue', 'key'])


def get_issue_title(payload: Dict[str, Any]) ->str:
    return get_in(payload, ['issue', 'fields', 'summary'])


def get_issue_subject(payload: Dict[str, Any]) ->str:
    return '{}: {}'.format(get_issue_id(payload), get_issue_title(payload))


def get_sub_event_for_update_issue(payload: Dict[str, Any]) ->str:
    sub_event = payload.get('issue_event_type_name', '')
    if sub_event == '':
        if payload.get('comment'):
            return 'issue_commented'
        elif payload.get('transition'):
            return 'issue_transited'
    return sub_event


def get_event_type(payload: Dict[str, Any]) ->Optional[str]:
    event = payload.get('webhookEvent')
    if event is None and payload.get('transition'):
        event = 'jira:issue_updated'
    return event


def add_change_info(content: str, field: str, from_field: str, to_field: str
    ) ->str:
    content += '* Changed {}'.format(field)
    if from_field:
        content += ' from **{}**'.format(from_field)
    if to_field:
        content += ' to {}\n'.format(to_field)
    return content


def handle_updated_issue_event(payload: Dict[str, Any], user_profile:
    UserProfile) ->str:
    payload = (annotations.
        annot_zerver_webhooks_jira_view_handle_updated_issue_event_payload)
    issue_id = get_in(payload, ['issue', 'key'])
    issue = get_issue_string(payload, issue_id)
    assignee_email = get_in(payload, ['issue', 'fields', 'assignee',
        'emailAddress'], '')
    assignee_mention = get_assignee_mention(assignee_email, user_profile.realm)
    if assignee_mention != '':
        assignee_blurb = ' (assigned to {})'.format(assignee_mention)
    else:
        assignee_blurb = ''
    sub_event = get_sub_event_for_update_issue(payload)
    if 'comment' in sub_event:
        if sub_event == 'issue_commented':
            verb = 'added comment to'
        elif sub_event == 'issue_comment_edited':
            verb = 'edited comment on'
        else:
            verb = 'deleted comment from'
        content = '{} **{}** {}{}'.format(get_issue_author(payload), verb,
            issue, assignee_blurb)
        comment = get_in(payload, ['comment', 'body'])
        if comment:
            comment = convert_jira_markup(comment, user_profile.realm)
            content = '{}:\n\n\n{}\n'.format(content, comment)
    else:
        content = '{} **updated** {}{}:\n\n'.format(get_issue_author(
            payload), issue, assignee_blurb)
        changelog = get_in(payload, ['changelog'])
        if changelog != '':
            items = changelog.get('items')
            for item in items:
                field = item.get('field')
                if field == 'assignee' and assignee_mention != '':
                    target_field_string = assignee_mention
                else:
                    target_field_string = '**{}**'.format(item.get('toString'))
                from_field_string = item.get('fromString')
                if target_field_string or from_field_string:
                    content = add_change_info(content, field,
                        from_field_string, target_field_string)
        elif sub_event == 'issue_transited':
            from_field_string = get_in(payload, ['transition', 'from_status'])
            target_field_string = '**{}**'.format(get_in(payload, [
                'transition', 'to_status']))
            if target_field_string or from_field_string:
                content = add_change_info(content, 'status',
                    from_field_string, target_field_string)
    return content


def handle_created_issue_event(payload: Dict[str, Any]) ->str:
    return '{} **created** {} priority {}, assigned to **{}**:\n\n> {}'.format(
        get_issue_author(payload), get_issue_string(payload), get_in(
        payload, ['issue', 'fields', 'priority', 'name']), get_in(payload,
        ['issue', 'fields', 'assignee', 'displayName'], 'no one'),
        get_issue_title(payload))


def handle_deleted_issue_event(payload: Dict[str, Any]) ->str:
    return '{} **deleted** {}!'.format(get_issue_author(payload),
        get_issue_string(payload))


@api_key_only_webhook_view('JIRA')
@has_request_variables
def api_jira_webhook(request: HttpRequest, user_profile: UserProfile,
    payload: Dict[str, Any]=REQ(argument_type='body')) ->HttpResponse:
    event = get_event_type(payload)
    if event == 'jira:issue_created':
        subject = get_issue_subject(payload)
        content = handle_created_issue_event(payload)
    elif event == 'jira:issue_deleted':
        subject = get_issue_subject(payload)
        content = handle_deleted_issue_event(payload)
    elif event == 'jira:issue_updated':
        subject = get_issue_subject(payload)
        content = handle_updated_issue_event(payload, user_profile)
    elif event in IGNORED_EVENTS:
        return json_success()
    else:
        raise UnexpectedWebhookEventType('Jira', event)
    check_send_webhook_message(request, user_profile, subject, content,
        unquote_url_parameters=True)
    return json_success()
