def annot_aiohttp_hdrs_istr(s: str)-> str: ...
annot_aiohttp_test_utils_make_mocked_request_mock_Mock = ... # type: Mock
annot_aiohttp_web_app_Callable = ... # type: Callable
annot_aiohttp_web_app_Union = ... # type: Union
annot_aiohttp_web_app_FrozenList = ... # type: FrozenList
annot_aiohttp_web_app_Optional = ... # type: Optional
annot_aiohttp_web_protocol_Callable = ... # type: Callable
annot_aiohttp_web_routedef_Callable = ... # type: Callable
annot_aiohttp_typedefs_Union = ... # type: Union
def annot_aiohttp_typedefs_str(a: Any) -> str: ...
