def annot_dash_dash_re_compile(s: str) ->  Pattern: ...
def annot_dash__configs_pathname_configs_get_config() -> str: ... 
annot_dash__configs_pathname_configs_url_base_pathname = ... # type: str
annot_dash_development_component_loader_load_components_metadata_path = ... # type: str
annot_tests_development_test_base_component_nested_tree_Component = ... # type: Component

annot_dash__configs_pathname_configs_environ_configs = ... # type: AttributeDict
annot_dash__configs_pathname_configs_env_configs = ... # type: AttributeDict
annot_dash_development_component_loader_load_components_data = ... # type: JSONObject
annot_dash_development__r_components_generation_generate_js_metadata_sys_modules = ... # type: Dict[str, Module]
