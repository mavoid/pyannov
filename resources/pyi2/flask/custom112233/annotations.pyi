def annot_flask_helpers_send_file_os_path_join(p1: str, p2: str) -> str: ...
def annot_flask__compat_iter() -> Iterator: ...
annot_flask_testing_make_test_environ_builder_url_query = ... # type: str
annot_flask_helpers_send_file_filename = ... # type: str
annot_flask_helpers_send_file_filename_or_fp = ... # type: str
annot_flask_helpers_send_file_current_app_root_path = ... # type: str
annot_flask_signals__signals_signal = ... # type: _FakeSignal
annot_flask_globals_LocalProxy = ... # type: LocalProxy
annot_flask_cli_routes_command_rules = ... # type: List[str]
def annot_flask_cli_routes_command_sorted(a: List[Any]) -> List[Any]: ...
