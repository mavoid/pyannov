annot_numpy_f2py_crackfortran_analyzevars_block = ... # type: dict
annot_numpy_ma_core__MaskedUnaryOperation = ... # type: _MaskedUnaryOperation
def annot_numpy_f2py_crackfortran_re_compile(s: str) -> Pattern: ...
annot_numpy_f2py_crackfortran_beforethisafter = ... # type: str
annot_numpy_f2py_crackfortran_analyzeline_groupcounter = ... # type: int
def annot_numpy_ma_core__frommethod(s: str) -> _frommethod: ... 
annot_numpy_f2py_crackfortran_analyzevars_vars = ... # type: set
annot_numpy_f2py_crackfortran_analyzeline_groupcache = ... # type: dict
annot_numpy_ma_core__MaskedBinaryOperation = ... # type: _MaskedBinaryOperation
annot_numpy_f2py_crackfortran_re_I = ... # type: RegexFlag

