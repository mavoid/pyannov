def annot_scipy_constants_constants__cd(s: str) -> float: ...
annot_scipy_sparse_linalg_isolve_lsqr_lsqr_alfa = ... # type: int
def annot_scipy_sparse_linalg_isolve_minres_minres_inner(a: List[Any]) -> ndarray: ... 
annot_scipy_sparse_linalg_isolve_minres_minres_beta1 = ... # type: int
def annot_scipy_linalg__expm_frechet_expm_frechet_algo_64_np_dot(a: List[Any], b: List[Any]) -> ndarray: ... 
annot_scipy_optimize__linprog_util__presolve_lb = ... # type: ndarray
annot_scipy_optimize__linprog_util__presolve_A_eq = ... # type: ndarray
annot_scipy_sparse_linalg_isolve_lsqr_lsqr_beta = ... # type: Union[float, ndarray]
annot_scipy_sparse_linalg_isolve_minres_minres_beta = ... # type: float
annot_scipy_sparse_linalg_isolve_minres_minres_y = ... # type: float
