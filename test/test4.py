def foo(x):
    base = 10000000
    i = x
    res = 0
    while i > 0:
        res += i * 3
        i -= 1
    else:
        res += base